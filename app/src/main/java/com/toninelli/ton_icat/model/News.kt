package com.toninelli.ton_icat.model

import java.util.*

data class News(
    val title: String,
    val body: String,
    val date: Date,
    val type: NewsType,
    val image: String?
) {
    enum class NewsType {
        ADD_INFO,
        ADD_COMPONENT,
        NEW_SECTION
    }
}