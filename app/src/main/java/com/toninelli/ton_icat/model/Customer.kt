package com.toninelli.ton_icat.model

import androidx.room.ColumnInfo
import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Customer(
    @PrimaryKey
    val customerId: String,
    val name: String,
    val tel: String,
    val cel: String,
    val fax: String,
    val email: String,
    val business_email: String,
    val piva: String,
    val assigned_agent: String,
    val price_list_name: String,
    val address: List<Address>,
    var preferredAddress: Address? = null
    ){

    data class Address(
        val alias: String?,
        val address: String,
        val town: String,
        val province: String,
        val cap: String
    )

}