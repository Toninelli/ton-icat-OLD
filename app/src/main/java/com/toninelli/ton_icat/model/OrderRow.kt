package com.toninelli.ton_icat.model

import androidx.room.*

@Entity(
    foreignKeys = [ForeignKey(
        entity = Order::class,
        parentColumns = ["orderId"],
        childColumns = ["orderId"],
        onDelete = ForeignKey.CASCADE
    )],
    indices = [Index(value = ["orderId"])]

)
data class OrderRow(
    @PrimaryKey(autoGenerate = true)
    val orderRowId: Long = 0,
    @Embedded
    val catalogItem: CatalogItem,
    var addition: Long? = null,
    var orderQta: Int = 0,
    var multQta: Int = catalogItem.article.min,
    var selectedPackage: Article.Package? = null,
    var orderId: Long? = null,
    var customerId: String? = null
)