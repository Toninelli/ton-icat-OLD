package com.toninelli.ton_icat.model

import java.util.*

data class SyncLog(val status: SyncStatus, val lastSync: Date)

enum class SyncStatus { NOT_COMPLETED, COMPLETED }