package com.toninelli.ton_icat.model

import androidx.room.Embedded
import androidx.room.PrimaryKey

data class Article(
    @PrimaryKey
    val artCod: Int,
    val descr: String,
    val ean: Int,
    val artCodSup: Int,
    val price: Float,
    val min: Int,
    val packaging: List<Package>,
    val ton_stock: Int,
    val hd_stock: Int,
    val type: String,
    @Embedded
    val supplier: Supplier,
    val category: Int,
    var image: String,
    var images: List<String>,
    val useDirections: String?,
    val item: List<Item>?
) {
    data class Package(val packName: String, val qta: Int)

    data class Item(
        val number: Int?,
        val name: String?,
        val height: String?,
        val weight: String?,
        val diameter: String?,
        val capacity: String?,
        val width: String?,
        val length: String?,
        val depth: String?,
        val thickness: String?,
        val size: String?,
        val volume: String?,
        val power: String?
    )

}