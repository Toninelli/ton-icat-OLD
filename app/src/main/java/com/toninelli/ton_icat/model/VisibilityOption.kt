package com.toninelli.ton_icat.model

data class VisibilityOption(val name: String, val show: Boolean)