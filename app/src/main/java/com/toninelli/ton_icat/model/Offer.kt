package com.toninelli.ton_icat.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Offer(
    @PrimaryKey
    val id: Int,
    val name: String,
    val validity: String,
    val description: String,
    val articles: List<ArtOffer>){

    fun getArtOffer(artId: Int): ArtOffer?{
        return articles.firstOrNull { it.artId == artId }
    }
}

data class ArtOffer(val artId: Int, val qta: Int, val price: Float)