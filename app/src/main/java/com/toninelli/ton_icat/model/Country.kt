package com.toninelli.ton_icat.model

data class Country(val name: String, val code: String)