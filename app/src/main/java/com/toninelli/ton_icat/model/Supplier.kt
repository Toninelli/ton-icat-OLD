package com.toninelli.ton_icat.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Supplier(
    @PrimaryKey
    val supId: String,
    val supName: String)