package com.toninelli.ton_icat.model

import androidx.room.*
import androidx.room.ForeignKey.CASCADE


@Entity(primaryKeys = ["artCod"])
data class  CatalogItem(
    @Embedded
    val article: Article,
    var offerName: String? = null,
    var offerPrice: Float = 0F,
    var offerQta: Int = 0,
    var acceptedOffer: Boolean = false,
    var discountedPrice: Float? = null

    )