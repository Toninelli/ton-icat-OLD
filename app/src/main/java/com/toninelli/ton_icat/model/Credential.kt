package com.toninelli.ton_icat.model


data class Credential(
    var username: String? = null,
    var password: String? = null,
    var remember: Boolean = false
)