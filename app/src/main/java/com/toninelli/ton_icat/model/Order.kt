package com.toninelli.ton_icat.model

import androidx.room.*
import java.util.*

@Entity(
    foreignKeys = [ForeignKey(
        entity = Customer::class,
        parentColumns = ["customerId"],
        childColumns = ["customerId"],
        onDelete = ForeignKey.CASCADE
    )],
    indices = [Index(value = ["customerId"])]

)
data class Order(
    @PrimaryKey
    val orderId: Long,
    var state: State,
    var type: Type,
    val customerId: String,
    val agent: String,
    val creationDate: Date,
    var delivery: Delivery?,
    var transmissionDate: Date?,
    var destination: Customer.Address?,
    var nRows: Int = 0,
    val note: String? = null
) {

    enum class State(val stateName: String) {
        NOT_COMPLETED("Non completato"),
        TRANSMITTED("Trasmesso")
    }

    enum class Type(val typeName: String) {
        ORDER("Ordine"),
        ORDER_PROPOSAL("Proposta d'ordine")
    }

    data class Delivery(val delName: String?, val delDate: Date?) {


        companion object {
            fun getStringValues(): List<String> {
                return listOf("Tassativa", "Indicativa", "Non prima")
            }
        }
    }


}

