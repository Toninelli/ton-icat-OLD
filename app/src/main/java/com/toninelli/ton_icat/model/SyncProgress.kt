package com.toninelli.ton_icat.model

import com.toninelli.ton_icat.vo.Status

data class SyncProgress(
    val type: Type? = null,
    val status: Status,
    val progress: Int? = null,
    val total: Int? = null
){

    enum class Type(val typeName: String){
        CATALOG("Catalogo"),
        CUSTOMER("Clienti"),
        CATEGORY("Categorie"),
        SUPPLIER("Fornitori")

    }

}

