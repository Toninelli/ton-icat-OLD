package com.toninelli.ton_icat.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Category(
    @PrimaryKey
    val id: Int,
    val name: String,
    val level: Int,
    val parent: Int
)