package com.toninelli.ton_icat.model

data class BooleanResponse(
    val error: Int,
    val value: String?,
    val txt: String?,
    val errors: List<Error>?
)


data class Error(
    val tit: String,
    val txt: String
)