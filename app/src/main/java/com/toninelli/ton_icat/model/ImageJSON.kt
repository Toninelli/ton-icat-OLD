package com.toninelli.ton_icat.model

data class ImageJSON(val image: String)