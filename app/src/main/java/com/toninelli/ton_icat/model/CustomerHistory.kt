package com.toninelli.ton_icat.model

data class CustomerHistory(

    val total: String,

    val date: Long,

    val rowCustomers: List<CustomerHistoryRow>
){

    data class CustomerHistoryRow(val artCod: Int, val artName: String, val supplierCod: String, val qta: Int, val unitPrice: Float)

}