package com.toninelli.ton_icat.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Agent(
    @PrimaryKey
    val cod: String,
    val name: String,
    val surname: String,
    val email: String
)