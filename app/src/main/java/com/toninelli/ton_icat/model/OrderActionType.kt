package com.toninelli.ton_icat.model

enum class OrderActionType {

    DETAIL,
    HEADER,
    DELETE,
}

enum class OrderRowActionType {

    HEADER,
    DELETE,
    DISCOUNT
}