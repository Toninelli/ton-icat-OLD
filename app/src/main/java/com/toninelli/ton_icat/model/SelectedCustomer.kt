package com.toninelli.ton_icat.model

import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.ForeignKey.CASCADE
import androidx.room.Index
import androidx.room.PrimaryKey

@Entity(
    foreignKeys = [ForeignKey(entity = Customer::class, parentColumns = ["customerId"], childColumns = ["customerId"], onDelete = CASCADE )],
    indices = [Index(value = ["customerId"])]
)
data class SelectedCustomer(
    @PrimaryKey(autoGenerate = true)
    val selectedCustomerId: Int = 0,
    val customerId: String
)