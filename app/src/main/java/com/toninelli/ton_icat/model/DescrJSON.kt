package com.toninelli.ton_icat.model

data class DescrJSON(val codArt: Int, val description: String)