package com.toninelli.ton_icat.data.database

import com.toninelli.ton_icat.model.*
import com.toninelli.ton_icat.vo.BooleanResult
import com.toninelli.ton_icat.vo.Either
import com.toninelli.ton_icat.vo.Failure
import com.toninelli.ton_icat.vo.SingleEventWrapper
import kotlinx.coroutines.flow.Flow
import java.util.*

interface DbRepository {

    //offer
    suspend fun getOffers(): Either<Failure, List<Offer>>

    //category
    suspend fun getCategories(): Either<Failure, CategoryTree>

    //supplier
    suspend fun getSupplier(): Either<Failure, List<Supplier>>

    //icat option
    suspend fun setVisibilityOption(visibilityOption: VisibilityOption?): Either<Failure, List<VisibilityOption>>

    //sync
    suspend fun getLastSyncCatalog(): Either<Failure, SyncLog>

    //agent
    suspend fun getCredential(): Either<Failure, Credential>

    suspend fun setCredential(credential: Credential): Either<Failure, BooleanResult>

    suspend fun getAgent(): Either<Failure, Agent>

    suspend fun saveAgent(agent: Agent): Either<Failure, BooleanResult>

    suspend fun chekUniqueAgent(newAgent: Agent): Either<Failure, BooleanResult>

    //catalog item
    suspend fun getCatalogItemByCateg(categId: Int, limit: Int, offset: Int): Either<Failure, List<CatalogItem>>

    suspend fun getCatalogItemByQuery(query: String, limit: Int, offset: Int): Either<Failure, List<CatalogItem>>

    suspend fun getCatalogItemBySupplier(supplierId: String,  limit: Int, offset: Int): Either<Failure, List<CatalogItem>>

    suspend fun getCatalogItemByCods(cods: List<Int>): Either<Failure, List<CatalogItem>>

    //customer
    suspend fun getAllCustomers(): Either<Failure, List<Customer>>

    suspend fun getSelectedCustomer(): Flow<Either<Failure, SelectedCustomer>>

    suspend fun selectCustomer(customerId: String, select: Boolean): Either<Failure, Boolean>

    suspend fun getCustomerById(customerId: String): Either<Failure, Customer>

    suspend fun getCustomerFlowById(customerId: String): Flow<Either<Failure, Customer>>

    suspend fun getAllCustomersDoingOrder(): Flow<Either<Failure, List<Customer>>>

    suspend fun updatePreferredAddress(customerId: String, address: Customer.Address): Either<Failure, SingleEventWrapper<BooleanResult>>

    //order
    suspend fun getAllOrders(): Flow<Either<Failure, List<Order>>>

    suspend fun getCustomerOrdersByStatus(customerId: String?, state: Order.State): Flow<Either<Failure, List<Order>>>

    suspend fun getOrdersByCustomer(customerId: String): Flow<Either<Failure, List<Order>>>

    suspend fun getOrderById(orderId: Long):Flow<Either<Failure, Order>>

    suspend fun deleteOrder(orderId: Long): Either<Failure, SingleEventWrapper<BooleanResult>>

    suspend fun insertOrder(customerId: String): Either<Failure, SingleEventWrapper<BooleanResult>>

    suspend fun updateOrderType(orderId: Long, type: Order.Type): Either<Failure, SingleEventWrapper<BooleanResult>>

    suspend fun updateOrderDestination(orderId: Long, destination: Customer.Address): Either<Failure, SingleEventWrapper<BooleanResult>>

    suspend fun updateOrderDelivery(orderId: Long, delivery: Order.Delivery): Either<Failure, SingleEventWrapper<BooleanResult>>

    suspend fun updateOrderState(orderId: Long, state: Order.State): Either<Failure, SingleEventWrapper<BooleanResult>>

    suspend fun updateTransmissionDate(orderId: Long, date: Date): Either<Failure, SingleEventWrapper<BooleanResult>>

    suspend fun updateOrderNote(orderId: Long, note: String): Either<Failure, BooleanResult>

    // OrderRow

    suspend fun insertOrderRow(orderRow: OrderRow): Either<Failure, SingleEventWrapper<BooleanResult>>

    suspend fun getOrderRowByOrder(orderId: Long): Flow<Either<Failure, List<OrderRow>>>

    suspend fun getOrderRowSizeByOrder(orderId: Long): Flow<Either<Failure, Int>>

    suspend fun deleteOrderRow(orderRow: OrderRow): Either<Failure, SingleEventWrapper<BooleanResult>>

    suspend fun orderRowSizeByCustomer(customerId: String): Either<Failure, Map<Long, Int>>

    suspend fun insertDiscountPrice(id: Long, discountPrice: Float): Either<Failure, BooleanResult>


}