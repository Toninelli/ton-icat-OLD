package com.toninelli.ton_icat.data.database

import androidx.room.*
import com.toninelli.ton_icat.model.SelectedCustomer
import kotlinx.coroutines.flow.Flow

@Dao
interface SelectedCustomerDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(selectedCustomer: SelectedCustomer): Long

    @Query("DELETE FROM SelectedCustomer")
    suspend fun remove()

    @Query("SELECT * FROM SelectedCustomer")
    fun getSelectedCustomer(): Flow<SelectedCustomer>



}