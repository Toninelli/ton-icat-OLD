package com.toninelli.ton_icat.data.database

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.toninelli.ton_icat.model.Article
import com.toninelli.ton_icat.model.CatalogItem
import com.toninelli.ton_icat.model.Supplier

@Dao
interface CatalogItemDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(catalogItem: CatalogItem)


    @Query("DELETE FROM CatalogItem")
    suspend fun delete()

    @Query("SELECT * FROM CatalogItem  WHERE descr LIKE :query OR artCod LIKE :query ORDER BY descr ")
    suspend fun getCatalogItemByQuery(query: String): List<CatalogItem>

    @Query("SELECT * FROM CatalogItem  WHERE supId LIKE :supplierId ORDER BY descr LIMIT :limit OFFSET :offset")
    suspend fun getCatalogItemBySupplier(supplierId: String, limit: Int, offset: Int): List<CatalogItem>

    @Query("SELECT * FROM CatalogItem  WHERE category in (:ids) ORDER BY descr LIMIT :limit OFFSET :offset")
    suspend fun getCatalogItemByCateg(ids: List<Int>, limit: Int, offset: Int): List<CatalogItem>

    @Query("SELECT * FROM CatalogItem WHERE artCod IN (:cods) ORDER BY descr")
    suspend fun getCatalogItemByCods(cods: List<Int>): List<CatalogItem>

}