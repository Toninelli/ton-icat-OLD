package com.toninelli.ton_icat.data.network

import com.toninelli.ton_icat.model.*
import com.toninelli.ton_icat.vo.BooleanResult
import com.toninelli.ton_icat.vo.Either
import com.toninelli.ton_icat.vo.Failure
import kotlinx.coroutines.flow.Flow

interface NetworkRepository {

    suspend fun getPdfOffer(): Either<Failure, List<Pdf>>

    suspend fun syncCatalog(): Flow<Either<Failure, SyncProgress>>

    suspend fun getDiscountedPrice(customerId: String, artId: Int): Either<Failure, Float>

    suspend fun sendOrder(): Either<Failure, BooleanResult>

    suspend fun login(username: String, password: String): Either<Failure, Agent>

    suspend fun getCustomerHistory(customerId: String): Either<Failure, List<CustomerHistory>>


}