package com.toninelli.ton_icat.data

import android.content.Context
import com.example.networkcalladapterlib.ResponseNetwork
import com.toninelli.ton_icat.R
import com.toninelli.ton_icat.extension.getDescrList
import com.toninelli.ton_icat.extension.getImagesList
import com.toninelli.ton_icat.model.*
import kotlinx.coroutines.delay
import retrofit2.Response
import java.util.*
import kotlin.collections.ArrayList
import kotlin.random.Random
import com.toninelli.ton_icat.model.Article.Package
import com.toninelli.ton_icat.model.Article.Item

object Mock {

    suspend fun offers(): MutableList<Offer> {

//        delay(10000)

        val list = mutableListOf<Offer>()
        repeat(2) {
            list.add(
                Offer(
                    it,
                    "Offerta $it",
                    "valida fino a bo",
                    "descrizione",
                    listOf(ArtOffer(it * 1000 * 5 + 4000, 1, 10.5F))
                )
            )

        }

        return list
    }

    suspend fun news(): ResponseNetwork<MutableList<News>> {

//        delay(100)

        val list = mutableListOf<News>()
        repeat(10) {
            if (it == 5)
                list.add(
                    News(
                        "News $it",
                        "Nuova notifica",
                        Date(),
                        News.NewsType.values().asList().shuffled()[0],
                        "https://picsum.photos/id/1/200/300"
                    )
                )
            else
                list.add(
                    News(
                        "News $it",
                        "Nuova notifica",
                        Date(),
                        News.NewsType.values().asList().shuffled()[0],
                        null
                    )
                )
        }

        val response = Response.success(list)
        return ResponseNetwork.create(response)
    }

    suspend fun pdfOffer(): ResponseNetwork<MutableList<Pdf>> {

//        delay(100)

        val list = mutableListOf<Pdf>()
        repeat(10) {
            list.add(Pdf("Pdf $it"))
        }

        val response = Response.success(list)
        return ResponseNetwork.create(response)
    }

    suspend fun makeCategories(): ArrayList<Category> {

//        delay(10000)


        //bricolage

        val bricolage = Category(2, "bricolage", 1, 0)
        val giardinaggio = Category(21, "Giardinaggio", 2, 2)
        val sisPiante = Category(211, "Sistemazione Piante", 3, 21)
        val vasi = Category(2111, "Vasi", 4, 211)
        val vasiPlast = Category(21111, "Plastica", 5, 2111)
        val sottoVasi = Category(2112, "Sottovasi", 4, 211)
        val sottoVasiPlast = Category(21121, "Plastica", 5, 2112)
        val fioriere = Category(2113, "Fioriere", 4, 211)
        val decoraz = Category(212, "Decorazione", 3, 21)
        val statFont = Category(2121, "Statue e fontane", 4, 212)

        //casalinghi
        val casalinghi = Category(3, "casalinghi", 1, 0)
        val tavola = Category(31, "tavola", 2, 3)
        val piatti = Category(311, "Piatti", 3, 31)
        val piano = Category(3111, "Piano", 4, 311)
        val pianoPorc = Category(31111, "Porcellana", 5, 3111)
        val fondo = Category(3112, "Fondo", 4, 311)
        val fondoPorc = Category(31121, "Porcellana", 5, 3112)
        val frutta = Category(3113, "Frutta", 4, 311)
        val fruttaPorc = Category(31131, "Porcellana", 5, 3113)
        val pizza = Category(3114, "Pizza", 4, 311)
        val pizzaPorc = Category(31141, "Porcellana", 5, 3114)
        val piatServ = Category(3115, "Servizi", 4, 311)
        val piatServPorc = Category(31151, "Porcellana", 5, 3115)
        val accPort = Category(312, "Accessori da portata", 3, 31)
        val sotVas = Category(3121, "Sottopiatti/vassoi", 4, 312)
        val sotVasPorc = Category(31211, "Porcellana", 5, 3121)
        val insZupp = Category(3122, "Insalat zuppiere sals antipasti", 4, 312)
        val insZuppPorc = Category(31221, "Porcellana", 5, 3122)
        val ciotolina = Category(3123, "Ciotolina", 4, 312)
        val ciotolinaPorc = Category(31231, "Porcellana", 5, 3123)
        val accPortServ = Category(3124, "Servizi/set", 4, 312)
        val accPortServVetOpal = Category(31241, "Vetro/opale", 5, 3124)
        val colaz = Category(313, "Colazione", 3, 31)
        val tazCaff = Category(3131, "Tazze da Caffè", 4, 313)
        val tazCaffPorc = Category(31311, "Porcellana", 5, 31311)
        val tazTe = Category(3132, "Tazze da Te", 4, 313)
        val tazTePorc = Category(31321, "Porcellana", 5, 3132)
        val tazjumb = Category(3133, "Tazze jumbo/colazione", 4, 313)
        val tazjumboPorc = Category(31331, "Porcellana", 5, 3133)
        val tazmug = Category(3134, "Tazze mug", 4, 313)
        val tazmugPorc = Category(31341, "Porcellana", 5, 3134)
        val tazbol = Category(3135, "Tazze bolo (scodella)", 4, 313)
        val tazbolPorc = Category(31351, "Porcellana", 5, 3135)
        val colazServ = Category(3136, "Servizi", 4, 313)
        val colazServPorc = Category(31361, "Porcellana", 5, 3136)
        val accColaz = Category(3137, "Accessori colazione", 4, 313)
        val accColazPorc = Category(31371, "Porcellana", 5, 3137)
        val bicchCalic = Category(314, "Bicchieri e calici", 3, 31)
        val bichAcq = Category(3141, "Bicchieri acqua", 4, 314)
        val bichAcqVet = Category(31411, "Vetro", 5, 3141)
        val bichVino = Category(3142, "Bicchieri vino", 4, 314)
        val bichVinoVet = Category(31421, "Vetro", 5, 3142)
        val bichBib = Category(3143, "Bicchieri bibita", 4, 314)
        val bichBibVet = Category(31431, "Vetro", 5, 3143)
        val bichBir = Category(3144, "Boccale birra", 4, 314)
        val bichBirVet = Category(31441, "Vetro", 5, 3144)
        val bichLiq = Category(3145, "Bicchieri liquore", 4, 314)
        val bichLiqVet = Category(31451, "Vetro", 5, 3145)
        val calAcq = Category(3146, "Calici acqua", 4, 314)
        val calAcqVet = Category(31461, "Vetro", 5, 3146)
        val calVino = Category(3147, "Calici vino", 4, 314)
        val calVinoVet = Category(31471, "Vetro", 5, 3147)
        val calFlut = Category(3148, "Calici e flute spumante", 4, 314)
        val calFlutVet = Category(31481, "Vetro", 5, 3148)
        val calBir = Category(3149, "Calici birra", 4, 314)
        val calBirVet = Category(31491, "Vetro", 5, 3149)
        val calLiq = Category(31401, "Calici liquori", 4, 314)
        val calLiqVet = Category(314011, "Vetro", 5, 31401)
        val bichServ = Category(31402, "Servizi", 4, 314)
        val bichServVet = Category(314021, "Vetro", 5, 31402)
        val bott = Category(31403, "Bottiglie", 4, 314)
        val bottVet = Category(314031, "Vetro", 5, 31403)
        val decanter = Category(31404, "Caraffe/Decanter", 4, 314)
        val decanterVet = Category(314041, "Vetro", 5, 31404)
        val altriCal = Category(31405, "AltriCalici", 4, 314)
        val posate = Category(315, "Posate", 3, 31)
        val forc = Category(3151, "Forchette", 4, 315)
        val forcAcc = Category(31511, "Acciaio", 5, 3151)
        val colt = Category(3152, "Coltelli", 4, 315)
        val coltAcc = Category(31521, "Acciaio", 5, 3152)
        val cucch = Category(3153, "Cucchiai", 4, 315)
        val cucchAcc = Category(31531, "Acciaio", 5, 3153)
        val cucchIni = Category(3154, "Cucchiaini", 4, 315)
        val cucchIniAcc = Category(31541, "Acciaio", 5, 3154)
        val posServ = Category(3155, "Servizi", 4, 315)
        val posServAcc = Category(31551, "Acciaio", 5, 3155)
        val posAServ = Category(3156, "Posate a servire", 4, 315)
        val posAServAcc = Category(31561, "Acciaio", 5, 3156)
        val casServizi = Category(316, "Servizi", 3, 31)
        val maced = Category(3161, "Macedonia", 4, 316)
        val mondCott = Category(32, "mondo cottura", 2, 3)
        val mondCottColaz = Category(321, "Colazione", 3, 32)
        val mondCottColazCaff = Category(3211, "Caffettiere", 4, 321)
        val mondCottColazCaffAll = Category(32111, "Alluminio", 5, 3211)
        val mondCottColazTei = Category(3212, "Teiere tisaniere infusiere", 4, 321)
        val mondCottColazTeiAcc = Category(32121, "Acciaio", 5, 3212)
        val mondCottColazBoll = Category(3213, "Bollitori", 4, 321)
        val mondCottColazBollAll = Category(32131, "Alluminio", 5, 3213)
        val mondCottColazBrich = Category(3214, "Bricchi", 4, 321)
        val mondCottColazBrichAcc = Category(32141, "Acciaio", 5, 3214)
        val mondCottPent = Category(322, "Pentolame", 3, 32)
        val mondCottPentPad = Category(3221, "Padelle/wok", 4, 322)
        val mondCottPentPadAll = Category(32211, "Alluminio", 5, 3221)
        val mondCottPentTeg = Category(3222, "Tegame", 4, 322)
        val mondCottPentTegAll = Category(32221, "Alluminio", 5, 3222)
        val mondCottPentCas = Category(3223, "Casseruole", 4, 322)
        val mondCottPentCasAll = Category(32231, "Alluminio", 5, 3223)
        val mondCottPentPent = Category(3224, "Pentola", 4, 322)
        val mondCottPentPentAll = Category(32241, "Alluminio", 5, 3224)
        val mondCottPentCop = Category(3225, "Coperchi", 4, 322)
        val mondCottPentCopAll = Category(32251, "Alluminio", 5, 3225)
        val mondCottPentBatPent = Category(3226, "Batteria di pentole", 4, 322)
        val mondCottPentBatPentIno = Category(32261, "Inox", 5, 3226)
        val mondCottPentPentPress = Category(3227, "Pentole a pressione", 4, 322)
        val mondCottPentPentPressAll = Category(32271, "Alluminio", 5, 3227)
        val mondCottPentPias = Category(3228, "Piastre a pressione", 4, 322)
        val mondCottPentPiasAll = Category(32281, "Alluminio", 5, 3228)
        val mondCottPentStamp = Category(3229, "Articoli per forno stampi", 4, 322)
        val mondCottPentStampPorc = Category(32291, "Porcellana", 5, 3229)
        val mondCottPentAltr = Category(32201, "Altre pentole", 4, 322)
        val mondCottPentAltrAll = Category(322011, "Alluminio", 5, 32201)
        val mondCottPentTegl = Category(32202, "Teglie", 4, 322)
        val mondCottPentTeglAll = Category(322021, "Alluminio", 5, 32202)
        val casPrepCibi = Category(33, "Preparazione Cibi", 2, 3)
        val casPrepCibiColt = Category(331, "Coltelli", 3, 33)
        val casPrepCibiColtSpel = Category(3311, "Spelucchino", 4, 331)
        val casPrepCibiColtSpelAcc = Category(33111, "Acciaio", 5, 3311)
        val casPrepCibiColtBist = Category(3312, "Coltelli Bistecca", 4, 331)
        val casPrepCibiColtBistAcc = Category(33121, "Acciaio", 5, 3312)
        val casPrepCibiColtArr = Category(3313, "Coltelli Arrosti", 4, 331)
        val casPrepCibiColtArrAcc = Category(33131, "Acciaio", 5, 3313)
        val casPrepCibiColtPan = Category(3314, "Coltelli Pane", 4, 331)
        val casPrepCibiColtPanAcc = Category(33141, "Acciaio", 5, 3314)
        val casPrepCibiColtCuc = Category(3315, "Coltelli cucina", 4, 331)
        val casPrepCibiColtCucAcc = Category(33151, "Acciaio", 5, 3315)
        val casPrepCibiColtPros = Category(3316, "Coltelli prosciuttto", 4, 331)
        val casPrepCibiColtProsAcc = Category(33161, "Acciaio", 5, 3316)
        val casPrepCibiColtAltr = Category(3317, "Altri coltelli", 4, 331)
        val casPrepCibiColtAltrAcc = Category(33171, "Acciao", 5, 3317)
        val casPrepCibiColtCepp = Category(3318, "Ceppi/Servizi", 4, 331)
        val casPrepCibiColtCeppAcc = Category(33181, "Acciaio", 5, 3318)
        val casPrepCibiColtTrinc = Category(3319, "Trinciapollo", 3, 331)
        val casPrepCibiUtens = Category(332, "Utensili da cucina", 3, 33)
        val casPrepCibiUtensGrat = Category(3321, "Grattugie", 4, 332)
        val casPrepCibiUtensGratAcc = Category(33211, "Acciaio", 5, 3321)
        val casPrepCibiUtensForb = Category(3322, "Forbici da cucina", 4, 332)
        val casPrepCibiUtensForbLeg = Category(33221, "Legno", 5, 3322)
        val casPrepCibiUtensVar = Category(3323, "Utensili vari", 4, 332)
        val casPrepCibiUtensVarIno = Category(33231, "Inox", 5, 3323)
        val casPrepCibiUtensSchiac = Category(3324, "Schiaccia patate passaverdure", 4, 332)
        val casPrepCibiUtensSchiacLegn = Category(33241, "Legno", 5, 3324)
        val casPrepCibiUtensPast = Category(3325, "Macchine per pasta", 4, 332)
        val casPrepCibiUtensPastIno = Category(33251, "Inox", 5, 3325)
        val casPrepCibiUtensGas = Category(3326, "Accendi Gas", 4, 332)
        val casPrepCibiUtensGasGas = Category(33261, "Gas", 5, 3326)
        val casPrepCibiUtensTim = Category(3327, "Timer/termometri", 4, 332)
        val cons = Category(34, "Conservazione", 2, 3)
        val consCont = Category(341, "Contenitori", 3, 34)
        val consContFrigo = Category(3411, "Contenitori frigo/microonde", 4, 341)
        val consContFrigoPlas = Category(34111, "Palstica", 5, 3411)
        val consContBar = Category(3412, "Barattoli", 4, 341)
        val consContBarPlas = Category(34121, "Plastica", 5, 3412)
        val consContPort = Category(3413, "Porta torta/formaggio", 4, 341)
        val consBar = Category(342, "Barattoli", 3, 34)
        val consBarPals = Category(3421, "Palstica", 4, 342)
        val consFrig = Category(343, "Sacchetti frigo", 3, 34)
        val consFrigPlas = Category(3431, "Plastica", 4, 343)
        val consEno = Category(344, "Accessori per enologia", 3, 34)
        val consEnoTap = Category(3441, "Tappi", 4, 344)
        val accTavCuc = Category(35, "Accessori tavola/cucina", 2, 2)
        val accTavCucTav = Category(351, "Tavola", 3, 35)
        val accTavCucTavOl = Category(3511, "Oliera sale/pepe", 4, 351)
        val accTavCucCuc = Category(352, "Cucina", 3, 35)
        val accTavCucCucScola = Category(3521, "Scolapiatti", 4, 352)
        val accCasa = Category(36, "Accessori casa", 2, 3)
        val accCasaTrat = Category(361, "Trattamento sistemazione bicchieri", 3, 36)
        val accCasaTratPort = Category(3611, "Porta binacheria", 4, 361)
        val accCasaTratPortMol = Category(36111, "Mollette", 5, 3611)
        val accCasaTratTav = Category(3612, "Tavolo da stiro", 4, 361)
        val accCasaTratTavTel = Category(36121, "Teli da stiro", 5, 3612)
        val accCasaTratCont = Category(3613, "Contenitori biancheria", 4, 361)
        val accCasaAltr = Category(362, "Altri accessori", 3, 36)
        val accCasaAltrEv = Category(3621, "Evaporatori", 4, 362)

        //elettrodomestici

        val elettrodomestici = Category(1, "Elettrodomestici", 1, 0)
        val piccoli = Category(11, "Picoli", 2, 1)
        val curaPersona = Category(111, "Cura Persona", 3, 11)
        val rasViso = Category(1111, "Rasatura Viso", 4, 111)
        val ioni = Category(11111, "Ioni", 5, 1111)
        val bilPers = Category(1112, "Bilance Pesa Persone", 4, 111)
        val curaBambino = Category(112, "Cura Bambino", 3, 11)
        val scope = Category(1121, "Scope", 4, 112)
        val lavaPav = Category(11211, "Lavapavimenti", 5, 1121)
        val ferriDaStiro = Category(1122, "Ferri Da Stiro", 4, 112)
        val aVapore = Category(11221, "A vapore", 5, 1122)
        val sistemiStiranti = Category(1123, "Sistemi Stiranti", 4, 112)
        val aCaldaia = Category(11231, "A Caldaia", 5, 1123)
        val elettPrepCibi = Category(113, "Preparazione Cibi", 3, 11)
        val robot = Category(1131, "Robot Preparazione Cibi", 4, 113)
        val inox = Category(11311, "Inox", 5, 1131)
        val centirfughe = Category(1132, "Centrifughe", 4, 113)
        val cotturaCibi = Category(114, "Cottura Cibi", 3, 11)
        val macchineCaf = Category(1141, "Macchine Caffè espresso", 4, 114)
        val mokaElett = Category(11411, "Moka Elettrica", 5, 1141)
        val bollitori = Category(1142, "Bollitori", 4, 114)
        val polenta = Category(11421, "Polenta", 5, 1142)
        val cotAcc = Category(1143, "Accessori", 4, 114)
        val professionali = Category(12, "Professionali", 2, 1)


        val cat = arrayListOf(
            bricolage,
            giardinaggio,
            sisPiante,
            vasi,
            vasiPlast,
            sottoVasi,
            sottoVasiPlast,
            fioriere,
            decoraz,
            statFont,
            casalinghi,
            tavola,
            piatti,
            piano,
            pianoPorc,
            fondo,
            fondoPorc,
            frutta,
            fruttaPorc,
            pizza,
            pizzaPorc,
            piatServ,
            piatServPorc,
            accPort,
            sotVas,
            sotVasPorc,
            insZupp,
            insZuppPorc,
            ciotolina,
            ciotolinaPorc,
            accPortServ,
            accPortServVetOpal,
            colaz,
            tazCaff,
            tazCaffPorc,
            tazTe,
            tazTePorc,
            tazjumb,
            tazjumboPorc,
            tazmug,
            tazmugPorc,
            tazbol,
            tazbolPorc,
            colazServ,
            colazServPorc,
            accColazPorc,
            accColaz,
            bicchCalic,
            bichAcq,
            bichAcqVet,
            bichVino,
            bichVinoVet,
            bichBib,
            bichBibVet,
            bichBir,
            bichBirVet,
            bichLiq,
            bichLiqVet,
            calAcq,
            calAcqVet,
            calVino,
            calVinoVet,
            calFlut,
            calFlutVet,
            calBir,
            calBirVet,
            calLiq,
            calLiqVet,
            bichServ,
            bichServVet,
            bott,
            bottVet,
            decanter,
            decanterVet,
            altriCal,
            posate,
            forc,
            forcAcc,
            colt,
            coltAcc,
            cucch,
            cucchAcc,
            cucchIni,
            cucchIniAcc,
            posServ,
            posServAcc,
            posAServ,
            posAServAcc,
            casServizi,
            maced,
            mondCott,
            mondCottColaz,
            mondCottColazCaff,
            mondCottColazCaffAll,
            mondCottColazTei,
            mondCottColazTeiAcc,
            mondCottColazBoll,
            mondCottColazBollAll,
            mondCottColazBrich,
            mondCottColazBrichAcc,
            mondCottPent,
            mondCottPentPad,
            mondCottPentPadAll,
            mondCottPentTeg,
            mondCottPentTegAll,
            mondCottPentCas,
            mondCottPentCasAll,
            mondCottPentPent,
            mondCottPentPentAll,
            mondCottPentCop,
            mondCottPentCopAll,
            mondCottPentBatPent,
            mondCottPentBatPentIno,
            mondCottPentPentPress,
            mondCottPentPentPressAll,
            mondCottPentPias,
            mondCottPentPiasAll,
            mondCottPentStampPorc,
            mondCottPentStamp,
            mondCottPentAltr,
            mondCottPentAltrAll,
            mondCottPentTegl,
            mondCottPentTeglAll,
            casPrepCibi,
            casPrepCibiColt,
            casPrepCibiColtSpel,
            casPrepCibiColtSpelAcc,
            casPrepCibiColtBist,
            casPrepCibiColtBistAcc,
            casPrepCibiColtArr,
            casPrepCibiColtArrAcc,
            casPrepCibiColtPan,
            casPrepCibiColtPanAcc,
            casPrepCibiColtCuc,
            casPrepCibiColtCucAcc,
            casPrepCibiColtPros,
            casPrepCibiColtProsAcc,
            casPrepCibiColtAltr,
            casPrepCibiColtAltrAcc,
            casPrepCibiColtCepp,
            casPrepCibiColtCeppAcc,
            casPrepCibiColtTrinc,
            casPrepCibiUtens,
            casPrepCibiUtensGrat,
            casPrepCibiUtensGratAcc,
            casPrepCibiUtensForb,
            casPrepCibiUtensForbLeg,
            casPrepCibiUtensVar,
            casPrepCibiUtensVarIno,
            casPrepCibiUtensSchiac,
            casPrepCibiUtensSchiacLegn,
            casPrepCibiUtensPast,
            casPrepCibiUtensPastIno,
            casPrepCibiUtensGas,
            casPrepCibiUtensGasGas,
            casPrepCibiUtensTim,
            cons,
            consCont,
            consFrig,
            consContFrigo,
            consContFrigoPlas,
            consContBar,
            consContBarPlas,
            consContPort,
            consBar,
            consBarPals,
            consFrigPlas,
            consEno,
            consEnoTap,
            accTavCuc,
            accTavCucTav,
            accTavCucTavOl,
            accTavCucCuc,
            accTavCucCucScola,
            accCasa,
            accCasaTrat,
            accCasaTratPort,
            accCasaTratPortMol,
            accCasaTratTav,
            accCasaTratTavTel,
            accCasaTratCont,
            accCasaAltr,
            accCasaAltrEv,
            elettrodomestici,
            piccoli,
            curaPersona,
            rasViso,
            ioni,
            bilPers,
            curaBambino,
            scope,
            lavaPav,
            ferriDaStiro,
            aVapore,
            sistemiStiranti,
            aCaldaia,
            elettPrepCibi,
            robot,
            inox,
            centirfughe,
            cotturaCibi,
            macchineCaf,
            mokaElett,
            bollitori,
            polenta,
            cotAcc,
            professionali
        )

        return cat

    }

    suspend fun getSupplier(): MutableList<Supplier> {
        val items = mutableListOf<Supplier>()
        repeat(10) {
            items.add(Supplier("$it", "Fornitore qualisasi $it"))
        }

        return items

    }


    suspend fun getArticle(applicationContext: Context): List<Article> {
        val list = mutableListOf<Article>()

        val categories = makeCategories()

        val min = listOf(1, 4, 8, 6, 12, 24)

        val suppliers = getSupplier()

        val imagesList = applicationContext.getImagesList()

        val descrRandom = applicationContext.getDescrList()

        repeat(28682) {

            list.add(
                Article(
                    descrRandom[it].codArt,
                    descrRandom[it].description,
                    it * 100,
                    it * 2,
                    it.toFloat(),
                    min.shuffled()[0],
                    listOf(Package("a ciotola", 1), Package("Pallet", 24)),
                    Random.nextInt(0,150),
                    Random.nextInt(0,50),
                    "continuativo",
                    suppliers.shuffled()[0],
                    categories.shuffled()[0].id,
                    imagesList[Random.nextInt(0,28597)],
                    listOf(imagesList[Random.nextInt(0,28597)], imagesList[Random.nextInt(0,28597)], imagesList[Random.nextInt(0,28597)]),
                    null,
                    listOf(
                        Item(
                            min.shuffled()[0],
                            "Nome",
                            null,
                            null,
                            "56.9",
                            null,
                            null,
                            "17.5",
                            "56",
                            "22",
                            null,
                            "36.5",
                            null
                        )
                    )
                )
            )
        }
        return list
    }


    suspend fun getCatalogOption(context: Context): List<VisibilityOption> {
//        delay(5000)
        val list = mutableListOf<VisibilityOption>()
        val array = context.resources.getStringArray(R.array.catalog_options)
        array.forEach { list.add(VisibilityOption(it, false)) }
        return list
    }

    suspend fun getCustomers(): List<Customer> {

        val letters = listOf(
            "A",
            "B",
            "C",
            "D",
            "E",
            "F",
            "G",
            "H",
            "I",
            "L",
            "M",
            "N",
            "O",
            "P",
            "Q",
            "R",
            "S",
            "T",
            "U",
            "V",
            "Z"
        )

        val list = mutableListOf<Customer>()
        repeat(200) {
            list.add(
                Customer(
                    "${it * 100}",
                    "${letters.shuffled()[0]}client",
                    "12346789",
                    "123456789",
                    "123456789",
                    "email@gmail.com",
                    "emailaziendale@gmail.com",
                    "123456789123456789",
                    "Optimus Prime",
                    "Listino divino",
                    listOf(
                        Customer.Address(
                            "Sottomarino nucleare di f.ll Plutonio",
                            "via della perdizione, 69",
                            "Atlantide",
                            "OC",
                            "111111"
                        ),
                        Customer.Address(
                            "Casa di gigi e gigino",
                            "via della perdizione, 69",
                            "Atlantide",
                            "OC",
                            "111111"
                        ),
                        Customer.Address(
                            "Bat Caverna",
                            "via della perdizione, 69",
                            "Atlantide",
                            "OC",
                            "111111"
                        ),
                        Customer.Address(
                            "Vault 76",
                            "via della perdizione, 69",
                            "Atlantide",
                            "OC",
                            "111111"
                        )
                    )
                )
            )
        }
        return list
    }


    suspend fun getCustomerHistory(customerId: String): ResponseNetwork<List<CustomerHistory>> {

        delay(5000)

        val list = listOf(
            CustomerHistory(
                "12000",
                Date().time,
                listOf(
                    CustomerHistory.CustomerHistoryRow(29000, "Articolo nome", "FORN1", 12, 89F),
                    CustomerHistory.CustomerHistoryRow(27000, "Articolo nome", "FORN1", 12, 89F),
                    CustomerHistory.CustomerHistoryRow(26000, "Articolo nome", "FORN1", 12, 89F),
                    CustomerHistory.CustomerHistoryRow(25000, "Articolo nome", "FORN1", 12, 89F),
                    CustomerHistory.CustomerHistoryRow(24000, "Articolo nome", "FORN1", 12, 89F),
                    CustomerHistory.CustomerHistoryRow(23000, "Articolo nome", "FORN1", 12, 89F),
                    CustomerHistory.CustomerHistoryRow(22000, "Articolo nome", "FORN1", 12, 89F),
                    CustomerHistory.CustomerHistoryRow(21000, "Articolo nome", "FORN1", 12, 89F),
                    CustomerHistory.CustomerHistoryRow(20000, "Articolo nome", "FORN1", 12, 89F),
                    CustomerHistory.CustomerHistoryRow(19000, "Articolo nome", "FORN1", 12, 89F),
                    CustomerHistory.CustomerHistoryRow(18000, "Articolo nome", "FORN1", 12, 89F),
                    CustomerHistory.CustomerHistoryRow(17000, "Articolo nome", "FORN1", 12, 89F)
                )
            )
        )

        val response = Response.success(list)
        return ResponseNetwork.create(response)
    }

    suspend fun getDiscountedPrice(): ResponseNetwork<Float> {

        val response = Response.success(Random.nextInt(1, 90).toFloat())
        return ResponseNetwork.create(response)

    }

    suspend fun sendOrder(): ResponseNetwork<BooleanResponse> {

        delay(5000)

        val response = Response.success(BooleanResponse(0, "ok", null, null))
        return ResponseNetwork.create(response)
    }

    suspend fun login(username: String, password: String): ResponseNetwork<BooleanResponse> {

        delay(2000)

        val cods = listOf(
            "CMR",
            "RRT",
            "PIV",
            "ROCCO",
            "GIL",
            "ELT",
            "OOP",
            "SAS",
            "KOK",
            "LOL",
            "FFF",
            "WOP",
            "KOP",
            "ALL",
            "LAB"
        )

        val response = Response.success(BooleanResponse(0, cods.shuffled()[0], null, null))
        return ResponseNetwork.create(response)
    }


    suspend fun agentInfo(cod: String): ResponseNetwork<Agent> {

        val response = Response.success(Agent(cod, "Re", "Scorpione", "ildesertoprosciuga@hot.it"))
        return ResponseNetwork.create(response)
    }


}