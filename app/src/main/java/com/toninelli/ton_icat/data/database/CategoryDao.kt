package com.toninelli.ton_icat.data.database

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.toninelli.ton_icat.model.Category
import kotlinx.coroutines.CoroutineScope

@Dao
interface CategoryDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(category: Category)

    @Query("DELETE FROM Category")
    suspend fun delete()

    @Query("SELECT * FROM Category")
    suspend fun getAllCategories(): List<Category>

    @Query("SELECT * FROM Category WHERE id LIKE :id")
    suspend fun getCategById(id: Int): Category

}