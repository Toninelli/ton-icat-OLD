package com.toninelli.ton_icat.data.database

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.toninelli.ton_icat.model.Supplier

@Dao
interface SupplierDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(supplier: Supplier)

    @Query("SELECT * FROM Supplier")
    suspend fun getSuppliers(): List<Supplier>

    @Query("DELETE FROM Supplier")
    suspend fun delete()

}