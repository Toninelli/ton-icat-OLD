package com.toninelli.ton_icat.data.database

import android.content.Context
import android.os.Build
import android.provider.Settings
import androidx.room.Transaction
import com.google.gson.Gson
import com.toninelli.fileutil.logd
import com.toninelli.fileutil.nullIfEmpty
import com.toninelli.ton_icat.BuildConfig
import com.toninelli.ton_icat.data.Mock
import com.toninelli.ton_icat.extension.*
import com.toninelli.ton_icat.icatcontext.Constants
import com.toninelli.ton_icat.model.*
import com.toninelli.ton_icat.vo.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import java.util.*
import javax.inject.Inject
import kotlin.math.log


class DbRepo @Inject constructor(
    private val context: Context,
    private val catalogItemDao: CatalogItemDao,
    private val categoryDao: CategoryDao,
    private val customerDao: CustomerDao,
    private val supplierDao: SupplierDao,
    private val selectedCustomerDao: SelectedCustomerDao,
    private val orderDao: OrderDao,
    private val orderRowDao: OrderRowDao,
    private val offerDao: OfferDao,
    private val agentDao: AgentDao

) : DbRepository {


    /**
     * Offer
     */

    override suspend fun getOffers(): Either<Failure, List<Offer>> {
        return offerDao.getOffers().nullIfEmpty()?.let { right(it) } ?: left(Failure.EmptyData())
    }

    /**
     * Categ
     */

    private var categTree: CategoryTree? = null

    override suspend fun getCategories(): Either<Failure, CategoryTree> {
        return categTree?.let {
            if (it.list.isNotEmpty())
                right(it)
            else
                right(initCategoryTree())
        } ?: right(initCategoryTree())
    }

    private suspend fun initCategoryTree(): CategoryTree {
        categTree = CategoryTree(categoryDao.getAllCategories())
        return categTree!!
    }

    /**
     * Supplier
     */
    override suspend fun getSupplier(): Either<Failure, List<Supplier>> {
        return supplierDao.getSuppliers().nullIfEmpty()?.let { right(it) }
            ?: left(Failure.EmptyData())
    }

    /**
     * IcatOption
     */
    override suspend fun setVisibilityOption(visibilityOption: VisibilityOption?): Either<Failure, List<VisibilityOption>> {
        return context.getPreference().getVisibilityOptions()?.let { items ->
            visibilityOption?.let {
                val mutableItems = items.toMutableList()
                val index = mutableItems.indexOfFirst { it.name == visibilityOption.name }
                mutableItems[index] = visibilityOption
                context.getPreference().setVisibilityOptions(mutableItems)
                right(mutableItems)
            } ?: right(items)
        } ?: kotlin.run {
            Mock.getCatalogOption(context).let {
                context.getPreference().setVisibilityOptions(it)
                right(it)
            }
        }
    }

    /**
     * Catalog Item
     */
    override suspend fun getCatalogItemByCateg(
        categId: Int,
        limit: Int,
        offset: Int
    ): Either<Failure, List<CatalogItem>> {
        val categories = categTree!!.getAllSubCateg(categId, true).map { it.id }
        val articles = catalogItemDao.getCatalogItemByCateg(categories, limit, offset)
        return articles.nullIfEmpty()?.let { right(it) } ?: left(
            Failure.EmptyData()
        )
    }

    override suspend fun getCatalogItemByQuery(
        query: String,
        limit: Int,
        offset: Int
    ): Either<Failure, List<CatalogItem>> {

        val keywords = query.replace("\\s+".toRegex()," ").trim().split(" ")
        logd(keywords)

        var result = listOf<CatalogItem>()

        keywords.forEach {
            val list = catalogItemDao.getCatalogItemByQuery("%$it%")
            result = result.union(list).toList()
        }

        if (keywords.size > 1) {
            result = result.filter { "${it.article.descr} ${it.article.artCod}".match(keywords) }
        }

        return result.sortedBy { it.article.descr }.nullIfEmpty()?.let { right(it) }
            ?: left(Failure.EmptyData())
    }

    override suspend fun getCatalogItemBySupplier(
        supplierId: String,
        limit: Int,
        offset: Int
    ): Either<Failure, List<CatalogItem>> {
        return catalogItemDao.getCatalogItemBySupplier(supplierId, limit, offset)
            .sortedBy { it.article.descr }
            .nullIfEmpty()?.let { right(it) } ?: left(
            Failure.EmptyData()
        )
    }

    override suspend fun getCatalogItemByCods(cods: List<Int>): Either<Failure, List<CatalogItem>> {
        return catalogItemDao.getCatalogItemByCods(cods).nullIfEmpty()?.let { right(it) } ?: left(
            Failure.EmptyData(
                "Codice non valido"
            )
        )
    }

    /**
     * Sync Dialog
     */
    override suspend fun getLastSyncCatalog(): Either<Failure, SyncLog> {
        val shared = context.getPreference()
        return shared.getString(Constants.KEY_SYNC_LOG, null)?.let {
            right(Gson().fromJson(it, SyncLog::class.java))
        } ?: left(Failure.EmptyData())
    }


    /**
     * agent
     */

    override suspend fun getCredential(): Either<Failure, Credential> {
        return right(context.getPreference().getCredential())
    }

    override suspend fun setCredential(credential: Credential): Either<Failure, BooleanResult> {
        context.getPreference().setCredential(credential)
        return right(BooleanResult(true, "Credenziali salvate"))
    }

    override suspend fun getAgent(): Either<Failure, Agent> {
        return agentDao.getAgent()?.let { right(it) } ?: left(Failure.EmptyData())
    }

    override suspend fun saveAgent(agent: Agent): Either<Failure, BooleanResult> {
        return if (agentDao.insert(agent) > 0)
            right(BooleanResult(true, "Agente salvato"))
        else
            right(BooleanResult(false, "Agente non salvato"))
    }

    override suspend fun chekUniqueAgent(newAgent: Agent): Either<Failure, BooleanResult> {
        return agentDao.getAgent()?.let {
            right(BooleanResult(it.cod == newAgent.cod, "Agente controllato"))
        } ?: kotlin.run {
            agentDao.insert(newAgent)
            right(BooleanResult(true, "Agente non ancora salvato"))
        }
    }

    /**
     * Customer
     */
    override suspend fun getAllCustomers(): Either<Failure, List<Customer>> {

        return customerDao.getAllCustomers().nullIfEmpty()?.let { right(it) }
            ?: left(Failure.EmptyData())
    }

    override suspend fun getSelectedCustomer(): Flow<Either<Failure, SelectedCustomer>> {

        return selectedCustomerDao.getSelectedCustomer()
            .either { Failure.Error(it.localizedMessage ?: "Unknown Error") }
    }

    @Transaction
    override suspend fun selectCustomer(
        customerId: String,
        select: Boolean
    ): Either<Failure, Boolean> {

        selectedCustomerDao.remove()

        if (select) {
            selectedCustomerDao.insert(SelectedCustomer(customerId = customerId)).toInt()
        }

        return right(true)
    }

    override suspend fun getCustomerById(customerId: String): Either<Failure, Customer> {

        return customerDao.getCustomerById(customerId)?.let {
            right(it)
        } ?: left(Failure.EmptyData())
    }

    override suspend fun getCustomerFlowById(customerId: String): Flow<Either<Failure, Customer>> {
        return customerDao.getCustomerFlowById(customerId)
            .either { Failure.Error(it.localizedMessage ?: "Unknown Error") }.distinctUntilChanged()
    }

    override suspend fun getAllCustomersDoingOrder(): Flow<Either<Failure, List<Customer>>> {

        return orderDao.getAllCustomerIdDoingOrder().distinctUntilChanged().transform {

            val mutableSet = mutableSetOf<Customer>()

            it.forEach { customerId ->
                mutableSet.add(customerDao.getCustomerById(customerId)!!)
            }

            emit(mutableSet.sortedBy { it.name })
        }.either { Failure.Error(it.localizedMessage ?: "Unknown Error") }

    }

    override suspend fun updatePreferredAddress(
        customerId: String,
        address: Customer.Address
    ): Either<Failure, SingleEventWrapper<BooleanResult>> {
        return if (customerDao.updatePreferredAddess(customerId, address) > 0)
            right(SingleEventWrapper(BooleanResult(true, "Indirizzo preferito aggiornato")))
        else
            right(SingleEventWrapper(BooleanResult(false, "Indirizzo preferito non aggiornato")))
    }

    /**
     * Order
     */
    override suspend fun getAllOrders(): Flow<Either<Failure, List<Order>>> {
        return orderDao.getAllOrders()
            .either { Failure.Error(it.localizedMessage ?: "Unknown Error") }.distinctUntilChanged()
    }

    override suspend fun getCustomerOrdersByStatus(
        customerId: String?,
        state: Order.State
    ): Flow<Either<Failure, List<Order>>> {
        return flow<Either<Failure, List<Order>>> {
            customerId?.let {
                emitAll(orderDao.getCustomerOrdersByStatus(it, state).either { Failure.Error(it.localizedMessage ?: "Unknown Error") }.distinctUntilChanged())
            } ?: emit(left(Failure.EmptyData()))
        }

    }

    override suspend fun getOrdersByCustomer(customerId: String): Flow<Either<Failure, List<Order>>> {
        return orderDao.getOrdersByCustomer(customerId)
            .either { Failure.Error(it.localizedMessage ?: "Unknown Error") }.distinctUntilChanged()
    }

    override suspend fun getOrderById(orderId: Long): Flow<Either<Failure, Order>> {
        return orderDao.getOrderByIdFlow(orderId)
            .either { Failure.Error(it.localizedMessage ?: "Unknown error") }.distinctUntilChanged()
    }

    override suspend fun deleteOrder(orderId: Long): Either<Failure, SingleEventWrapper<BooleanResult>> {
        return if (orderDao.delete(orderDao.getOrderById(orderId)!!) > 0)
            right(SingleEventWrapper(BooleanResult(true, "Ordine cancellato")))
        else
            right(SingleEventWrapper(BooleanResult(false, "Ordine non cancellato")))
    }

    override suspend fun insertOrder(customerId: String): Either<Failure, SingleEventWrapper<BooleanResult>> {
        val creationDate = Date()
        val order = Order(
            creationDate.time,
            Order.State.NOT_COMPLETED,
            Order.Type.ORDER,
            customerId,
            customerDao.getCustomerById(customerId)?.assigned_agent!!,
            creationDate,
            null,
            null,
            customerDao.getCustomerById(customerId)?.preferredAddress
        )

        return if (orderDao.insert(order) > 0)
            right(SingleEventWrapper(BooleanResult(true, "Ordine Inserito")))
        else
            right(SingleEventWrapper(BooleanResult(false, "Inserimento ordine non riuscito")))
    }

    override suspend fun updateOrderType(
        orderId: Long,
        type: Order.Type
    ): Either<Failure, SingleEventWrapper<BooleanResult>> {
        return if (orderDao.updateType(orderId, type) > 0)
            right(SingleEventWrapper(BooleanResult(true, "Tipo ordine aggiornato")))
        else right(SingleEventWrapper(BooleanResult(false, "Tipo ordine non aggiornato")))
    }

    override suspend fun updateOrderDestination(
        orderId: Long,
        destination: Customer.Address
    ): Either<Failure, SingleEventWrapper<BooleanResult>> {
        return if (orderDao.updateDestination(orderId, destination) > 0)
            right(SingleEventWrapper(BooleanResult(true, "Destinazione ordine aggiornato")))
        else right(SingleEventWrapper(BooleanResult(false, "Destinazione ordine non aggiornato")))
    }

    override suspend fun updateOrderDelivery(
        orderId: Long,
        delivery: Order.Delivery
    ): Either<Failure, SingleEventWrapper<BooleanResult>> {
        return if (orderDao.updateDelivery(orderId, delivery) > 0)
            right(SingleEventWrapper(BooleanResult(true, "Consegna ordine aggiornato")))
        else right(SingleEventWrapper(BooleanResult(false, "Consegna ordine non aggiornato")))
    }

    override suspend fun updateOrderState(
        orderId: Long,
        state: Order.State
    ): Either<Failure, SingleEventWrapper<BooleanResult>> {
        return if (orderDao.updateState(orderId, state) > 0)
            right(SingleEventWrapper(BooleanResult(true, "Stato ordine aggiornato")))
        else
            right(SingleEventWrapper(BooleanResult(false, "Stato ordine non aggiornato")))
    }

    override suspend fun updateTransmissionDate(
        orderId: Long,
        date: Date
    ): Either<Failure, SingleEventWrapper<BooleanResult>> {
        return if (orderDao.updateTransmissionDate(orderId, date) > 0)
            right(SingleEventWrapper(BooleanResult(true, "Data trasmissione aggiornata")))
        else
            right(SingleEventWrapper(BooleanResult(false, "Data trasmssione non aggiornato")))
    }

    override suspend fun updateOrderNote(
        orderId: Long,
        note: String
    ): Either<Failure, BooleanResult> {
        return if (orderDao.updateNote(orderId, note) > 0)
            right(BooleanResult(true, "Note aggiornata"))
        else
            right(BooleanResult(false, "Note non aggiornato"))
    }

    /**
     * OrderRow
     */
    override suspend fun insertOrderRow(orderRow: OrderRow): Either<Failure, SingleEventWrapper<BooleanResult>> {

        return if (orderRowDao.insert(orderRow) > 0) {
            orderDao.increaseNRows(orderRow.orderId!!)
            right(SingleEventWrapper(BooleanResult(true, "Riga Inserita")))
        } else
            right(SingleEventWrapper(BooleanResult(false, "Inserimento riga non riuscito")))
    }

    override suspend fun getOrderRowByOrder(orderId: Long): Flow<Either<Failure, List<OrderRow>>> {
        return orderRowDao.getOrderRowByOrder(orderId)
            .either { Failure.Error(it.localizedMessage ?: "Unknown Error") }.distinctUntilChanged()
    }

    override suspend fun getOrderRowSizeByOrder(orderId: Long): Flow<Either<Failure, Int>> {
        return orderRowDao.getOrderRowSize(orderId)
            .either { Failure.Error(it.localizedMessage ?: "Unknown Error") }.distinctUntilChanged()
    }

    override suspend fun deleteOrderRow(orderRow: OrderRow): Either<Failure, SingleEventWrapper<BooleanResult>> {

        return if (orderRowDao.delete(orderRow) > 0) {
            orderDao.decreaseNRows(orderRow.orderId!!)
            right(SingleEventWrapper(BooleanResult(true, "Riga cancellata")))
        } else
            right(SingleEventWrapper(BooleanResult(false, "Riga non cancellata")))
    }

    override suspend fun orderRowSizeByCustomer(customerId: String): Either<Failure, Map<Long, Int>> {
        val ids = orderRowDao.getOrderRowSizeByCustomer(customerId)

        return ids.nullIfEmpty()?.let {
            val groupedIds = ids.distinct()
            val map = mutableMapOf<Long, Int>()

            groupedIds.forEach { id ->
                val rows = ids.count { it == id }
                map[id] = rows
            }
            right(map)
        } ?: left(Failure.EmptyData())

    }

    override suspend fun insertDiscountPrice(
        id: Long,
        discountPrice: Float
    ): Either<Failure, BooleanResult> {

        orderRowDao.insertDiscountPrice(id, discountPrice)
        return right(BooleanResult(true, "Sconto applicato"))

    }


}