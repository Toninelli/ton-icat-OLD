package com.toninelli.ton_icat.data.database

import androidx.room.*
import com.toninelli.ton_icat.model.Customer
import kotlinx.coroutines.flow.Flow


@Dao
interface CustomerDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(customer: Customer)

    @Query("SELECT * FROM Customer ORDER BY name ASC")
    suspend fun getAllCustomers(): List<Customer>

    @Query("SELECT * FROM Customer WHERE customerId LIKE :id")
    suspend fun getCustomerById(id: String): Customer?

    @Query("SELECT * FROM Customer WHERE customerId LIKE :id")
    fun getCustomerFlowById(id: String): Flow<Customer>

    @Query("UPDATE Customer SET preferredAddress = :address WHERE customerId LIKE :customerId")
    suspend fun updatePreferredAddess(customerId: String, address: Customer.Address): Int

    @Query("DELETE FROM Customer")
    suspend fun delete()

}