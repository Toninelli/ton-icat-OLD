package com.toninelli.ton_icat.data.network

import android.content.Context
import android.content.ContextWrapper
import android.graphics.Bitmap
import androidx.core.graphics.drawable.toBitmap
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.target.Target
import com.google.gson.Gson
import com.toninelli.fileutil.logd

import com.toninelli.ton_icat.data.Mock
import com.toninelli.ton_icat.data.database.*
import com.toninelli.ton_icat.extension.getPreference
import com.toninelli.ton_icat.icatcontext.Constants
import com.toninelli.ton_icat.model.*
import com.toninelli.ton_icat.vo.Status
import kotlinx.coroutines.*
import kotlinx.coroutines.channels.ProducerScope
import kotlinx.coroutines.flow.*
import java.io.File
import java.io.FileOutputStream
import java.util.*
import javax.inject.Inject
import javax.inject.Named
import kotlin.collections.ArrayList
import kotlin.math.log

class SyncIcat @Inject constructor(
    @Named("sync")
    private val api: SyncApi,
    private val categoryDao: CategoryDao,
    private val catalogItemDao: CatalogItemDao,
    private val customerDao: CustomerDao,
    private val supplierDao: SupplierDao,
    private val offerDao: OfferDao,
    private val applicationContext: Context
) {

    private val glide = Glide.with(applicationContext)

    suspend fun start() = channelFlow {

        logSyncStatus(SyncLog(SyncStatus.NOT_COMPLETED, Date()))

        coroutineScope {

            launch { delete() }
            launch { sync(this@channelFlow) }
        }

        logSyncStatus(SyncLog(SyncStatus.COMPLETED, Date()))
        send(SyncProgress(status = Status.SUCCESS))
    }

    /**
     * Sync
     */
    private suspend fun sync(producerScope: ProducerScope<SyncProgress>) {

        coroutineScope {
            launch { syncCateg(producerScope) }
            launch { syncCustomer(producerScope) }
            launch { syncSupplier(producerScope) }
            launch { syncCatalog(producerScope) }
        }
    }

    /**
     * Supplier
     */
    private suspend fun syncSupplier(producerScope: ProducerScope<SyncProgress>) {
        val downloadedSupplier = downloadSupplier()
        saveSupplier(producerScope, downloadedSupplier)
    }

    private suspend fun saveSupplier(
        producerScope: ProducerScope<SyncProgress>,
        downloadedSupplier: List<Supplier>
    ) {
        val totalSize = downloadedSupplier.size
        var count = 0
        downloadedSupplier.forEach {
            supplierDao.insert(it)
            delay(100)
            count++
            producerScope.send(
                SyncProgress(
                    SyncProgress.Type.SUPPLIER,
                    Status.LOADING,
                    count,
                    totalSize
                )
            )

        }

    }

    private suspend fun downloadSupplier(): List<Supplier> {
        return Mock.getSupplier()
    }

    /**
     * Customer
     */
    private suspend fun syncCustomer(producerScope: ProducerScope<SyncProgress>) {
        val downloadedCustomers = downloadCustomers()
        saveCustomers(producerScope, downloadedCustomers)
    }

    private suspend fun saveCustomers(
        producerScope: ProducerScope<SyncProgress>,
        downloadedCustomers: List<Customer>
    ) {
        val totalSize = downloadedCustomers.size
        var count = 0

        downloadedCustomers.forEach {
            if(it.address.size == 1)
                it.preferredAddress = it.address[0]
            customerDao.insert(it)
            delay(100)
            count++
            producerScope.send(
                SyncProgress(
                    SyncProgress.Type.CUSTOMER,
                    Status.LOADING,
                    count,
                    totalSize
                )
            )
        }

    }

    private suspend fun downloadCustomers(): List<Customer> {
        return Mock.getCustomers()
    }


    /**
     * Catalog
     */
    private suspend fun syncCatalog(producerScope: ProducerScope<SyncProgress>) {
        val downloadedArticles = downloadArticle()
        val downloadedOffers = donwloadOffer()
        saveCatalog(downloadedArticles, downloadedOffers, producerScope)
    }

    private suspend fun donwloadOffer(): List<Offer> {
        return Mock.offers()
    }

    private suspend fun downloadArticle(): List<Article> {
        return Mock.getArticle(applicationContext)
    }

    private suspend fun saveCatalog(
        articles: List<Article>,
        offers: List<Offer>,
        producerScope: ProducerScope<SyncProgress>
    ) {
        val totalSize = articles.size
        var count = 0

        offerDao.insert(offers)
        articles.forEach { art ->
            saveImages(art)
            val item = CatalogItem(art)
            findOffer(item, offers)
            catalogItemDao.insert(item)
            logd("Download $count")
            producerScope.send(
                SyncProgress(
                    SyncProgress.Type.CATALOG,
                    Status.LOADING,
                    count,
                    totalSize
                )
            )
            count++
        }
    }

    private fun findOffer(
        item: CatalogItem,
        offers: List<Offer>
    ) {

        val offer = offers.firstOrNull { it.getArtOffer(item.article.artCod) != null }
        offer?.let {
            val artOffer = it.getArtOffer(item.article.artCod)!!
            with(item) {
                offerName = it.name
                offerPrice = artOffer.price
                offerQta = artOffer.qta
            }
        }
    }

    private fun saveImages(art: Article) {

        val dir = ContextWrapper(applicationContext).getDir(
            Constants.DIR_ART_IMAGES,
            Context.MODE_PRIVATE
        )

        val totalList = art.images.toMutableList()
        totalList.add(art.image)

        totalList.forEach {
            try {
//                val url = "https:/picsum.photos/id/1/400/400"
                val url = "https://extranet.toninelli.it/foto/${it}m.jpg"
                val file = glide.download(url).submit().get()
                val newFile = File(dir, "${it.substringAfter("/")}.jpg")
                file.renameTo(newFile)
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
//        clear()
    }

    /**
     * Categ
     */
    private suspend fun syncCateg(producerScope: ProducerScope<SyncProgress>) {

        val downloadedCateg = downloadCateg()
        saveCateg(downloadedCateg, producerScope)
    }

    private suspend fun saveCateg(
        list: ArrayList<Category>,
        producerScope: ProducerScope<SyncProgress>
    ) {
        val totalSize = list.size
        var count = 0

        list.forEach { cat ->
            categoryDao.insert(cat)
            delay(100)
            count++
            producerScope.send(
                SyncProgress(
                    SyncProgress.Type.CATEGORY,
                    Status.LOADING,
                    count,
                    totalSize
                )
            )
        }

    }

    private suspend fun downloadCateg(): ArrayList<Category> {
        return Mock.makeCategories()
    }

    /**
     * Delete
     */
    private suspend fun delete() {

        coroutineScope {
            launch { categoryDao.delete() }
            launch { catalogItemDao.delete(); offerDao.delete() }
            launch { customerDao.delete() }
            launch { supplierDao.delete() }
            launch {
                ContextWrapper(applicationContext).getDir(
                    Constants.DIR_ART_IMAGES,
                    Context.MODE_PRIVATE
                ).deleteRecursively()
            }
        }
    }


    private fun clear() {
        Glide.get(applicationContext).clearDiskCache()
    }

    /**
     * LOG
     */
    private fun logSyncStatus(log: SyncLog) {
        val shared = applicationContext.getPreference()
        with(shared.edit()) {
            putString(Constants.KEY_SYNC_LOG, Gson().toJson(log))
            apply()
        }
    }

}
