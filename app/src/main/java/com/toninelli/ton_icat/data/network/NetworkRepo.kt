package com.toninelli.ton_icat.data.network

import android.content.Context
import com.example.networkcalladapterlib.ResponseNetworkEmpty
import com.example.networkcalladapterlib.ResponseNetworkError
import com.example.networkcalladapterlib.ResponseNetworkSuccess
import com.toninelli.ton_icat.data.Mock
import com.toninelli.ton_icat.extension.composeErrorString
import com.toninelli.ton_icat.model.*
import com.toninelli.ton_icat.vo.*
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class NetworkRepo @Inject constructor(
    val api: RemoteApi,
    private val syncIcat: SyncIcat,
    val context: Context
) : NetworkRepository {

    override suspend fun getPdfOffer(): Either<Failure, List<Pdf>> {
        return when (val response = Mock.pdfOffer()) {
            is ResponseNetworkSuccess -> {
                right(response.body)
            }
            is ResponseNetworkEmpty -> {
                left(Failure.Error(response.message))
            }
            is ResponseNetworkError -> {
                left(Failure.Error(response.exception.localizedMessage))
            }
        }
    }

    override suspend fun syncCatalog(): Flow<Either<Failure, SyncProgress>> {
        return syncIcat.start()
            .either { Failure.SyncCatalogError(it.localizedMessage ?: "Unknown Error") }
    }

    override suspend fun getDiscountedPrice(
        customerId: String,
        artId: Int
    ): Either<Failure, Float> {

//        return when (val response = Mock.getDiscountedPrice()) {
//            is ResponseNetworkSuccess -> right(response.body)
//            is ResponseNetworkEmpty -> left(Failure.EmptyData(response.message))
//            is ResponseNetworkError -> left(
//                Failure.Error(
//                    response.exception.localizedMessage ?: "Unknown Error"
//                )
//            )
//        }

        return right(3F)

//        return left(Failure.NoConnection)

    }

    override suspend fun sendOrder(): Either<Failure, BooleanResult> {
        return when (val response = Mock.sendOrder()) {
            is ResponseNetworkSuccess -> {
                when (response.body.error) {
                    0 -> right(BooleanResult(true, "Ordine Inviato"))
                    1 -> right(BooleanResult(false, response.body.errors.composeErrorString()))
                    else -> right(BooleanResult(false, "Invio ordine fallito, causa sconosciuta"))
                }
            }
            is ResponseNetworkEmpty -> left(Failure.EmptyData())
            is ResponseNetworkError -> left(Failure.NetworkError(response.exception.message!!))
        }
    }

    override suspend fun login(username: String, password: String): Either<Failure, Agent> {

        return when (val response = Mock.login(username, password)) {
            is ResponseNetworkSuccess -> {
                when (response.body.error) {
                    0 -> {
                        when (val agentResponse = Mock.agentInfo(response.body.value!!)) {
                            is ResponseNetworkSuccess -> {
                                right(agentResponse.body)
                            }
                            is ResponseNetworkEmpty -> left(Failure.UnknownError)
                            is ResponseNetworkError -> left(Failure.NetworkError(agentResponse.exception.message!!))
                        }
                    }
                    1 -> left(Failure.LoginError(response.body.errors.composeErrorString()))
                    else -> left(Failure.LoginError("Login non riuscito, causa sconosciuta"))
                }
            }
            is ResponseNetworkEmpty -> {
                left(Failure.UnknownError)
            }

            is ResponseNetworkError -> {
                left(Failure.NetworkError(response.exception.message!!))
            }
        }
    }

    override suspend fun getCustomerHistory(customerId: String): Either<Failure, List<CustomerHistory>> {
        return when(val response = Mock.getCustomerHistory(customerId)){
            is ResponseNetworkSuccess -> right(response.body)
            is ResponseNetworkEmpty -> left(Failure.EmptyData())
            is ResponseNetworkError -> left(Failure.NetworkError(response.exception.message!!))
        }
    }
}


