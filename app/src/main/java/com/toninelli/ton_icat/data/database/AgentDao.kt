package com.toninelli.ton_icat.data.database

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.toninelli.ton_icat.model.Agent

@Dao
interface AgentDao{


    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(agent: Agent): Long

    @Query("SELECT * FROM Agent")
    suspend fun getAgent(): Agent?

}