package com.toninelli.ton_icat.data.database

import androidx.room.*
import com.toninelli.ton_icat.model.Customer
import com.toninelli.ton_icat.model.Order
import kotlinx.coroutines.flow.Flow
import java.util.*

@Dao
interface OrderDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(order: Order): Long

    @Delete
    suspend fun delete(order: Order): Int

    @Query("SELECT * FROM `Order` WHERE customerId LIKE :customerId ORDER BY orderId DESC")
    fun getOrdersByCustomer(customerId: String): Flow<List<Order>>

    @Query("SELECT * FROM `Order` WHERE customerId LIKE :customerId AND state LIKE :state ORDER BY orderId DESC")
    fun getCustomerOrdersByStatus(customerId: String, state: Order.State): Flow<List<Order>>

    @Query("SELECT * FROM `Order` WHERE orderId LIKE :id")
    fun getOrderByIdFlow(id: Long): Flow<Order>

    @Query("SELECT * FROM `Order` WHERE orderId LIKE :id")
    suspend fun getOrderById(id: Long): Order?

    @Query("SELECT * FROM `Order` ORDER BY orderId DESC")
    fun getAllOrders(): Flow<List<Order>>

    @Query("SELECT customerId FROM `Order`")
    fun getAllCustomerIdDoingOrder(): Flow<List<String>>

    @Query("UPDATE `Order` SET nRows = nRows + 1 WHERE orderId LIKE :orderId")
    suspend fun increaseNRows(orderId: Long): Int

    @Query("UPDATE `Order` SET nRows = nRows - 1 WHERE orderId LIKE :orderId")
    suspend fun decreaseNRows(orderId: Long): Int

    @Query("UPDATE `Order` SET type = :type WHERE orderId LIKE :orderId")
    suspend fun updateType(orderId: Long, type: Order.Type): Int

    @Query("UPDATE `Order` SET destination = :destination WHERE orderId LIKE :orderId")
    suspend fun updateDestination(orderId: Long, destination: Customer.Address): Int

    @Query("UPDATE `Order` SET delivery = :delivery WHERE orderId LIKE :orderId")
    suspend fun updateDelivery(orderId: Long, delivery: Order.Delivery): Int

    @Query("UPDATE `Order` SET state = :state WHERE orderId LIKE :orderId")
    suspend fun updateState(orderId: Long, state: Order.State): Int

    @Query("UPDATE `Order` SET transmissionDate = :date WHERE orderId LIKE :orderId")
    suspend fun updateTransmissionDate(orderId: Long, date: Date): Int

    @Query("UPDATE `Order` SET note = :note WHERE orderId LIKE :orderId")
    suspend fun updateNote(orderId: Long, note: String): Int

}