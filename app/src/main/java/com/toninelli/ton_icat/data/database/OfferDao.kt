package com.toninelli.ton_icat.data.database

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.toninelli.ton_icat.model.Offer

@Dao
interface OfferDao {


    @Insert
    suspend fun insert(list: List<Offer>)

    @Query("SELECT * FROM Offer")
    suspend fun getOffers(): List<Offer>

    @Query("DELETE FROM Offer")
    suspend fun delete()

}