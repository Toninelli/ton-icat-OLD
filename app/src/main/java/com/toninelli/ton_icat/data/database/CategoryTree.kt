package com.toninelli.ton_icat.data.database

import com.toninelli.ton_icat.di.scope.ApplicationScope
import com.toninelli.ton_icat.model.Category
import javax.inject.Inject

class CategoryTree constructor(val list: List<Category>) {

    data class Node(
        val data: Category,
        val parent: Node?,
        var leftChild: Node?,
        var rightSibling: Node?
    )

    private val root = Node(Category(0, "root", 0, 0), null, null, null)


    init {
        populateTree(root,list.toMutableList())
    }

    private fun populateTree(node: Node, mutableCategories: MutableList<Category>) {
        val nodeId = node.data.id
        val leftChild = mutableCategories.firstOrNull { it.parent == nodeId }
        val rightSibling = mutableCategories.firstOrNull { it.parent == node.parent?.data?.id }
        leftChild?.let {
            mutableCategories.remove(it)
            val newNode = Node(it, node, null, null)
            node.leftChild = newNode
            populateTree(newNode, mutableCategories)
        }
        rightSibling?.let {
            mutableCategories.remove(it)
            val newNode = Node(it, node.parent, null, null)
            node.rightSibling = newNode
            populateTree(newNode, mutableCategories)
        }
    }

    fun getSubCateg(id: Int): List<Category> {
//        if (root.leftChild == null)
//            init()

        val node = searchNode(id, root)
        val list = mutableListOf<Category>()
        node?.let {
            levelorderTraversal(it.leftChild, list)
        }

        return list
    }

    fun getAllSubCateg(id: Int, includeParent: Boolean = false): List<Category> {
//        if (root.leftChild == null)
//            init()

        val node = searchNode(id, root)
        val list = mutableListOf<Category>()
        node?.let {
            if (includeParent) list.add(it.data)
            preorderTraversal(it.leftChild, list)
        }
        return list
    }


    private fun preorderTraversal(node: Node?, cat: MutableList<Category>) {
        node?.let {
            cat.add(it.data)
            preorderTraversal(it.leftChild, cat)
            preorderTraversal(it.rightSibling, cat)
        }
    }

    private fun levelorderTraversal(node: Node?, cat: MutableList<Category>) {
        node?.let {
            cat.add(it.data)
            levelorderTraversal(it.rightSibling, cat)
        }
    }

    private fun searchNode(id: Int, node: Node?): Node? {
        if (node == null || node.data.id == id)
            return node

        val leftChild = searchNode(id, node.leftChild)
        if (leftChild != null)
            return leftChild

        return searchNode(id, node.rightSibling)
    }


}