package com.toninelli.ton_icat.data.database

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverter
import androidx.room.TypeConverters
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.toninelli.ton_icat.model.*
import com.toninelli.ton_icat.model.Article.Package
import java.util.*

@Database(
    entities = [
        Category::class,
        CatalogItem::class,
        Customer::class,
        SelectedCustomer::class,
        Order::class, OrderRow::class,
        Supplier::class,
        Offer::class,
        Agent::class],
    version = 1,
    exportSchema = false
)
@TypeConverters(Converters::class)
abstract class AppDatabase : RoomDatabase() {

    abstract fun categoryDao(): CategoryDao

    abstract fun catalogItemDao(): CatalogItemDao

    abstract fun customerDao(): CustomerDao

    abstract fun selectedCustomerDao(): SelectedCustomerDao

    abstract fun orderDao(): OrderDao

    abstract fun orderRowDao(): OrderRowDao

    abstract fun supplierDao(): SupplierDao

    abstract fun offerDao(): OfferDao

    abstract fun agentDao(): AgentDao

}

class Converters {
    @TypeConverter
    fun fromSupplier(value: Supplier?): String? {
        return value?.let {
            val gson = Gson()
            gson.toJson(it)

        }
    }

    @TypeConverter
    fun toSupplier(value: String?): Supplier? {
        return value?.let {
            val gson = Gson()
            gson.fromJson(value, Supplier::class.java)
        }
    }

    @TypeConverter
    fun fromPackageList(value: List<Package>?): String? {
        return value?.let {
            val gson = Gson()
            gson.toJson(it)
        }
    }

    @TypeConverter
    fun toPackageList(value: String?): List<Package>? {
        return value?.let {
            val gson = Gson()
            val sType = object : TypeToken<List<Package>>() {}.type
            gson.fromJson(value, sType)
        }
    }

    @TypeConverter
    fun fromItemList(value: List<Article.Item>?): String? {
        return value?.let {
            val gson = Gson()
            gson.toJson(it)
        }
    }

    @TypeConverter
    fun toItemList(value: String?): List<Article.Item>? {
        return value?.let {
            val gson = Gson()
            val sType = object : TypeToken<List<Article.Item>>() {}.type
            gson.fromJson(value, sType)
        }
    }

    @TypeConverter
    fun fromShippingAddressList(value: List<Customer.Address>?): String? {
        return value?.let {
            val gson = Gson()
            gson.toJson(it)
        }
    }

    @TypeConverter
    fun toShippingAddressList(value: String?): List<Customer.Address>? {
        return value?.let {
            val gson = Gson()
            val sType = object : TypeToken<List<Customer.Address>>() {}.type
            gson.fromJson(value, sType)
        }
    }


    @TypeConverter
    fun fromIcatItemList(value: List<CatalogItem>?): String? {
        return value?.let {
            val gson = Gson()
            gson.toJson(it)
        }
    }

    @TypeConverter
    fun toIcatItemList(value: String?): List<CatalogItem>? {
        return value?.let {
            val gson = Gson()
            val sType = object : TypeToken<List<CatalogItem>>() {}.type
            gson.fromJson(value, sType)
        }
    }

    @TypeConverter
    fun fromStringList(value: List<String>?): String? {
        return value?.let {
            val gson = Gson()
            gson.toJson(it)
        }
    }

    @TypeConverter
    fun toStringList(value: String?): List<String>? {
        return value?.let {
            val gson = Gson()
            val sType = object : TypeToken<List<String>>() {}.type
            gson.fromJson(value, sType)
        }
    }

    @TypeConverter
    fun fromArtOfferist(value: List<ArtOffer>?): String? {
        return value?.let {
            val gson = Gson()
            gson.toJson(it)
        }
    }

    @TypeConverter
    fun toArtOfferList(value: String?): List<ArtOffer>? {
        return value?.let {
            val gson = Gson()
            val sType = object : TypeToken<List<ArtOffer>>() {}.type
            gson.fromJson(value, sType)
        }
    }

    @TypeConverter
    fun fromTimestamp(value: Long?): Date? {
        return value?.let { Date(it) }
    }

    @TypeConverter
    fun dateToTimestamp(date: Date?): Long? {
        return date?.time
    }

    @TypeConverter
    fun stringToOrderState(value: String?): Order.State? {
        val gson = Gson()
        return gson.fromJson(value, Order.State::class.java)
    }

    @TypeConverter
    fun orderStateToString(orderSate: Order.State?): String? {
        val gson = Gson()
        return gson.toJson(orderSate)
    }

    @TypeConverter
    fun stringToOrderType(value: String?): Order.Type? {
        val gson = Gson()
        return gson.fromJson(value, Order.Type::class.java)
    }

    @TypeConverter
    fun orderTypeToString(orderSate: Order.Type?): String? {
        val gson = Gson()
        return gson.toJson(orderSate)
    }
    
    @TypeConverter
    fun stringToOrderDate(value: String?): Order.Delivery? {
        val gson = Gson()
        return gson.fromJson(value, Order.Delivery::class.java)
    }

    @TypeConverter
    fun orderDateToString(value: Order.Delivery?): String? {
        val gson = Gson()
        return gson.toJson(value)
    }

    @TypeConverter
    fun stringToShippingAddress(value: String?): Customer.Address? {
        val gson = Gson()
        return gson.fromJson(value, Customer.Address::class.java)
    }

    @TypeConverter
    fun shippingAddressToString(value: Customer.Address?): String? {
        val gson = Gson()
        return gson.toJson(value)
    }

    @TypeConverter
    fun stringToArticle(value: String?): Article? {
        val gson = Gson()
        return gson.fromJson(value, Article::class.java)
    }

    @TypeConverter
    fun articleToString(value: Article?): String? {
        val gson = Gson()
        return gson.toJson(value)
    }

    @TypeConverter
    fun stringToOffer(value: String?): Offer? {
        val gson = Gson()
        return gson.fromJson(value, Offer::class.java)
    }

    @TypeConverter
    fun offerToString(value: Offer?): String? {
        val gson = Gson()
        return gson.toJson(value)
    }

    @TypeConverter
    fun stringToPackage(value: String?): Package? {
        val gson = Gson()
        return gson.fromJson(value, Package::class.java)
    }

    @TypeConverter
    fun packageToString(value: Package?): String? {
        val gson = Gson()
        return gson.toJson(value)
    }


}