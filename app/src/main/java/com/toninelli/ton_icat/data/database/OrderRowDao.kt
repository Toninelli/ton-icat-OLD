package com.toninelli.ton_icat.data.database

import androidx.room.*
import com.toninelli.ton_icat.model.CatalogItem
import com.toninelli.ton_icat.model.OrderRow
import kotlinx.coroutines.flow.Flow

@Dao
interface OrderRowDao {


    @Insert
    suspend fun insert(orderRow: OrderRow): Long

    @Delete
    suspend fun delete(orderRow: OrderRow): Int

    @Query("SELECT * FROM OrderRow WHERE orderId LIKE :orderId")
    fun getOrderRowByOrder(orderId: Long): Flow<List<OrderRow>>

    @Query("SELECT * FROM OrderRow WHERE orderRowId LIKE :id")
    suspend fun getOrderRowById(id: Long): OrderRow?

    @Query("SELECT orderId FROM OrderRow WHERE customerId LIKE :customerId")
    suspend fun getOrderRowSizeByCustomer(customerId: String): List<Long>

    @Query("UPDATE OrderRow SET discountedPrice = :discountPrice  WHERE orderRowId LIKE :id")
    suspend fun insertDiscountPrice(id: Long, discountPrice: Float)

    @Query("SELECT COUNT(orderRowId) FROM OrderRow WHERE orderId LIKE :orderId")
    fun getOrderRowSize(orderId: Long): Flow<Int>

}