package com.toninelli.ton_icat.icatcontext

import androidx.lifecycle.MutableLiveData
import com.toninelli.ton_icat.model.Order
import kotlinx.coroutines.channels.ConflatedBroadcastChannel

object Constants {

    //shared
    const val KEY_SHARED_PREFERENCE = "Ton Shared preference"
    const val KEY_SYNC_LOG = "Ton Sync Log"
    const val KEY_CATEG_OPTIONS = "Ton categ option"
    const val KEY_LOGIN_CREDENTIAL = "Ton credential agent"

    const val DIR_ART_IMAGES = "images"

    //bundle key
    const val BUNDLE_KEY_CUSTOMER_DETAIL = "customer"

}

object RunTimeValue {

    val activeOrder = MutableLiveData<Order?>(null)

    val rangeValue = MutableLiveData<Array<Int>?>(null)

}