package com.toninelli.ton_icat.extension

import android.content.Context
import android.content.SharedPreferences
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.toninelli.ton_icat.icatcontext.Constants
import com.toninelli.ton_icat.model.*

fun Context.getPreference(): SharedPreferences {
    return this.getSharedPreferences(
        Constants.KEY_SHARED_PREFERENCE,
        Context.MODE_PRIVATE
    )
}

fun SharedPreferences.getVisibilityOptions(): List<VisibilityOption>?{
    val jsonOptions = this.getString(Constants.KEY_CATEG_OPTIONS,null)
    return jsonOptions?.let { json ->
        val type = object : TypeToken<List<VisibilityOption>>() {}.type
        val items: List<VisibilityOption> = Gson().fromJson(json, type)
        items
    }
}

fun SharedPreferences.setVisibilityOptions(list: List<VisibilityOption>){
    with(this.edit()){
        val json = Gson().toJson(list)
        putString(Constants.KEY_CATEG_OPTIONS, json)
        apply()
    }
}

fun SharedPreferences.getCredential(): Credential{
    val jsonOptions = this.getString(Constants.KEY_LOGIN_CREDENTIAL,null)
    return jsonOptions?.let { json ->
       Gson().fromJson(json, Credential::class.java)
    }?: Credential()
}

fun SharedPreferences.setCredential(credential: Credential){
    with(this.edit()){
        val json = Gson().toJson(credential)
        putString(Constants.KEY_LOGIN_CREDENTIAL, json)
        apply()
    }
}

fun Context.getCountryNamesList(): List<String>{
    val gson = Gson()
    val jsonCountry = String(this.assets.open("country.json").readBytes(), Charsets.UTF_8)
    val type = object: TypeToken<List<Country>>(){}.type
    return  gson.fromJson<List<Country>>(jsonCountry, type).map { it.name }
}

fun Context.getCountryByName(name: String): Country {
    val gson = Gson()
    val jsonCountry = String(this.assets.open("country.json").readBytes(), Charsets.UTF_8)
    val type = object: TypeToken<List<Country>>(){}.type
    return  gson.fromJson<List<Country>>(jsonCountry, type).first { it.name == name }
}

fun Context.getImagesList(): List<String>{
    val gson = Gson()
    val jsonCountry = String(this.assets.open("image.json").readBytes(), Charsets.UTF_8)
    val type = object: TypeToken<List<ImageJSON>>(){}.type
    return  gson.fromJson<List<ImageJSON>>(jsonCountry, type).map { it.image }
}

fun Context.getDescrList(): List<DescrJSON>{
    val gson = Gson()
    val jsonCountry = String(this.assets.open("mapped.json").readBytes(), Charsets.UTF_8)
    val type = object: TypeToken<List<DescrJSON>>(){}.type
    return  gson.fromJson(jsonCountry, type)
}
