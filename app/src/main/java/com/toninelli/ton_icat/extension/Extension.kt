package com.toninelli.ton_icat.extension

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.view.View
import android.view.ViewTreeObserver
import android.view.inputmethod.InputMethodManager
import androidx.core.view.isEmpty
import androidx.fragment.app.Fragment
import androidx.lifecycle.*
import androidx.lifecycle.Observer
import androidx.navigation.findNavController
import androidx.navigation.ui.setupWithNavController
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.snackbar.Snackbar
import com.toninelli.fileutil.logd
import com.toninelli.ton_icat.R
import com.toninelli.ton_icat.model.Customer
import com.toninelli.ton_icat.model.Error
import com.toninelli.ton_icat.model.Order
import com.toninelli.ton_icat.model.OrderRow
import com.toninelli.ton_icat.ui.BaseFragment
import com.toninelli.ton_icat.ui.main.MainActivity
import com.toninelli.ton_icat.vo.Failure
import kotlinx.android.synthetic.main.app_bar_main.*
import kotlinx.coroutines.launch
import java.text.SimpleDateFormat
import java.util.*


fun BaseFragment.setTitle(title: String) {
    activity.supportActionBar?.title = title
}

fun MainActivity.showBottomNav(show: Boolean, menuId: Int = 0) {


    if (binding.appBarMain.bottomNavVis == show) return

    binding.appBarMain.bottomNavVis = show

    if (show) {
        if (binding.appBarMain.bottomNav.menu.isEmpty())
            binding.appBarMain.bottomNav.inflateMenu(menuId)
        bottom_nav.setupWithNavController(findNavController(R.id.nav_host_fragment))
    } else {
        binding.appBarMain.bottomNav.menu.clear()
    }
}


@SuppressLint("SimpleDateFormat")
fun Date?.format(): String {
    return this?.let {
        val format = SimpleDateFormat("dd/MM/yyyy HH:mm:ss")
        return format.format(it)
    } ?: ""
}

@SuppressLint("SimpleDateFormat")
fun Long?.format(): String {
    return this?.let {
        val format = SimpleDateFormat("dd/MM/yyyy HH:mm:ss")
        return format.format(Date(it))
    } ?: ""
}

@SuppressLint("SimpleDateFormat")
fun Date?.formatWithoutHour(): String {
    return this?.let {
        val format = SimpleDateFormat("dd/MM/yyyy")
        return format.format(it)
    } ?: ""
}

fun Date.differenceDaysFromToday(): Long {
    val difference: Long = Date().time - this.time
    return (difference / (1000 * 60 * 60 * 24))
}

fun String.match(list: List<String>): Boolean {
    return list.all {
        this.contains(it, true)
    }
}

fun ViewModel.withTimer(delay: Long, after: ViewModel.() -> Unit) {
    this.viewModelScope.launch {
        kotlinx.coroutines.delay(delay)
        after()
    }
}

fun OrderRow.amount(): Float {
    return if (this.catalogItem.acceptedOffer)
        this.catalogItem.offerPrice * (this.orderQta * this.multQta)
    else {
        this.catalogItem.discountedPrice?.let { discPrice ->
            discPrice * (this.orderQta * this.multQta)
        } ?: this.catalogItem.article.price * (this.orderQta * this.multQta)
    }

}

fun Customer.Address.string(): String {
    val builder = StringBuilder()
    builder.appendln(this.address)
    builder.appendln(this.town)
    builder.appendln(this.province)
    builder.appendln(this.cap)
    builder.appendln(this.alias)
    return builder.toString()
}


fun Fragment.hideKeyboard() {
    view?.let { activity?.hideKeyboard(it) }
}

fun Activity.hideKeyboard() {
    hideKeyboard(currentFocus ?: View(this))
}

fun Context.hideKeyboard(view: View) {
    val inputManager: InputMethodManager =
        getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    inputManager.hideSoftInputFromWindow(view.windowToken, InputMethodManager.SHOW_FORCED)
}

fun List<Error>?.composeErrorString(): String {
    val stringBuilder = StringBuilder()
    return this?.let {
        it.forEach {
            val string = "${it.tit} ${it.txt}"
            stringBuilder.append(string)
        }
        stringBuilder.toString()
    } ?: "Unknown Error"
}

inline fun RecyclerView.waitForLayout(crossinline f: () -> Unit) {
    with(this.viewTreeObserver) {
        addOnGlobalLayoutListener(object : ViewTreeObserver.OnGlobalLayoutListener {
            override fun onGlobalLayout() {
                removeOnGlobalLayoutListener(this)
                f()
            }

        })
    }
}
