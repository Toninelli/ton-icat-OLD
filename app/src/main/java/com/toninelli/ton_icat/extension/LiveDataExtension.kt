package com.toninelli.ton_icat.extension

import androidx.fragment.app.Fragment
import androidx.lifecycle.*
import com.toninelli.ton_icat.vo.Failure
import com.toninelli.ton_icat.vo.Resource
import com.toninelli.ton_icat.vo.Status

fun <X, Y> switchMapRes(source: LiveData<Resource<X>>, switchMapFun: (Resource<X>)-> LiveData<Resource<Y>>): LiveData<Resource<Y>>{

    val result = MediatorLiveData<Resource<Y>>()

    var mSource : LiveData<Resource<Y>>? = null

    result.addSource(source) {

        when(it.status){
            Status.SUCCESS -> {
                val newLiveData = switchMapFun(it)

                if (mSource == newLiveData)
                    return@addSource

                mSource?.let { result.removeSource(it) }

                mSource = newLiveData

                mSource?.let {
                    result.addSource(it) {
                        result.value = it
                    }
                }
            }
            Status.ERROR -> {
                result.value = Resource.Error(it.exception!!)
            }
            Status.LOADING -> {
                result.value = Resource.Loading()
            }
        }
    }

    return result
}

inline fun <X, Y> LiveData<Resource<X>>.switchRes(
    crossinline transform: (Resource<X>) -> LiveData<Resource<Y>>
): LiveData<Resource<Y>> = switchMapRes(this) { transform(it) }

fun <T: Any?, L: LiveData<T>> LifecycleOwner.observe(liveData: L, f:(T) -> Unit){
    liveData.observe(this, Observer {
        f(it)
    })
}


fun <L : LiveData<Failure>> LifecycleOwner.failure(liveData: L, f: (Failure?) -> Unit) {
    liveData.observe(this, Observer {
        f(it)
    })
}