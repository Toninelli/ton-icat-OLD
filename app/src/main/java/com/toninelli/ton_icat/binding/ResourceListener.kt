package com.toninelli.ton_icat.binding

import com.toninelli.ton_icat.vo.Resource

interface ResourceListener {
    fun <T> onResourceSuccess(resource: Resource<T>) {}
    fun onResourceError(exception: Exception) {}
    fun onResourceLoading() {}
}