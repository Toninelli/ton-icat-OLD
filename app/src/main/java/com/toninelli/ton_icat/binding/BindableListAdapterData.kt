package com.toninelli.ton_icat.binding


import com.toninelli.ton_icat.vo.Resource

interface BindableListAdapterData<T> {
    fun setData(data: Resource<T>)

}

interface BindableListAdapterInformation<T>{
    fun setInformation(info: T)
}

