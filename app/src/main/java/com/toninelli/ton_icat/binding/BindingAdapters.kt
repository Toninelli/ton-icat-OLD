package com.toninelli.ton_icat.binding

import android.content.Context
import android.content.ContextWrapper
import android.graphics.Typeface
import android.text.Html
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.appcompat.widget.ListPopupWindow
import androidx.core.net.toUri
import androidx.databinding.BindingAdapter
import androidx.databinding.InverseBindingAdapter
import androidx.databinding.InverseBindingListener
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.google.android.material.button.MaterialButton
import com.google.android.material.button.MaterialButtonToggleGroup
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.textfield.TextInputLayout
import com.squareup.picasso.Picasso
import com.toninelli.fileutil.logd
import com.toninelli.ton_icat.R
import com.toninelli.ton_icat.extension.amount
import com.toninelli.ton_icat.model.*
import com.toninelli.ton_icat.vo.BooleanResult
import com.toninelli.ton_icat.vo.Resource
import com.toninelli.ton_icat.vo.SingleEventWrapper
import com.toninelli.ton_icat.vo.case
import java.io.File
import java.lang.StringBuilder

@BindingAdapter("showHide")
fun showHide(view: View, b: Boolean) {
    view.visibility = if (b) View.VISIBLE else View.GONE
}

@BindingAdapter("showError")
fun <T> showError(view: TextView, resource: Resource<T>?) {
    resource?.case(error = {
        view.text = it.exception.message
    })
}

@BindingAdapter("showError")
fun <T> showError(view: ViewGroup, resource: Resource<T>?) {
    resource?.case(error = {
        Snackbar.make(view, it.exception.message.toString(), Snackbar.LENGTH_SHORT).show()
    })
}

@BindingAdapter("showError")
fun <T> showError(view: TextInputLayout, resource: Resource<T>?) {
    resource?.case(
        error = {
            view.error = it.exception.localizedMessage
        },
        success = {
            view.error = null
        }

    )
}


@BindingAdapter("showBooleanResult")
fun showBooleanResult(view: ViewGroup, result: Resource<SingleEventWrapper<BooleanResult>>?) {
    result?.let {
            it.data?.getContent()?.let {
                Snackbar.make(view, it.msg, Snackbar.LENGTH_SHORT).show()
            }
    }
}


@Suppress("UNCHECKED_CAST")
@BindingAdapter("adapterResourceData")
fun <T> bindAdapterData(view: RecyclerView, resource: Resource<T>?) {

    resource?.let {
        if (view.adapter is BindableListAdapterData<*>) {
            (view.adapter as BindableListAdapterData<T>).setData(it)
        }
    }
}

@Suppress("UNCHECKED_CAST")
@BindingAdapter("adapterResourceInfo")
fun <T> bindAdapterInfo(view: RecyclerView, info: T?) {

    info?.let {
        if (view.adapter is BindableListAdapterInformation<*>) {
            (view.adapter as BindableListAdapterInformation<T>).setInformation(it)
        }
    }
}

@BindingAdapter("uiListener", "resource")
fun <T> onResourceListener(view: View, listener: ResourceListener, resource: Resource<T>?) {
    resource?.case(
        success = { listener.onResourceSuccess(resource) },
        error = { listener.onResourceError(it.exception) },
        loading = { listener.onResourceLoading() })
}

@BindingAdapter("namePhoto")
fun loaderPhotoAdapter(view: ImageView, name: String?) {
    name?.let {
        val file =
            File(ContextWrapper(view.context).getDir("images", Context.MODE_PRIVATE), "${it.substringAfter("/")}.jpg")
        if (file.exists())
            Glide.with(view.context!!).load(file.toUri()).into(view)
        else
            Glide.with(view.context).load(R.drawable.no_image_placeholder).into(view)

    }
}

@BindingAdapter("activeLiveData")
fun toggleAdapter(
    view: MaterialButtonToggleGroup,
    active: MutableLiveData<Boolean>?
) {

    view.addOnButtonCheckedListener { _, _, _ ->
        if (view.checkedButtonIds.size == 2) {
            val toggle1 = view.checkedButtonIds[0]
            val toggle2 = view.checkedButtonIds[1]
            if (view.checkedButtonId == toggle1) {
                active?.postValue(true)
            } else {
                active?.postValue(false)
            }
        }
    }

}

@BindingAdapter("checked")
fun checkedButton(view: MaterialButton, boolean: Boolean) {
    view.isChecked = boolean
}

@InverseBindingAdapter(attribute = "checked")
fun getIsActiveFromView(view: MaterialButton): Boolean {
    return view.isChecked
}

@BindingAdapter("isCheckedAttrChanged")
fun isActivatedAttrChanged(view: MaterialButton, listener: InverseBindingListener?) {
    view.setOnClickListener {
        logd("changed  ${view.id}  ${view.isChecked}")
        listener?.onChange()
    }
}

@BindingAdapter("loadImage")
fun loadImage(view: ImageView, url: String?) {
    url?.let {
        Picasso.get().load(url).placeholder(R.drawable.bubu1).into(view)
    }
}

@BindingAdapter("setTextHTML")
fun setTextHTML(view: TextView, s: String) {
    val text = Html.fromHtml(s)
    view.text = text
}

@BindingAdapter("syncStatus")
fun syncStatus(view: TextView, syncProgress: SyncProgress?) {
    syncProgress?.let {

        if (it.progress != null && it.total != null && it.type != null) {
//            view.text =  "Sincronizzazione ${it.type.typeName}: ${it.progress} di ${it.total}"
            view.text = view.context.getString(R.string.sync_progress_text_progress, it.type.typeName, it.progress, it.total)
        }
    }
}

@BindingAdapter("popUpMenu")
fun showPopUpMenu(view: MaterialButton, list: List<String>) {

    val menu = ListPopupWindow(view.context, null, R.attr.listPopupWindowStyle)
    val adapter = ArrayAdapter<String>(view.context, R.layout.popup_menu_item, list)
    menu.setAdapter(adapter)
    menu.anchorView = view
    menu.setOnItemClickListener { _, _, position, _ ->
        view.text = adapter.getItem(position).toString()
        menu.dismiss()
    }
    view.setOnClickListener { menu.show() }
}


@BindingAdapter("orderState")
fun showOrderState(view: TextView, state: Order.State?) {
    state?.let {
        view.text = it.stateName
        when (it) {
            Order.State.NOT_COMPLETED -> view.setTextColor(view.context.resources.getColor(R.color.red))
            Order.State.TRANSMITTED -> view.setTextColor(view.context.resources.getColor(R.color.green))
        }
    }
}

@BindingAdapter("textPackage")
fun textPackage(view: TextView, list: List<Article.Package>?) {
    list?.let {
        val stringBuilder = StringBuilder()
        list.forEach {
            stringBuilder.append(it.packName)
            if (list.indexOf(it) != list.size - 1)
                stringBuilder.append(", ")
        }

        view.text =
            String.format(view.context.getString(R.string.catalog_pack), stringBuilder.toString())
    }
}

@BindingAdapter("textTotalPrice")
fun textTotalPice(view: TextView, item: OrderRow?) {
    item?.let {

        view.text = it.amount().toString()

    }
}

@BindingAdapter("textPrice")
fun textPrice(view: TextView, item: CatalogItem?) {
    item?.let {
        if (it.acceptedOffer) {
            view.text = it.offerPrice.toString()
            view.setTextColor(view.context.getColor(R.color.black))
        } else {
            it.discountedPrice?.let { discounted ->
                view.text = discounted.toString()
                view.setTextColor(view.context.getColor(R.color.black))
            } ?: run {
                view.text = it.article.price.toString()
                view.setTextColor(view.context.getColor(R.color.red_dark))
            }
        }
    }
}


@BindingAdapter("textTonQta")
fun textTonQta(view: TextView, qta: Int?){
    qta?.let {
        view.text = view.context.getString(R.string.catalog_ton_stock, it)
        if(it > 0)
            view.setTextColor(view.context.getColor(R.color.black))
        else
            view.setTextColor(view.context.getColor(R.color.red))

    }
}

@BindingAdapter("textHDQta")
fun textHdQta(view: TextView, qta: Int?){
    qta?.let {
        view.text = view.context.getString(R.string.catalog_hd_stock, it)
        if(it > 0)
            view.setTextColor(view.context.getColor(R.color.black))
        else
            view.setTextColor(view.context.getColor(R.color.red))

    }
}


