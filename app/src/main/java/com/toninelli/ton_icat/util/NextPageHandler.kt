package com.toninelli.ton_icat.util

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import com.toninelli.fileutil.logd
import com.toninelli.ton_icat.model.CatalogItem
import com.toninelli.ton_icat.vo.Resource
import com.toninelli.ton_icat.vo.case
import kotlin.math.log

class NextPageHandler<T>(
    private val liveDataToObserve: LiveData<Resource<List<T>>>,
    private val doCall: (Int) -> Unit
) : Observer<Resource<List<T>>> {

    private var itemCount: Int? = null
    private var hasMore: Boolean = true
    private val elementCache = mutableSetOf<T>()
    val totalResult = MutableLiveData<Resource<List<T>>>()

    init {
        liveDataToObserve.observeForever(this)
    }

    fun loadNextPage(itemCount: Int) {

        if (itemCount == this.itemCount)
            return

        if (!hasMore)
            return

        this.itemCount = itemCount

        logd(itemCount)

        doCall(itemCount)
    }

    override fun onChanged(t: Resource<List<T>>?) {
        t?.case(
            success = {
                it.data?.let { newest ->
                    logd("nuovi arrivati ${newest.size} con ite")
                    logd(elementCache.intersect(newest).size)
                    elementCache.addAll(newest)
                    hasMore = itemCount != elementCache.size
                    totalResult.value = (Resource.Success(elementCache.toList()))
                }

                itemCount = null
            },
            error = {
                hasMore = false
                itemCount = null
            }
        )
    }

    fun clearData() {
        elementCache.clear()
    }

    fun reset() {
        elementCache.clear()
        hasMore = true
    }

}