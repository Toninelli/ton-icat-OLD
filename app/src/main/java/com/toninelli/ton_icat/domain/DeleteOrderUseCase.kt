package com.toninelli.ton_icat.domain

import com.toninelli.ton_icat.data.database.DbRepository
import com.toninelli.ton_icat.vo.BooleanResult
import com.toninelli.ton_icat.vo.Either
import com.toninelli.ton_icat.vo.Failure
import com.toninelli.ton_icat.vo.SingleEventWrapper
import java.util.*
import javax.inject.Inject

class DeleteOrderUseCase @Inject constructor(private val dbRepository: DbRepository): UseCase<DeleteOrderUseCase.Param, SingleEventWrapper<BooleanResult>>() {

    data class Param(val orderId: Long)

    override suspend fun exec(param: Param?): Either<Failure, SingleEventWrapper<BooleanResult>> {
        return dbRepository.deleteOrder(param?.orderId!!)
    }

}