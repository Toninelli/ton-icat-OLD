package com.toninelli.ton_icat.domain

import com.toninelli.ton_icat.data.database.DbRepository
import com.toninelli.ton_icat.model.Customer
import com.toninelli.ton_icat.vo.Either
import com.toninelli.ton_icat.vo.Failure
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class GetAllCustomerDoingOrder @Inject constructor(private val dbRepository: DbRepository): FlowUseCase<Nothing, List<Customer>>() {
    override suspend fun exec(param: Nothing?): Flow<Either<Failure, List<Customer>>> {
        return dbRepository.getAllCustomersDoingOrder()
    }
}