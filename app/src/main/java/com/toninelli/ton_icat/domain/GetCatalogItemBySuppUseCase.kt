package com.toninelli.ton_icat.domain

import com.toninelli.ton_icat.data.database.DbRepository
import com.toninelli.ton_icat.data.network.NetworkRepository
import com.toninelli.ton_icat.model.CatalogItem
import com.toninelli.ton_icat.vo.*
import javax.inject.Inject

class GetCatalogItemBySuppUseCase@Inject constructor(
    private val dbRepo: DbRepository,
    private val networkRepository: NetworkRepository
) : BaseGetCatalogItemUseCase<GetCatalogItemBySuppUseCase.Param>(networkRepository) {

    override suspend fun itemFromDb(param: Param?): Either<Failure, List<CatalogItem>> = dbRepo.getCatalogItemBySupplier(param?.supplierId!!, param.limit, param.offset)

    class Param(val supplierId: String, val limit: Int, val offset: Int, override val customerId: String?): BaseGetCatalogItemUseCase.BaseParam(customerId)

}