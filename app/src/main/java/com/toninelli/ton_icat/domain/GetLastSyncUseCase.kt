package com.toninelli.ton_icat.domain


import com.toninelli.ton_icat.data.database.DbRepository
import com.toninelli.ton_icat.data.network.NetworkRepository
import com.toninelli.ton_icat.domain.UseCase
import com.toninelli.ton_icat.model.SyncLog
import com.toninelli.ton_icat.vo.Either
import com.toninelli.ton_icat.vo.Failure
import javax.inject.Inject

class GetLastSyncUseCase @Inject constructor(private val dbRepository: DbRepository) :
    UseCase<Nothing, SyncLog>() {
    override suspend fun exec(param: Nothing?): Either<Failure, SyncLog> {
        return dbRepository.getLastSyncCatalog()
    }

}