package com.toninelli.ton_icat.domain

import com.toninelli.ton_icat.data.database.DbRepository
import com.toninelli.ton_icat.model.Customer
import com.toninelli.ton_icat.model.Order
import com.toninelli.ton_icat.vo.BooleanResult
import com.toninelli.ton_icat.vo.Either
import com.toninelli.ton_icat.vo.Failure
import com.toninelli.ton_icat.vo.SingleEventWrapper
import javax.inject.Inject

class UpdateOrderDestinationUseCase @Inject constructor(private val dbRepository: DbRepository): UseCase<UpdateOrderDestinationUseCase.Param, SingleEventWrapper<BooleanResult>>() {

    data class Param(val orderId: Long , val destination: Customer.Address)

    override suspend fun exec(param: Param?): Either<Failure, SingleEventWrapper<BooleanResult>> {

        return dbRepository.updateOrderDestination(param?.orderId!!, param.destination)
    }

}