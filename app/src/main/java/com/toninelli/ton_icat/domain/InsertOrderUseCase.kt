package com.toninelli.ton_icat.domain

import com.toninelli.ton_icat.data.database.DbRepository
import com.toninelli.ton_icat.model.Order
import com.toninelli.ton_icat.vo.BooleanResult
import com.toninelli.ton_icat.vo.Either
import com.toninelli.ton_icat.vo.Failure
import com.toninelli.ton_icat.vo.SingleEventWrapper
import java.util.*
import javax.inject.Inject

class InsertOrderUseCase @Inject constructor(private val dbRepository: DbRepository): UseCase<InsertOrderUseCase.Param, SingleEventWrapper<BooleanResult>>() {

    data class Param(val customerId: String)

    override suspend fun exec(param: Param?): Either<Failure, SingleEventWrapper<BooleanResult>> {
        return dbRepository.insertOrder(param?.customerId!!)
    }

}