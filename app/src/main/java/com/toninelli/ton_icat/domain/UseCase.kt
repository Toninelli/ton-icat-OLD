package com.toninelli.ton_icat.domain

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.toninelli.fileutil.logd
import com.toninelli.ton_icat.vo.Either
import com.toninelli.ton_icat.vo.Failure
import com.toninelli.ton_icat.vo.Resource
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.*
import kotlin.math.log

abstract class UseCase<P, R> : AbstractUseCase<P, Either<Failure, R>>() {

    var resultLiveData = MutableLiveData<Resource<R>>()

    abstract override suspend fun exec(param: P?): Either<Failure, R>

    override fun start(scope: CoroutineScope, param: P?) {

        scope.launch {
            val result = withContext(Dispatchers.IO) {
                resultLiveData.postValue(Resource.Loading(null))
                exec(param)
            }
            with(result) {

                this.either(
                    {
                        resultLiveData.postValue(Resource.Error(it))
                    }, {
                        resultLiveData.postValue(Resource.Success(it))
                    })
            }
        }



    }
}


abstract class FlowUseCase<P, R> : AbstractUseCase<P, Flow<Either<Failure, R>>>() {

    var resultLiveData = MutableLiveData<Resource<R>>()

    abstract override suspend fun exec(param: P?): Flow<Either<Failure, R>>

    @ExperimentalCoroutinesApi
    override fun start(
        scope: CoroutineScope,
        param: P?
    ) {
        scope.launch {
            val result = withContext(Dispatchers.IO) { exec(param) }
            result.let {
                it.onStart { resultLiveData.postValue(Resource.Loading()) }
                    .onCompletion { logd("completed") }
                    .flowOn(Dispatchers.Default)
                    .collect { value ->
                        value.either(
                            {
                                resultLiveData.postValue(Resource.Error(it))
                            },
                            {
                                resultLiveData.postValue(Resource.Success(it))
                            })
                    }
            }
        }
    }

}

abstract class AbstractUseCase<P, R> {

    private var useCaseScope: CoroutineScope? = null

    operator fun invoke(scope: CoroutineScope, param: P?){
        useCaseScope?.cancel().also { useCaseScope = scope + Job() }
        start(useCaseScope!!, param)
    }

    protected abstract suspend fun exec(param: P?): R

    abstract fun start(scope: CoroutineScope, param: P?)
}


fun <P, R> ViewModel.exec(
    useCase: AbstractUseCase<P, R>,
    param: P? = null,
    scope: CoroutineScope = viewModelScope

) {
    useCase(scope, param)

}
