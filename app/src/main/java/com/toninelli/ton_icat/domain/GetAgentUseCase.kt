package com.toninelli.ton_icat.domain

import com.toninelli.ton_icat.data.database.AgentDao
import com.toninelli.ton_icat.data.database.DbRepository
import com.toninelli.ton_icat.model.Agent
import com.toninelli.ton_icat.vo.Either
import com.toninelli.ton_icat.vo.Failure
import javax.inject.Inject

class GetAgentUseCase @Inject constructor(private val dbRepository: DbRepository): UseCase<Nothing, Agent>() {

    override suspend fun exec(param: Nothing?): Either<Failure, Agent> {
        return dbRepository.getAgent()
    }


}