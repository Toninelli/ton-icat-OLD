package com.toninelli.ton_icat.domain

import com.toninelli.ton_icat.data.database.DbRepository
import com.toninelli.ton_icat.model.Order
import com.toninelli.ton_icat.vo.Either
import com.toninelli.ton_icat.vo.Failure
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class GetCustomerOrdersNotCompletedUseCase @Inject constructor(private val dbRepository: DbRepository): FlowUseCase<GetCustomerOrdersNotCompletedUseCase.Param, List<Order>>(){

    data class Param(val customerId: String?)

    override suspend fun exec(param: Param?): Flow<Either<Failure, List<Order>>> {

        return dbRepository.getCustomerOrdersByStatus(param?.customerId, Order.State.NOT_COMPLETED)
    }
}