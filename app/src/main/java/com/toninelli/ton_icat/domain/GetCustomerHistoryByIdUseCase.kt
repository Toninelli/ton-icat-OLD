package com.toninelli.ton_icat.domain

import com.toninelli.ton_icat.data.network.NetworkRepository
import com.toninelli.ton_icat.model.CustomerHistory
import com.toninelli.ton_icat.vo.Either
import com.toninelli.ton_icat.vo.Failure
import javax.inject.Inject

class GetCustomerHistoryByIdUseCase @Inject constructor(private val networkRepository: NetworkRepository): UseCase<GetCustomerHistoryByIdUseCase.Param, List<CustomerHistory>>() {

    data class Param(val customerId: String)

    override suspend fun exec(param: Param?): Either<Failure, List<CustomerHistory>> {
        return networkRepository.getCustomerHistory(param?.customerId!!)
    }

}