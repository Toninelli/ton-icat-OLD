package com.toninelli.ton_icat.domain

import com.toninelli.ton_icat.data.database.DbRepository
import com.toninelli.ton_icat.model.VisibilityOption
import com.toninelli.ton_icat.vo.Either
import com.toninelli.ton_icat.vo.Failure
import javax.inject.Inject

class SetVisibilityOptionUseCase @Inject constructor(private val dbRepository: DbRepository): UseCase<SetVisibilityOptionUseCase.Param, List<VisibilityOption>>() {

    data class Param(val option: VisibilityOption)

    override suspend fun exec(param: Param?): Either<Failure, List<VisibilityOption>> {
        return dbRepository.setVisibilityOption(param?.option)
    }
}