package com.toninelli.ton_icat.domain

import com.toninelli.fileutil.logd
import com.toninelli.ton_icat.data.database.DbRepository
import com.toninelli.ton_icat.data.network.NetworkRepository
import com.toninelli.ton_icat.model.CatalogItem
import com.toninelli.ton_icat.vo.*
import javax.inject.Inject

class GetCatalogItemByQueryUseCase @Inject constructor(
    private val dbRepo: DbRepository,
    private val networkRepository: NetworkRepository
) : BaseGetCatalogItemUseCase<GetCatalogItemByQueryUseCase.Param>(networkRepository) {

    init {
        logd("Inizializzato")
    }

    override suspend fun itemFromDb(param: Param?): Either<Failure, List<CatalogItem>> {
        return dbRepo.getCatalogItemByQuery(param?.query!!, param.limit, param.offset)
    }
    class Param(val query: String, val limit: Int, val offset: Int, override val customerId: String?): BaseGetCatalogItemUseCase.BaseParam(customerId)

}