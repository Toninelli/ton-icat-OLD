package com.toninelli.ton_icat.domain

import com.toninelli.ton_icat.data.database.DbRepository
import com.toninelli.ton_icat.model.Customer
import com.toninelli.ton_icat.vo.Either
import com.toninelli.ton_icat.vo.Failure
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class GetCustomerFlowByIdUseCase @Inject constructor(private val dbRepository: DbRepository): FlowUseCase<GetCustomerByIdUseCase.Param, Customer>() {

    data class Param(val id: String)

    override suspend fun exec(param: GetCustomerByIdUseCase.Param?): Flow<Either<Failure, Customer>> {
        return dbRepository.getCustomerFlowById(param?.id!!)
    }


}