package com.toninelli.ton_icat.domain

import com.toninelli.ton_icat.data.database.DbRepository
import com.toninelli.ton_icat.model.Credential
import com.toninelli.ton_icat.vo.Either
import com.toninelli.ton_icat.vo.Failure
import javax.inject.Inject

class GetCredentialUseCase @Inject constructor(private val dbRepository: DbRepository): UseCase<Nothing, Credential>() {
    override suspend fun exec(param: Nothing?): Either<Failure, Credential> {
        return dbRepository.getCredential()
    }


}