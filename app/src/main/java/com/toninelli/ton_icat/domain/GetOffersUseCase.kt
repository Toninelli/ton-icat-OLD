package com.toninelli.ton_icat.domain

import com.toninelli.ton_icat.data.database.DbRepository
import com.toninelli.ton_icat.data.network.NetworkRepository
import com.toninelli.ton_icat.model.Offer
import com.toninelli.ton_icat.vo.Either
import com.toninelli.ton_icat.vo.Failure
import javax.inject.Inject

class GetOffersUseCase @Inject constructor(val repo: DbRepository) :
    UseCase<Nothing, List<Offer>>() {
    override suspend fun exec(param: Nothing?): Either<Failure, List<Offer>> {
        return repo.getOffers()
    }
}