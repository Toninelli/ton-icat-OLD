package com.toninelli.ton_icat.domain

import com.toninelli.ton_icat.data.database.DbRepository
import com.toninelli.ton_icat.data.network.NetworkRepository
import com.toninelli.ton_icat.vo.*
import javax.inject.Inject

class LoadSaveDiscountedPriceUseCase @Inject constructor(private val networkRepository: NetworkRepository, private val dbRepository: DbRepository): UseCase<LoadSaveDiscountedPriceUseCase.Param, BooleanResult>() {

    data class Param(val customerId: String, val artCod: Int, val orderRowId: Long)

    override suspend fun exec(param: Param?): Either<Failure, BooleanResult> {

        val discountedPrice = networkRepository.getDiscountedPrice(
            param?.customerId!!,
            param.artCod
        )

        return discountedPrice.rightOrNull()?.let { dbRepository.insertDiscountPrice(param.orderRowId, it) } ?: left(discountedPrice.leftOrNull()!!)

    }
}