package com.toninelli.ton_icat.domain

import com.toninelli.ton_icat.data.network.NetworkRepository
import com.toninelli.ton_icat.model.News
import com.toninelli.ton_icat.model.Pdf
import com.toninelli.ton_icat.vo.Either
import com.toninelli.ton_icat.vo.Failure
import javax.inject.Inject

class GetPdfOfferUseCase @Inject constructor(val repo: NetworkRepository) :
    UseCase<Nothing, List<Pdf>>() {
    override suspend fun exec(param: Nothing?): Either<Failure, List<Pdf>> {
        return repo.getPdfOffer()
    }
}