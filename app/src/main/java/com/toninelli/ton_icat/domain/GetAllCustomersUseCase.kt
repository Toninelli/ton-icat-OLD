package com.toninelli.ton_icat.domain

import com.toninelli.ton_icat.data.database.DbRepository
import com.toninelli.ton_icat.model.Customer
import com.toninelli.ton_icat.vo.Either
import com.toninelli.ton_icat.vo.Failure
import javax.inject.Inject

class GetAllCustomersUseCase @Inject constructor(private val dbRepository: DbRepository): UseCase<Nothing, List<Customer>>() {
    override suspend fun exec(param: Nothing?): Either<Failure, List<Customer>> {
        return dbRepository.getAllCustomers()
    }
}