package com.toninelli.ton_icat.domain

import com.toninelli.ton_icat.data.database.DbRepository
import com.toninelli.ton_icat.model.Order
import com.toninelli.ton_icat.vo.BooleanResult
import com.toninelli.ton_icat.vo.Either
import com.toninelli.ton_icat.vo.Failure
import com.toninelli.ton_icat.vo.SingleEventWrapper
import javax.inject.Inject

class UpdateOrderDeliveryUseCase @Inject constructor(private val dbRepository: DbRepository): UseCase<UpdateOrderDeliveryUseCase.Param, SingleEventWrapper<BooleanResult>>() {

    data class Param(val orderId: Long , val delivery: Order.Delivery)

    override suspend fun exec(param: Param?): Either<Failure, SingleEventWrapper<BooleanResult>> {

        return dbRepository.updateOrderDelivery(param?.orderId!!, param.delivery)
    }

}