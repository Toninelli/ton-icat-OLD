package com.toninelli.ton_icat.domain

import com.toninelli.ton_icat.data.database.CategoryTree
import com.toninelli.ton_icat.data.database.DbRepository
import com.toninelli.ton_icat.model.Category
import com.toninelli.ton_icat.vo.Either
import com.toninelli.ton_icat.vo.Failure
import javax.inject.Inject

class GetCategUseCase @Inject constructor(private val repo: DbRepository) :
    UseCase<Nothing, CategoryTree>() {

    override suspend fun exec(param: Nothing?): Either<Failure, CategoryTree> {
        return repo.getCategories()
    }

}

