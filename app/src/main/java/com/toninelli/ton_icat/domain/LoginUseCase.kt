package com.toninelli.ton_icat.domain

import com.toninelli.ton_icat.data.database.DbRepository
import com.toninelli.ton_icat.data.network.NetworkRepository
import com.toninelli.ton_icat.model.Agent
import com.toninelli.ton_icat.model.Credential
import com.toninelli.ton_icat.vo.*
import javax.inject.Inject

class LoginUseCase @Inject constructor(
    private val dbRepository: DbRepository,
    private val networkRepository: NetworkRepository
) : UseCase<LoginUseCase.Param, Agent>() {

    data class Param(val credential: Credential)

    override suspend fun exec(param: Param?): Either<Failure, Agent> {

        val lastCredential = dbRepository.getCredential().rightOrNull()

        dbRepository.setCredential(param?.credential!!)

        return if (lastCredential != Credential() &&
            lastCredential?.username == param.credential.username &&
            lastCredential?.password == param.credential.password
        )
            dbRepository.getAgent()
        else
            networkRepository.login(param.credential.username!!, param.credential.password!!)


    }

}