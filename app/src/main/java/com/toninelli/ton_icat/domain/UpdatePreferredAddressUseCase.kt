package com.toninelli.ton_icat.domain

import com.toninelli.ton_icat.data.database.DbRepository
import com.toninelli.ton_icat.model.Customer
import com.toninelli.ton_icat.vo.BooleanResult
import com.toninelli.ton_icat.vo.Either
import com.toninelli.ton_icat.vo.Failure
import com.toninelli.ton_icat.vo.SingleEventWrapper
import javax.inject.Inject

class UpdatePreferredAddressUseCase @Inject constructor(private val dbRepository: DbRepository): UseCase<UpdatePreferredAddressUseCase.Param, SingleEventWrapper<BooleanResult>>(){

    data class Param(val customerId: String, val address: Customer.Address)

    override suspend fun exec(param: Param?): Either<Failure, SingleEventWrapper<BooleanResult>> {
        return dbRepository.updatePreferredAddress(param?.customerId!!, param.address)
    }

}