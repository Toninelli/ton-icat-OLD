package com.toninelli.ton_icat.domain

import com.toninelli.ton_icat.data.database.DbRepository
import com.toninelli.ton_icat.model.Customer
import com.toninelli.ton_icat.vo.Either
import com.toninelli.ton_icat.vo.Failure
import javax.inject.Inject

class GetCustomerByIdUseCase @Inject constructor(private val dbRepository: DbRepository): UseCase<GetCustomerByIdUseCase.Param, Customer>() {

    data class Param(val id: String)

    override suspend fun exec(param: Param?): Either<Failure, Customer> {
        return dbRepository.getCustomerById(param?.id!!)
    }

}