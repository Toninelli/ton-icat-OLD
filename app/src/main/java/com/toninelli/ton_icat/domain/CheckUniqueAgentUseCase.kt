package com.toninelli.ton_icat.domain

import com.toninelli.ton_icat.data.database.DbRepository
import com.toninelli.ton_icat.model.Agent
import com.toninelli.ton_icat.vo.BooleanResult
import com.toninelli.ton_icat.vo.Either
import com.toninelli.ton_icat.vo.Failure
import javax.inject.Inject

class CheckUniqueAgentUseCase @Inject constructor(private val dbRepository: DbRepository): UseCase<CheckUniqueAgentUseCase.Param, BooleanResult>() {

    data class Param(val newAgent: Agent)

    override suspend fun exec(param: Param?): Either<Failure, BooleanResult> {
        return dbRepository.chekUniqueAgent(param?.newAgent!!)
    }

}