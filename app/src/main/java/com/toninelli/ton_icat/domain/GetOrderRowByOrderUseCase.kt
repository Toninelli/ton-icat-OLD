package com.toninelli.ton_icat.domain

import com.toninelli.ton_icat.data.database.DbRepository
import com.toninelli.ton_icat.model.CatalogItem
import com.toninelli.ton_icat.model.OrderRow
import com.toninelli.ton_icat.vo.Either
import com.toninelli.ton_icat.vo.Failure
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class GetOrderRowByOrderUseCase @Inject constructor(private val dbRepository: DbRepository): FlowUseCase<GetOrderRowByOrderUseCase.Param, List<OrderRow>>() {

    data class Param(val orderId: Long)

    override suspend fun exec(param: Param?): Flow<Either<Failure, List<OrderRow>>> {
        return dbRepository.getOrderRowByOrder(param?.orderId!!)
    }

}