package com.toninelli.ton_icat.domain

import com.toninelli.ton_icat.data.database.DbRepository
import com.toninelli.ton_icat.model.Supplier
import com.toninelli.ton_icat.vo.Either
import com.toninelli.ton_icat.vo.Failure
import javax.inject.Inject

class GetSupplierUseCase @Inject constructor(val repository: DbRepository) :
    UseCase<Nothing, List<Supplier>>() {
    override suspend fun exec(param: Nothing?): Either<Failure, List<Supplier>> {
        return repository.getSupplier()
    }
}