package com.toninelli.ton_icat.domain

import com.toninelli.ton_icat.data.database.DbRepository
import com.toninelli.ton_icat.vo.Either
import com.toninelli.ton_icat.vo.Failure
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class GetOrderRowSizeByOrderUseCase @Inject constructor(private val dbRepository: DbRepository): FlowUseCase<GetOrderRowSizeByOrderUseCase.Param, Int>(){

    data class Param(val orderId: Long)

    override suspend fun exec(param: Param?): Flow<Either<Failure, Int>> {
        return dbRepository.getOrderRowSizeByOrder(param?.orderId!!)
    }

}