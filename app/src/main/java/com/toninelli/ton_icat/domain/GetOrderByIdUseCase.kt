package com.toninelli.ton_icat.domain

import com.toninelli.ton_icat.data.database.DbRepository
import com.toninelli.ton_icat.model.Order
import com.toninelli.ton_icat.vo.Either
import com.toninelli.ton_icat.vo.Failure
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class GetOrderByIdUseCase @Inject constructor(private val dbRepository: DbRepository): FlowUseCase<GetOrderByIdUseCase.Param, Order>() {

    data class Param(val orderId: Long)

    override suspend fun exec(param: Param?): Flow<Either<Failure, Order>> {
        return dbRepository.getOrderById(param?.orderId!!)
    }

}