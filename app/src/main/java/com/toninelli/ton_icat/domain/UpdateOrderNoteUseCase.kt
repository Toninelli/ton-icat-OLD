package com.toninelli.ton_icat.domain

import com.toninelli.ton_icat.data.database.DbRepository
import com.toninelli.ton_icat.model.Order
import com.toninelli.ton_icat.vo.*
import javax.inject.Inject

class UpdateOrderNoteUseCase @Inject constructor(private val dbRepository: DbRepository): UseCase<UpdateOrderNoteUseCase.Param, SingleEventWrapper<BooleanResult>>() {

    data class Param(val orderId: Long , val note: String)

    override suspend fun exec(param: Param?): Either<Failure, SingleEventWrapper<BooleanResult>> {

        return dbRepository.updateOrderNote(param?.orderId!!, param.note).toSingle()
    }

}