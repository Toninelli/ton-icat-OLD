package com.toninelli.ton_icat.domain

import com.toninelli.fileutil.nullIfEmpty
import com.toninelli.ton_icat.data.database.DbRepository
import com.toninelli.ton_icat.data.network.NetworkRepository
import com.toninelli.ton_icat.model.CatalogItem
import com.toninelli.ton_icat.vo.*
import javax.inject.Inject

class GetCatalogItemByCodListUseCase @Inject constructor(
    private val dbRepository: DbRepository,
    private val networkRepository: NetworkRepository
) : BaseGetCatalogItemUseCase<GetCatalogItemByCodListUseCase.Param>(networkRepository) {

    data class Param(val cods: List<Int>, override val customerId: String?) : BaseParam(customerId)

    override suspend fun itemFromDb(param: Param?): Either<Failure, List<CatalogItem>> {

        return dbRepository.getCatalogItemByCods(param?.cods!!)

    }
}