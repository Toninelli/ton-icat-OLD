package com.toninelli.ton_icat.domain

import com.toninelli.ton_icat.data.database.DbRepository
import com.toninelli.ton_icat.model.CatalogItem
import com.toninelli.ton_icat.model.OrderRow
import com.toninelli.ton_icat.vo.*
import javax.inject.Inject

class InsertOrderRowUseCase @Inject constructor(
    private val dbRepository: DbRepository

) : UseCase<InsertOrderRowUseCase.Param, SingleEventWrapper<BooleanResult>>() {

    data class Param(val orderRow: OrderRow)

    override suspend fun exec(param: Param?): Either<Failure, SingleEventWrapper<BooleanResult>> {
        return dbRepository.insertOrderRow(param?.orderRow!!)
    }
}