package com.toninelli.ton_icat.domain

import com.toninelli.fileutil.logd
import com.toninelli.ton_icat.data.database.DbRepository
import com.toninelli.ton_icat.model.SelectedCustomer
import com.toninelli.ton_icat.vo.Either
import com.toninelli.ton_icat.vo.Failure
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class GetSelectedCustomerUseCase @Inject constructor(private val dbRepository: DbRepository): FlowUseCase<Nothing, SelectedCustomer>() {
    override suspend fun exec(param: Nothing?): Flow<Either<Failure, SelectedCustomer>> {
        return dbRepository.getSelectedCustomer()
    }
}