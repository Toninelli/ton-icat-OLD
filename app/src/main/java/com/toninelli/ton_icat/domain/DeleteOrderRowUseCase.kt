package com.toninelli.ton_icat.domain

import com.toninelli.ton_icat.data.database.DbRepository
import com.toninelli.ton_icat.model.CatalogItem
import com.toninelli.ton_icat.model.OrderRow
import com.toninelli.ton_icat.vo.BooleanResult
import com.toninelli.ton_icat.vo.Either
import com.toninelli.ton_icat.vo.Failure
import com.toninelli.ton_icat.vo.SingleEventWrapper
import javax.inject.Inject

class DeleteOrderRowUseCase @Inject constructor(private val dbRepository: DbRepository): UseCase<DeleteOrderRowUseCase.Param, SingleEventWrapper<BooleanResult>>() {

    data class Param(val orderRow: OrderRow)

    override suspend fun exec(param: Param?): Either<Failure, SingleEventWrapper<BooleanResult>> {
        return dbRepository.deleteOrderRow(param?.orderRow!!)
    }

}