package com.toninelli.ton_icat.domain

import com.toninelli.ton_icat.data.network.NetworkRepository
import com.toninelli.ton_icat.model.CatalogItem
import com.toninelli.ton_icat.vo.Either
import com.toninelli.ton_icat.vo.Failure
import com.toninelli.ton_icat.vo.rightOrNull
import kotlinx.coroutines.cancel

abstract class BaseGetCatalogItemUseCase<Param: BaseGetCatalogItemUseCase.BaseParam>(private val networkRepository: NetworkRepository): UseCase<Param, List<CatalogItem>>(){

    open class BaseParam(open val customerId: String?)

    override suspend fun exec(param: Param?): Either<Failure, List<CatalogItem>> {

        val list = itemFromDb(param)

        param?.customerId?.let {customer ->

            list.rightOrNull()?.let {
                it.forEach {
                    val discountedPrice = networkRepository.getDiscountedPrice(
                        customer,
                        it.article.artCod
                    )
                    it.discountedPrice = discountedPrice.rightOrNull()

                }
            }
        }
        return list
    }

    abstract suspend fun itemFromDb(param: Param?): Either<Failure, List<CatalogItem>>

}