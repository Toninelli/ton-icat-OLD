package com.toninelli.ton_icat.domain

import androidx.room.FtsOptions
import com.toninelli.ton_icat.data.database.DbRepository
import com.toninelli.ton_icat.model.Customer
import com.toninelli.ton_icat.model.Order
import com.toninelli.ton_icat.vo.*
import kotlinx.coroutines.flow.Flow
import java.util.*
import javax.inject.Inject

class GetAllOrdersUseCase @Inject constructor(private val dbRepository: DbRepository) :
    FlowUseCase<Nothing, List<Order>>() {
    override suspend fun exec(param: Nothing?): Flow<Either<Failure, List<Order>>> {

        return dbRepository.getAllOrders()
    }


}