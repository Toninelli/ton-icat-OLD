package com.toninelli.ton_icat.domain

import com.toninelli.ton_icat.data.database.DbRepository
import com.toninelli.ton_icat.data.network.NetworkRepository
import com.toninelli.ton_icat.model.Order
import com.toninelli.ton_icat.vo.*
import java.util.*
import javax.inject.Inject

class SendOrderUseCase @Inject constructor(
    private val dbRepository: DbRepository,
    private val networkRepository: NetworkRepository
) : UseCase<SendOrderUseCase.Param, SingleEventWrapper<BooleanResult>>() {

    data class Param(val orderId: Long)

    override suspend fun exec(param: Param?): Either<Failure, SingleEventWrapper<BooleanResult>> {

        val response = networkRepository.sendOrder()

        return response.rightOrNull()?.let {resp ->
            when (resp.value) {
                true -> {
                    dbRepository.updateOrderState(param?.orderId!!, Order.State.TRANSMITTED)
                    dbRepository.updateTransmissionDate(param.orderId, Date())
                    right(SingleEventWrapper(resp))
                }
                false -> {
                    left(Failure.Error(resp.msg))
                }
            }
        } ?: left(response.leftOrNull()!!)

    }

}