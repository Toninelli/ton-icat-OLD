package com.toninelli.ton_icat.domain

import com.toninelli.fileutil.logd
import com.toninelli.ton_icat.data.network.NetworkRepository
import com.toninelli.ton_icat.model.SyncProgress
import com.toninelli.ton_icat.vo.Either
import com.toninelli.ton_icat.vo.Failure
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class ExeSyncUseCase @Inject constructor(val repo: NetworkRepository) :
    FlowUseCase<Nothing, SyncProgress>() {

    override suspend fun exec(param: Nothing?): Flow<Either<Failure, SyncProgress>> {
        return repo.syncCatalog()
    }

}