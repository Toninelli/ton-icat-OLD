package com.toninelli.ton_icat.domain

import com.toninelli.ton_icat.data.database.DbRepository
import com.toninelli.ton_icat.model.Agent
import com.toninelli.ton_icat.vo.*
import javax.inject.Inject

class SaveAgentUseCase @Inject constructor(private val dbRepository: DbRepository): UseCase<SaveAgentUseCase.Param, SingleEventWrapper<BooleanResult>>() {

    data class Param(val agent: Agent)

    override suspend fun exec(param: Param?): Either<Failure, SingleEventWrapper<BooleanResult>> {
        return dbRepository.saveAgent(param?.agent!!).toSingle()
    }

}