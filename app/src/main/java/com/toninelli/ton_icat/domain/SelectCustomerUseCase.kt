package com.toninelli.ton_icat.domain

import com.toninelli.ton_icat.data.database.DbRepository
import com.toninelli.ton_icat.model.Customer
import com.toninelli.ton_icat.vo.Either
import com.toninelli.ton_icat.vo.Failure
import javax.inject.Inject

class SelectCustomerUseCase @Inject constructor(private val dbRepository: DbRepository) : UseCase<SelectCustomerUseCase.Param, Boolean>() {

    data class Param(val id: String, val select: Boolean)

    override suspend fun exec(param: Param?): Either<Failure, Boolean> {
        return dbRepository.selectCustomer(param?.id!!, param.select)
    }

}