package com.toninelli.ton_icat.di.module

import android.content.Context
import androidx.room.Room
import com.toninelli.ton_icat.data.database.*
import com.toninelli.ton_icat.di.scope.ApplicationScope
import dagger.Module
import dagger.Provides


@Module
class DbModule {

    @ApplicationScope
    @Provides
    fun provideDb(context: Context): AppDatabase {
        return Room.databaseBuilder(context, AppDatabase::class.java, "tonIcatDB")
            .fallbackToDestructiveMigration()
            .build()

    }

    @ApplicationScope
    @Provides
    fun provideCategoryDao(db: AppDatabase): CategoryDao {
        return db.categoryDao()
    }

    @ApplicationScope
    @Provides
    fun provideCatalogItemDao(db: AppDatabase): CatalogItemDao {
        return db.catalogItemDao()
    }

    @ApplicationScope
    @Provides
    fun provideCustomerDao(db: AppDatabase): CustomerDao{
        return db.customerDao()
    }

    @ApplicationScope
    @Provides
    fun provideSelectedCustomerDao(db: AppDatabase): SelectedCustomerDao{
        return db.selectedCustomerDao()
    }

    @ApplicationScope
    @Provides
    fun provideOrderDao(db: AppDatabase): OrderDao{
        return db.orderDao()
    }

    @ApplicationScope
    @Provides
    fun provideOrderRowDao(db: AppDatabase): OrderRowDao{
        return db.orderRowDao()
    }

    @ApplicationScope
    @Provides
    fun provideSupplierDao(db: AppDatabase): SupplierDao{
        return db.supplierDao()
    }

    @ApplicationScope
    @Provides
    fun provideOfferDao(db: AppDatabase): OfferDao{
        return db.offerDao()
    }

    @ApplicationScope
    @Provides
    fun provideAgentDao(db: AppDatabase): AgentDao{
        return db.agentDao()
    }
}