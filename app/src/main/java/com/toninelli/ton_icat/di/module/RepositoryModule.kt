package com.toninelli.ton_icat.di.module

import com.toninelli.ton_icat.data.database.DbRepo
import com.toninelli.ton_icat.data.database.DbRepository
import com.toninelli.ton_icat.data.network.NetworkRepo
import com.toninelli.ton_icat.data.network.NetworkRepository
import com.toninelli.ton_icat.di.scope.ApplicationScope
import dagger.Binds
import dagger.Module

@Module
abstract class RepositoryModule {

    @ApplicationScope
    @Binds
    abstract fun getNetworkRepo(repo: NetworkRepo): NetworkRepository

    @ApplicationScope
    @Binds
    abstract fun getDbRepo(repo: DbRepo): DbRepository

}