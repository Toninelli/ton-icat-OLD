package com.toninelli.ton_icat.di.component

import com.toninelli.ton_icat.di.module.LoginActivityModule
import com.toninelli.ton_icat.di.scope.ActivityScope
import com.toninelli.ton_icat.di.module.MainActivityModule
import com.toninelli.ton_icat.ui.login.LoginActivity
import com.toninelli.ton_icat.ui.main.MainActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class  SubComponentBuilder {

    @ActivityScope
    @ContributesAndroidInjector(modules = [MainActivityModule::class])
    abstract fun getMainActivity(): MainActivity

    @ActivityScope
    @ContributesAndroidInjector(modules = [LoginActivityModule::class])
    abstract fun getLoginActivity(): LoginActivity

}
