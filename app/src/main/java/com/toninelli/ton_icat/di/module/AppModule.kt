package com.toninelli.ton_icat.di.module

import android.content.Context
import com.toninelli.ton_icat.di.scope.ApplicationScope
import com.toninelli.ton_icat.TonIcatApplication

import dagger.Binds
import dagger.Module

@Module(includes = [NetModule::class, ViewModelModule::class, RepositoryModule::class, DbModule::class])
abstract class AppModule {

    @ApplicationScope
    @Binds
    abstract fun getContext(context: TonIcatApplication): Context

}