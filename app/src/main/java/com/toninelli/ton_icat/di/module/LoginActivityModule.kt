package com.toninelli.ton_icat.di.module

import com.toninelli.ton_icat.di.scope.FragmentScope
import com.toninelli.ton_icat.ui.login.LoginFragment
import com.toninelli.ton_icat.ui.main.home.HomeFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class LoginActivityModule {

    @FragmentScope
    @ContributesAndroidInjector
    abstract fun getLoginFragment(): LoginFragment

}