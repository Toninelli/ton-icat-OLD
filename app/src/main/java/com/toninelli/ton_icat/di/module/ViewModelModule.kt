package com.toninelli.ton_icat.di.module

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.toninelli.ton_icat.di.scope.ViewModelKey
import com.toninelli.ton_icat.ui.common.icatoption.IcatOptionDialogViewModel
import com.toninelli.ton_icat.ui.common.ViewModelFactory
import com.toninelli.ton_icat.ui.login.LoginViewModel
import com.toninelli.ton_icat.ui.main.MainViewModel
import com.toninelli.ton_icat.ui.main.catalog.CatalogItemDetailViewModel
import com.toninelli.ton_icat.ui.main.catalog.CatalogViewModel
import com.toninelli.ton_icat.ui.main.customer.CustomerDetailViewModel
import com.toninelli.ton_icat.ui.main.customer.CustomerHistoryViewModel
import com.toninelli.ton_icat.ui.main.customer.CustomerViewModel
import com.toninelli.ton_icat.ui.main.home.HomeViewModel
import com.toninelli.ton_icat.ui.main.home.PdfOfferDialogViewModel
import com.toninelli.ton_icat.ui.main.order.*
import com.toninelli.ton_icat.ui.main.sync.LastSyncViewModel
import com.toninelli.ton_icat.ui.main.sync.SyncProgressViewModel
import com.toninelli.ton_icat.ui.main.sync.SyncViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class ViewModelModule {


    @Binds
    @IntoMap
    @ViewModelKey(HomeViewModel::class)
    abstract fun bindsHomeViewModel(homeViewModel: HomeViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(PdfOfferDialogViewModel::class)
    abstract fun bindsPdfOfferDialogViewModel(pdfOfferDialogViewModel: PdfOfferDialogViewModel): ViewModel


    @Binds
    @IntoMap
    @ViewModelKey(CatalogViewModel::class)
    abstract fun bindsCatalogViewModel(catalogViewModel: CatalogViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(SyncViewModel::class)
    abstract fun bindsSyncViewModel(syncViewModel: SyncViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(SyncProgressViewModel::class)
    abstract fun bindsSyncProgressViewModel(syncProgressViewModel: SyncProgressViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(LastSyncViewModel::class)
    abstract fun bindsLastSyncViewModel(viewModel: LastSyncViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(IcatOptionDialogViewModel::class)
    abstract fun bindsIcatOptionDialogViewModel(viewModel: IcatOptionDialogViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(CustomerViewModel::class)
    abstract fun bindsCustomerViewModel(viewModel: CustomerViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(CustomerDetailViewModel::class)
    abstract fun bindsCustomerDetailViewModel(viewModel: CustomerDetailViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(MainViewModel::class)
    abstract fun bindsMainViewModel(viewModel: MainViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(OrderViewModel::class)
    abstract fun bindsOrderViewModel(viewModel: OrderViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(BalanceViewModel::class)
    abstract fun bindsBalanceViewModel(viewModel: BalanceViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(OrderDetailViewModel::class)
    abstract fun bindsOrderDetailViewModel(viewModel: OrderDetailViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(OrderHeaderViewModel::class)
    abstract fun bindsOrderHeaderViewModel(viewModel: OrderHeaderViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(AddRowDialogViewModel::class)
    abstract fun bindsAddRowDialogViewModel(viewModel: AddRowDialogViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(CatalogItemDetailViewModel::class)
    abstract fun bindsCatalogItemDetailViewModel(viewModel: CatalogItemDetailViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(LoginViewModel::class)
    abstract fun bindsLoginViewModel(viewModel: LoginViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(CustomerHistoryViewModel::class)
    abstract fun bindsCustomerHistoryViewModel(viewModel: CustomerHistoryViewModel): ViewModel

    @Binds
    abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory


}