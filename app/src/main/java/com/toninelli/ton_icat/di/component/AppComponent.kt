package com.toninelli.ton_icat.di.component

import com.toninelli.ton_icat.di.scope.ApplicationScope
import com.toninelli.ton_icat.di.module.AppModule
import com.toninelli.ton_icat.TonIcatApplication
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule

@ApplicationScope
@Component(
    modules = [
        AndroidInjectionModule::class,
        AppModule::class,
        SubComponentBuilder::class
    ]
)
interface AppComponent {

    @Component.Builder
    interface Builder {

        @BindsInstance
        fun application(app: TonIcatApplication): Builder

        fun build(): AppComponent

    }

    fun inject(app: TonIcatApplication)
}