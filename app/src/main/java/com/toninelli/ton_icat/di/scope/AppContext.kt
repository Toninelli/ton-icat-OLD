package com.toninelli.ton_icat.di.scope

import javax.inject.Qualifier

@Qualifier
annotation class AppContext {
}