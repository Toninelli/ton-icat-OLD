package com.toninelli.ton_icat.di.module

import com.toninelli.ton_icat.di.scope.ApplicationScope
import com.example.networkcalladapterlib.NetworkCallAdapterFactory
import com.toninelli.ton_icat.data.network.RemoteApi
import com.toninelli.ton_icat.data.network.SyncApi
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Named

@Module
class NetModule {

    @ApplicationScope
    @Provides
    fun provideRemoteApiService(client: OkHttpClient): RemoteApi {

        return Retrofit.Builder()
            .baseUrl("https://icat.toninelli.it/")
            .client(client)
            .addCallAdapterFactory(NetworkCallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create(RemoteApi::class.java)
    }

    @ApplicationScope
    @Named("sync")
    @Provides
    fun provideSyncApiService( @Named("sync") client: OkHttpClient): SyncApi {

        return Retrofit.Builder()
            .baseUrl("https://icat.toninelli.it/")
            .client(client)
            .addCallAdapterFactory(NetworkCallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create(SyncApi::class.java)
    }


    @ApplicationScope
    @Named("sync")
    @Provides
    fun provideSyncHttpClient(): OkHttpClient {
        return OkHttpClient.Builder()
            .addInterceptor(HttpLoggingInterceptor().apply {
                this.level = HttpLoggingInterceptor.Level.BODY
            })
            .connectTimeout(5, TimeUnit.MINUTES)
            .readTimeout(5, TimeUnit.MINUTES)
            .build()
    }

    @ApplicationScope
    @Provides
    fun provideRemoteHttpClient(): OkHttpClient {
        return OkHttpClient.Builder()
            .addInterceptor(HttpLoggingInterceptor().apply {
                this.level = HttpLoggingInterceptor.Level.BODY
            })
            .connectTimeout(10, TimeUnit.SECONDS)
            .readTimeout(10, TimeUnit.SECONDS)
            .build()
    }


}