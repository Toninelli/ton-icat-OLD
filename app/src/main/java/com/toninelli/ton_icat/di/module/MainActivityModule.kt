package com.toninelli.ton_icat.di.module

import com.toninelli.ton_icat.di.scope.FragmentScope
import com.toninelli.ton_icat.ui.common.icatoption.IcatOptionDialog
import com.toninelli.ton_icat.ui.main.catalog.CatalogFragment
import com.toninelli.ton_icat.ui.main.catalog.CatalogItemDetailDialog
import com.toninelli.ton_icat.ui.main.customer.CustomerDetailFragment
import com.toninelli.ton_icat.ui.main.customer.CustomerFragment
import com.toninelli.ton_icat.ui.main.customer.CustomerHistoryFragment
import com.toninelli.ton_icat.ui.main.home.HomeFragment
import com.toninelli.ton_icat.ui.main.home.PdfOfferDialog
import com.toninelli.ton_icat.ui.main.order.*
import com.toninelli.ton_icat.ui.main.sync.LastSyncDialog
import com.toninelli.ton_icat.ui.main.sync.SyncFragment
import com.toninelli.ton_icat.ui.main.sync.SyncProgressDialog
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class MainActivityModule {


    @FragmentScope
    @ContributesAndroidInjector
    abstract fun getHomeFragment(): HomeFragment

    @FragmentScope
    @ContributesAndroidInjector
    abstract fun getCatalogFragment(): CatalogFragment

    @FragmentScope
    @ContributesAndroidInjector
    abstract fun getCatalogItemDetailFragment(): CatalogItemDetailDialog

    @FragmentScope
    @ContributesAndroidInjector
    abstract fun getCustomerFragment(): CustomerFragment

    @FragmentScope
    @ContributesAndroidInjector
    abstract fun getCustomerDetailFragment(): CustomerDetailFragment

    @FragmentScope
    @ContributesAndroidInjector
    abstract fun getCustomerHistoryFragment(): CustomerHistoryFragment

    @FragmentScope
    @ContributesAndroidInjector
    abstract fun getOrderFragment(): OrderFragment

    @FragmentScope
    @ContributesAndroidInjector
    abstract fun getBalanceFragment(): BalanceFragment

    @FragmentScope
    @ContributesAndroidInjector
    abstract fun getOrderDetailFragment(): OrderDetailFragment

    @FragmentScope
    @ContributesAndroidInjector
    abstract fun getAddRowDialog(): AddRowDialog

    @FragmentScope
    @ContributesAndroidInjector
    abstract fun getOrderHeaderFragment(): OrderHeaderFragment

    @FragmentScope
    @ContributesAndroidInjector
    abstract fun getSyncFragment(): SyncFragment

    @FragmentScope
    @ContributesAndroidInjector
    abstract fun getNewsPdfDialog(): PdfOfferDialog

    @FragmentScope
    @ContributesAndroidInjector
    abstract fun getSyncDialog(): SyncProgressDialog

    @FragmentScope
    @ContributesAndroidInjector
    abstract fun getLastSyncDialog(): LastSyncDialog

    @FragmentScope
    @ContributesAndroidInjector
    abstract fun getIcatOptionDialog(): IcatOptionDialog


}