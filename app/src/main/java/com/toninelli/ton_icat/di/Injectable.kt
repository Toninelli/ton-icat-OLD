package com.toninelli.ton_icat.di

/**
 * Marks an activity / fragment injectable.
 */
interface Injectable
