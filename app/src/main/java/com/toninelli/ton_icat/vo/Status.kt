package com.toninelli.ton_icat.vo

enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}