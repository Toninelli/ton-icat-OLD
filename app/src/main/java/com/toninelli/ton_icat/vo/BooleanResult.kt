package com.toninelli.ton_icat.vo

data class BooleanResult(val value: Boolean, val msg: String, val exception: Exception? = null)