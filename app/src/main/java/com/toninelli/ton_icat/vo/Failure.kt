package com.toninelli.ton_icat.vo

@Suppress("UNDERSCORE")
sealed class Failure(val msg: String) : Exception(msg) {

    object UnknownError : Failure("Unknown Exception")
    class Error(msg: String) : Failure(msg)
    class EmptyData(msg: String = "No Data") : Failure(msg)
    object NoConnection : Failure("No connection")
    class LoginError(msg: String) : Failure(msg)
    class NetworkError(msg: String) : Failure(msg)
    class SyncCatalogError(msg: String) : Failure(msg)
}



