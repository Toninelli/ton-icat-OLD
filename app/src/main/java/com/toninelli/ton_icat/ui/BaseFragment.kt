package com.toninelli.ton_icat.ui

import android.content.Context
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.toninelli.ton_icat.ui.common.ViewModelFactory
import com.toninelli.ton_icat.ui.login.LoginActivity
import com.toninelli.ton_icat.ui.main.MainActivity
import javax.inject.Inject

abstract class BaseFragment : Fragment() {

    @Inject
    lateinit var factory: ViewModelFactory

    lateinit var activity: AppCompatActivity

    override fun onAttach(context: Context) {
        super.onAttach(context)
        activity = context as AppCompatActivity
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }




}