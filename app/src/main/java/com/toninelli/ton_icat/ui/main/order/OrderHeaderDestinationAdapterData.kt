package com.toninelli.ton_icat.ui.main.order

import androidx.recyclerview.widget.DiffUtil
import com.toninelli.ton_icat.R
import com.toninelli.ton_icat.binding.BindableListAdapterData
import com.toninelli.ton_icat.databinding.OrderHeaderDestinationItemBinding
import com.toninelli.ton_icat.model.Customer
import com.toninelli.ton_icat.ui.common.BaseAdapter
import com.toninelli.ton_icat.vo.Resource
import com.toninelli.ton_icat.vo.case

class OrderHeaderDestinationAdapterData(private val callback: (Customer.Address) -> Unit): BaseAdapter<Customer.Address, OrderHeaderDestinationItemBinding>(
    R.layout.order_header_destination_item, object : DiffUtil.ItemCallback<Customer.Address>(){
        override fun areItemsTheSame(
            oldItem: Customer.Address,
            newItem: Customer.Address
        ): Boolean {
            return oldItem == newItem
        }

        override fun areContentsTheSame(
            oldItem: Customer.Address,
            newItem: Customer.Address
        ): Boolean {
            return oldItem.alias == newItem.alias
        }

    }), BindableListAdapterData<Customer> {

    var selectedDestination: Customer.Address? = null

    override fun onBindViewHolder(
        binding: OrderHeaderDestinationItemBinding,
        item: Customer.Address,
        position: Int
    ) {
        binding.destination = item

        binding.orderHeaderDestinationRadioButton.setOnClickListener {
            callback(item)
            selectedDestination = item
            notifyDataSetChanged()
        }

        binding.orderHeaderDestinationRadioButton.isChecked = item == selectedDestination

    }

    override fun setData(data: Resource<Customer>) {
        data.case(success = {
            it.data?.let {
                submitList(it.address)

            }
        })
    }


}