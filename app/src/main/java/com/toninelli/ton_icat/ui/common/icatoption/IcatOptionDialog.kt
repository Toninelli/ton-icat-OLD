package com.toninelli.ton_icat.ui.common.icatoption

import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.appyvet.materialrangebar.RangeBar
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.toninelli.fileutil.autoclearedValue
import com.toninelli.fileutil.logd
import com.toninelli.ton_icat.R
import com.toninelli.ton_icat.databinding.DialogIcatOptionBinding
import com.toninelli.ton_icat.di.Injectable
import com.toninelli.ton_icat.icatcontext.RunTimeValue
import com.toninelli.ton_icat.ui.BaseFragment
import com.toninelli.ton_icat.ui.common.ViewModelFactory
import javax.inject.Inject

class IcatOptionDialog private constructor(
    private val baseFragment: BaseFragment,
    private val showPriceFilterValue: Boolean?,
    private val showVisibilityOptionValue: Boolean?
    ) : BottomSheetDialogFragment(), Injectable {

    @Inject
    lateinit var factory: ViewModelFactory
    private val viewModel: IcatOptionDialogViewModel by viewModels { factory }
    var binding by autoclearedValue<DialogIcatOptionBinding>()
    var adapter by autoclearedValue<IcatOptionAdapterData>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.dialog_icat_option, container, false)
        binding.lifecycleOwner = this
        binding.model = viewModel

        binding.icatOptionList.layoutManager = GridLayoutManager(
            activity, 3,
            RecyclerView.VERTICAL, false
        )

        adapter = IcatOptionAdapterData {
            viewModel.setIcatOption(it)
        }

        binding.icatOptionRange.setOnRangeBarChangeListener(object : RangeBar.OnRangeBarChangeListener{
            override fun onTouchEnded(rangeBar: RangeBar?) {

                RunTimeValue.rangeValue.value?.let {
                    if(rangeBar?.leftPinValue?.toInt()!! != it[0] || rangeBar.rightPinValue?.toInt()!! != it[1])
                        RunTimeValue.rangeValue.postValue(arrayOf(rangeBar.leftPinValue?.toInt()!!,rangeBar.rightPinValue?.toInt()!!))
                } ?:  RunTimeValue.rangeValue.postValue(arrayOf(rangeBar?.leftPinValue?.toInt()!!,rangeBar.rightPinValue?.toInt()!!))

            }

            override fun onRangeChangeListener(
                rangeBar: RangeBar?,
                leftPinIndex: Int,
                rightPinIndex: Int,
                leftPinValue: String?,
                rightPinValue: String?
            ) {
                /* nothing */
            }

            override fun onTouchStarted(rangeBar: RangeBar?) {
               /* nothing */
            }

        })

        RunTimeValue.rangeValue.value?.let {
            binding.icatOptionRange.setRangePinsByValue(it[0].toFloat(),it[1].toFloat())
        }

        binding.icatOptionList.adapter = adapter

        binding.showPriceFilter = showPriceFilterValue
        binding.showVisibilityOption = showVisibilityOptionValue

        return binding.root
    }

    fun show() {
        this.show(baseFragment.activity.supportFragmentManager, "Icat Option Dialog")
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = super.onCreateDialog(savedInstanceState)
        (dialog as BottomSheetDialog).behavior.peekHeight = BottomSheetBehavior.PEEK_HEIGHT_AUTO
        return dialog
    }

    data class Builder(val baseFragment: BaseFragment){

        private var showPriceFilter: Boolean? = null

        private var showVisibilityOption: Boolean? = null

        fun showPriceFilter(value: Boolean) = apply { this.showPriceFilter = value }

        fun showVisibilityOption(value: Boolean) = apply{ this.showVisibilityOption = value }

        fun build(): IcatOptionDialog {
            return IcatOptionDialog(
                baseFragment,
                showPriceFilter,
                showVisibilityOption
            )
        }

    }


}