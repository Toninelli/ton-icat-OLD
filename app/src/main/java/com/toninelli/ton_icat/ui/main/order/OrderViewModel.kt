package com.toninelli.ton_icat.ui.main.order

import androidx.lifecycle.ViewModel
import com.toninelli.ton_icat.domain.*
import com.toninelli.ton_icat.extension.switchRes
import com.toninelli.ton_icat.model.Order
import javax.inject.Inject

class OrderViewModel @Inject constructor(
    private val getSelectedCustomerUseCase: GetSelectedCustomerUseCase,
    private val getCustomerByIdUseCase: GetCustomerByIdUseCase,
    private val getOrderByCustomerUseCase: GetOrderByCustomerUseCase,
    private val insertOrderUseCase: InsertOrderUseCase,
    private val deleteOrderUseCase: DeleteOrderUseCase
) : ViewModel() {

    init {
        exec(getSelectedCustomerUseCase, null)
    }

    val customerResult = getSelectedCustomerUseCase.resultLiveData.switchRes {
        exec(getCustomerByIdUseCase, GetCustomerByIdUseCase.Param(it.data?.customerId ?: "null"))
        getCustomerByIdUseCase.resultLiveData

    }

    val orderResult = getSelectedCustomerUseCase.resultLiveData.switchRes {
        exec(getOrderByCustomerUseCase, GetOrderByCustomerUseCase.Param(it.data?.customerId ?: "null"))
        getOrderByCustomerUseCase.resultLiveData
    }

    val insertResult = insertOrderUseCase.resultLiveData

    fun insertOrder() {
        exec(insertOrderUseCase, InsertOrderUseCase.Param(customerResult.value?.data?.customerId!!))
    }

    fun deleteOrder(order: Order) {
        exec(deleteOrderUseCase, DeleteOrderUseCase.Param(order.orderId))
    }

}