package com.toninelli.ton_icat.ui.main.catalog

import androidx.lifecycle.ViewModel
import com.toninelli.ton_icat.domain.GetCatalogItemByCodListUseCase
import com.toninelli.ton_icat.domain.exec
import javax.inject.Inject

class CatalogItemDetailViewModel @Inject constructor(
    private val getCatalogItemByCodListUseCase: GetCatalogItemByCodListUseCase
): ViewModel(){


    val itemResult = getCatalogItemByCodListUseCase.resultLiveData

    fun loadItem(id: Int, customerId: String?){
        exec(getCatalogItemByCodListUseCase, GetCatalogItemByCodListUseCase.Param(listOf(id), customerId))
    }
}