package com.toninelli.ton_icat.ui.main.order


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.RecyclerView
import com.toninelli.fileutil.autoclearedValue
import com.toninelli.ton_icat.NavGraphDirections
import com.toninelli.ton_icat.R
import com.toninelli.ton_icat.databinding.FragmentOrderDetailBinding
import com.toninelli.ton_icat.di.Injectable
import com.toninelli.ton_icat.model.OrderRowActionType
import com.toninelli.ton_icat.ui.BaseFragment


class OrderDetailFragment : BaseFragment(), Injectable {

    var binding by autoclearedValue<FragmentOrderDetailBinding>()
    val model: OrderDetailViewModel by viewModels { factory }
    private var rowAdapter  by autoclearedValue<OrderRowAdapterData>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentOrderDetailBinding.inflate(inflater, container, false)

        binding.lifecycleOwner = this

        binding.model = model

        rowAdapter = OrderRowAdapterData { orderRowActionType, orderRow ->
            when(orderRowActionType){
                OrderRowActionType.HEADER -> {
                    OrderRowHeaderDialog(orderRow).show(activity.supportFragmentManager, "header")
                }
                OrderRowActionType.DELETE -> {model.deleteRow(orderRow)}
                OrderRowActionType.DISCOUNT -> { model.applyDiscount(orderRow) }
            }
        }

        binding.orderDetailRowList.adapter = rowAdapter

        binding.orderDetailRowList.addItemDecoration(DividerItemDecoration(activity,RecyclerView.VERTICAL))

        binding.orderDetailAddRowButton.setOnClickListener {
            model.orderResult.value?.let {
                val dialog = AddRowDialog(it)
                dialog.show(activity.supportFragmentManager, "AddRow")
            }

        }

        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        val orderId = navArgs<OrderDetailFragmentArgs>().value.order

        model.loadOrderRow(orderId)

    }
}
