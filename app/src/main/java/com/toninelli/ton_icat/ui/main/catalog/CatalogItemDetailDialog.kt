package com.toninelli.ton_icat.ui.main.catalog


import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.viewModels
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.toninelli.fileutil.autoclearedValue
import com.toninelli.ton_icat.R
import com.toninelli.ton_icat.databinding.DialogCatalogItemDetailBinding
import com.toninelli.ton_icat.di.Injectable
import com.toninelli.ton_icat.extension.observe
import com.toninelli.ton_icat.ui.common.ViewModelFactory
import javax.inject.Inject


class CatalogItemDetailDialog(private val itemId: Int, private val customerId: String?) : DialogFragment(), Injectable {

    @Inject
    lateinit var factory: ViewModelFactory

    var binding by autoclearedValue<DialogCatalogItemDetailBinding>()

    var itemAdapter by autoclearedValue<CatalogItemDetailItemAdapter>()

    private val model: CatalogItemDetailViewModel by viewModels { factory }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return activity?.let {
            val builder = MaterialAlertDialogBuilder(it)

            val inflater = requireActivity().layoutInflater

            binding = DialogCatalogItemDetailBinding.inflate(inflater)

            binding.model = model

            binding.lifecycleOwner = this

            itemAdapter = CatalogItemDetailItemAdapter()

            binding.itemList.adapter = itemAdapter

            builder.setView(binding.root)

            builder.create()
        }?: throw IllegalStateException("Activity must not be null")

    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        model.loadItem(itemId, customerId)

        observe(model.itemResult) {
            it?.data?.get(0)?.let {item ->
                val adapter = ImageCollectionAdapter(this, item.article.images)
                binding.pager.adapter = adapter

            }
        }

    }

    override fun onStart() {
        super.onStart()
        dialog?.window!!.setLayout(1500, 1800)
    }

}
