package com.toninelli.ton_icat.ui.main.catalog

import android.app.AlertDialog
import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.DialogFragment
import com.toninelli.fileutil.RxBus
import com.toninelli.fileutil.autoclearedValue
import com.toninelli.fileutil.logd
import com.toninelli.ton_icat.R
import com.toninelli.ton_icat.databinding.DialogPhotoZoomableBinding
import java.lang.IllegalStateException

class PhotoZoomableDialog(val name: String) : DialogFragment() {

    var binding by autoclearedValue<DialogPhotoZoomableBinding>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DialogPhotoZoomableBinding.inflate(inflater)
        binding.name = name


        return binding.root
    }

    override fun onStart() {
        super.onStart()
        dialog?.window!!.setLayout(1300, 1300)


    }



}