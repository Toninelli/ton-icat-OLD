package com.toninelli.ton_icat.ui.common

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView

abstract class BaseAdapter<Item, Binding>(
    private val layoutId: Int,
    private val diffCallback: DiffUtil.ItemCallback<Item>
) : ListAdapter<Item, BaseAdapter.BaseViewHolder<Binding>>(diffCallback) where Binding : ViewDataBinding {

    protected lateinit var binding: Binding

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder<Binding> {
        binding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            layoutId,
            parent,
            false
        )

       return onCreateViewHolder(binding, viewType)
    }

    override fun onBindViewHolder(holder: BaseViewHolder<Binding>, position: Int) {
        onBindViewHolder(holder.binding, getItem(position),position)
    }

    open fun onCreateViewHolder(binding: Binding, viewType: Int): BaseViewHolder<Binding> {
        return BaseViewHolder(binding)
    }

    abstract fun onBindViewHolder(binding: Binding, item: Item, position: Int)

    open class BaseViewHolder<out T : ViewDataBinding> constructor(val binding: T) :
        RecyclerView.ViewHolder(binding.root)

}