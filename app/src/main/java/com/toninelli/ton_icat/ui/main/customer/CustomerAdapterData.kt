package com.toninelli.ton_icat.ui.main.customer


import androidx.recyclerview.widget.DiffUtil
import com.toninelli.ton_icat.R
import com.toninelli.ton_icat.binding.BindableListAdapterData
import com.toninelli.ton_icat.binding.BindableListAdapterInformation
import com.toninelli.ton_icat.databinding.CustomerItemBinding
import com.toninelli.ton_icat.model.Customer
import com.toninelli.ton_icat.model.Order
import com.toninelli.ton_icat.ui.common.BaseAdapter
import com.toninelli.ton_icat.vo.Resource
import com.toninelli.ton_icat.vo.case

class CustomerAdapterData(private val callback: (Customer) -> Unit) :
    BaseAdapter<Customer, CustomerItemBinding>(
        R.layout.customer_item, object : DiffUtil.ItemCallback<Customer>() {
            override fun areItemsTheSame(oldItem: Customer, newItem: Customer): Boolean {
                return oldItem.customerId == newItem.customerId
            }

            override fun areContentsTheSame(oldItem: Customer, newItem: Customer): Boolean {
                return oldItem.name == newItem.name
            }

        }), BindableListAdapterData<List<Customer>>, BindableListAdapterInformation<List<Order>> {


    private var orders: List<Order>? = null

    override fun onBindViewHolder(binding: CustomerItemBinding, item: Customer, position: Int) {
        binding.customer = item

        orders?.let {
            binding.warningVisibility =
                it.firstOrNull { it.customerId == item.customerId && it.state == Order.State.NOT_COMPLETED } != null
        }

        binding.root.setOnClickListener {
            binding.customer?.let(callback)
        }
    }

    override fun setData(data: Resource<List<Customer>>) {
        data.case(success = { submitList(it.data) })
    }

    fun getCustomer(position: Int): Customer {
        return getItem(position)
    }

    override fun setInformation(info: List<Order>) {
        this.orders = info
    }

}