package com.toninelli.ton_icat.ui.common.icatoption

import androidx.recyclerview.widget.DiffUtil
import com.toninelli.ton_icat.R
import com.toninelli.ton_icat.binding.BindableListAdapterData
import com.toninelli.ton_icat.databinding.CatalogOptionItemBinding
import com.toninelli.ton_icat.model.VisibilityOption
import com.toninelli.ton_icat.ui.common.BaseAdapter
import com.toninelli.ton_icat.vo.Resource
import com.toninelli.ton_icat.vo.case

class IcatOptionAdapterData(private val callback: (VisibilityOption) -> Unit) :
    BaseAdapter<VisibilityOption, CatalogOptionItemBinding>(diffCallback = object :
        DiffUtil.ItemCallback<VisibilityOption>() {
        override fun areItemsTheSame(oldItem: VisibilityOption, newItem: VisibilityOption): Boolean {
            return oldItem == newItem
        }

        override fun areContentsTheSame(oldItem: VisibilityOption, newItem: VisibilityOption): Boolean {
            return oldItem.name == newItem.name
        }
    }, layoutId = R.layout.catalog_option_item), BindableListAdapterData<List<VisibilityOption>> {
    override fun onBindViewHolder(
        binding: CatalogOptionItemBinding,
        item: VisibilityOption,
        position: Int
    ) {
        binding.option = item
        binding.optionSwitch.setOnCheckedChangeListener { _, isChecked ->
           binding.option?.let { callback(VisibilityOption(it.name, isChecked)) }
        }
    }

    override fun setData(data: Resource<List<VisibilityOption>>) {
        data.case(success = {submitList(it.data)})
    }
}