package com.toninelli.ton_icat.ui.main.order

import androidx.lifecycle.ViewModel
import androidx.lifecycle.map
import com.toninelli.ton_icat.domain.*
import com.toninelli.ton_icat.extension.amount
import com.toninelli.ton_icat.extension.switchRes
import com.toninelli.ton_icat.model.OrderRow
import com.toninelli.ton_icat.vo.case
import javax.inject.Inject

class OrderDetailViewModel @Inject constructor(
    private val getOrderRowByOrderUseCase: GetOrderRowByOrderUseCase,
    private val getOrderByIdUseCase: GetOrderByIdUseCase,
    private val getCustomerByIdUseCase: GetCustomerByIdUseCase,
    private val deleteOrderRowUseCase: DeleteOrderRowUseCase,
    private val loadSaveDiscountedPriceUseCase: LoadSaveDiscountedPriceUseCase
): ViewModel() {

    val rowResult = getOrderRowByOrderUseCase.resultLiveData

    val orderResult = getOrderByIdUseCase.resultLiveData.map {
        it?.data
    }

    val totalResult = rowResult.map {
        var total = 0.0F
        it?.data?.let {
            it.forEach { row-> total += row.amount() }
        }
        total
    }

    val customerResult = getOrderByIdUseCase.resultLiveData.switchRes {
        exec(getCustomerByIdUseCase, GetCustomerByIdUseCase.Param(it.data?.customerId ?: "null"))
        getCustomerByIdUseCase.resultLiveData
    }

    val deleteResult = deleteOrderRowUseCase.resultLiveData

    val applyDiscountResult = loadSaveDiscountedPriceUseCase.resultLiveData

    fun loadOrderRow(orderId: Long){
        exec(getOrderRowByOrderUseCase, GetOrderRowByOrderUseCase.Param(orderId))
        exec(getOrderByIdUseCase, GetOrderByIdUseCase.Param(orderId))
    }

    fun deleteRow(orderRow: OrderRow){
        exec(deleteOrderRowUseCase, DeleteOrderRowUseCase.Param(orderRow))
    }

    fun applyDiscount(orderRow: OrderRow){
        exec(loadSaveDiscountedPriceUseCase, LoadSaveDiscountedPriceUseCase.Param(orderRow.customerId!!, orderRow.catalogItem.article.artCod, orderRow.orderRowId))
    }

}