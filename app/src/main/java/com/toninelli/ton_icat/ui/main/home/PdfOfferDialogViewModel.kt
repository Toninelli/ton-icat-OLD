package com.toninelli.ton_icat.ui.main.home

import androidx.lifecycle.ViewModel
import com.toninelli.ton_icat.domain.GetPdfOfferUseCase
import com.toninelli.ton_icat.domain.exec
import javax.inject.Inject

class PdfOfferDialogViewModel @Inject constructor(private val getPdfOfferUseCase: GetPdfOfferUseCase) :
    ViewModel() {

    init {
        exec(getPdfOfferUseCase,null)
    }


    val pdfResult = getPdfOfferUseCase.resultLiveData

}