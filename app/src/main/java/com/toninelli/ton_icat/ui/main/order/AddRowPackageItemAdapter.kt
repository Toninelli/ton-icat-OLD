package com.toninelli.ton_icat.ui.main.order

import androidx.recyclerview.widget.DiffUtil
import com.toninelli.ton_icat.R
import com.toninelli.ton_icat.databinding.CatalogItemSelectionPackageItemBinding
import com.toninelli.ton_icat.model.Article.Package
import com.toninelli.ton_icat.ui.common.BaseAdapter

class AddRowPackageItemAdapter(val callback: (Package)-> Unit) : BaseAdapter<Package, CatalogItemSelectionPackageItemBinding>(
    R.layout.catalog_item_selection_package_item, object : DiffUtil.ItemCallback<Package>(){
        override fun areItemsTheSame(oldItem: Package, newItem: Package): Boolean {
            return oldItem.packName == newItem.packName
        }

        override fun areContentsTheSame(oldItem: Package, newItem: Package): Boolean {
            return oldItem.packName == newItem.packName
        }
    }){

    var selectedPackage: Package? = null

    override fun onBindViewHolder(
        binding: CatalogItemSelectionPackageItemBinding,
        item: Package,
        position: Int
    ) {
        binding.pack = item
        binding.catalogItemPackageRadioButton.setOnClickListener {
            selectedPackage = item
            callback(item)
            notifyDataSetChanged()
        }

        binding.catalogItemPackageRadioButton.isChecked = selectedPackage == item
    }


}