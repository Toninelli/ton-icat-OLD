package com.toninelli.ton_icat.ui.main.customer


import androidx.recyclerview.widget.DiffUtil
import com.toninelli.ton_icat.R
import com.toninelli.ton_icat.databinding.CustomerHistoryRowItemBinding
import com.toninelli.ton_icat.model.CustomerHistory
import com.toninelli.ton_icat.ui.common.BaseAdapter

class CustomerHistoryRowAdapter(
    val list: List<CustomerHistory.CustomerHistoryRow>,
    val callback: (Int) -> Unit
) : BaseAdapter<CustomerHistory.CustomerHistoryRow, CustomerHistoryRowItemBinding>(
    R.layout.customer_history_row_item,
    object : DiffUtil.ItemCallback<CustomerHistory.CustomerHistoryRow>() {
        override fun areItemsTheSame(
            oldRowCustomer: CustomerHistory.CustomerHistoryRow,
            newRowCustomer: CustomerHistory.CustomerHistoryRow
        ): Boolean {
            return oldRowCustomer == newRowCustomer
        }

        override fun areContentsTheSame(
            oldRowCustomer: CustomerHistory.CustomerHistoryRow,
            newRowCustomer: CustomerHistory.CustomerHistoryRow
        ): Boolean {
            return oldRowCustomer.artCod == newRowCustomer.artCod
        }

    }) {


    init {

        submitList(list)

    }

    override fun onBindViewHolder(
        binding: CustomerHistoryRowItemBinding,
        item: CustomerHistory.CustomerHistoryRow,
        position: Int
    ) {
        binding.item = item
        binding.rowArt.setOnClickListener {
            callback(item.artCod)
        }

    }


}