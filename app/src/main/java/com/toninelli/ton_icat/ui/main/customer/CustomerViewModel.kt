package com.toninelli.ton_icat.ui.main.customer

import androidx.lifecycle.ViewModel
import androidx.lifecycle.map
import com.toninelli.ton_icat.domain.GetAllCustomersUseCase
import com.toninelli.ton_icat.domain.GetAllOrdersUseCase
import com.toninelli.ton_icat.domain.GetCustomerOrdersNotCompletedUseCase
import com.toninelli.ton_icat.domain.exec
import javax.inject.Inject

class CustomerViewModel @Inject constructor(
    private val getAllCustomersUseCase: GetAllCustomersUseCase,
    private val getAllOrdersUseCase: GetAllOrdersUseCase
): ViewModel() {

    val customersResult = getAllCustomersUseCase.resultLiveData

    val ordersResult = getAllOrdersUseCase.resultLiveData.map {
        it?.data
    }

    init {
        exec(getAllCustomersUseCase,null)
        exec(getAllOrdersUseCase, null)
    }


}