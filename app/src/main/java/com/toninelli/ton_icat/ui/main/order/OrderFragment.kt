package com.toninelli.ton_icat.ui.main.order


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.toninelli.fileutil.autoclearedValue
import com.toninelli.ton_icat.NavGraphDirections
import com.toninelli.ton_icat.R
import com.toninelli.ton_icat.binding.ResourceListener
import com.toninelli.ton_icat.databinding.FragmentOrderBinding
import com.toninelli.ton_icat.di.Injectable
import com.toninelli.ton_icat.model.OrderActionType
import com.toninelli.ton_icat.ui.BaseFragment


class OrderFragment : BaseFragment(), Injectable, ResourceListener {

    var binding by autoclearedValue<FragmentOrderBinding>()
    val model: OrderViewModel by viewModels { factory }
    var adapter by autoclearedValue<OrderAdapterData>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentOrderBinding.inflate(inflater, container, false)

        binding.lifecycleOwner = this

        binding.model = model

        adapter = OrderAdapterData(binding.orderList) { orderActionType, orders ->
            when (orderActionType) {
                OrderActionType.DETAIL -> {
                    findNavController().navigate(
                        NavGraphDirections.actionGlobalOrderDetailFragment(
                            orders[0].orderId
                        )
                    )
                }
                OrderActionType.HEADER -> {
                    findNavController().navigate(
                        NavGraphDirections.actionGlobalOrderHeaderFragment(
                            orders[0].orderId
                        )
                    )
                }
                OrderActionType.DELETE -> {
                    if (orders.size > 1) {
                        orders.forEach {model.deleteOrder(it)}
                        

                    } else {
                        MaterialAlertDialogBuilder(activity)
                            .setTitle(R.string.order_delete_action)
                            .setPositiveButton(R.string.button_yes) { _, _ ->
                                model.deleteOrder(
                                    orders[0]
                                )
                            }
                            .setNegativeButton(R.string.button_no) { dialog, _ -> dialog.dismiss() }
                            .show()
                    }
                }
            }
        }

        binding.orderList.adapter = adapter

//        adapter.initSelectionAdapter(this)

        binding.listener = this

        binding.orderAddButton.setOnClickListener {
            model.insertOrder()
        }

        return binding.root
    }

}
