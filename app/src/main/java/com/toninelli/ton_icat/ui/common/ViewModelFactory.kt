package com.toninelli.ton_icat.ui.common

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.toninelli.ton_icat.di.scope.ApplicationScope
import javax.inject.Inject
import javax.inject.Provider


@Suppress("UNCHECKED_CAST")
@ApplicationScope
class ViewModelFactory @Inject constructor(
    private val creators: Map<Class<out ViewModel>, @JvmSuppressWildcards Provider<ViewModel>>
) : ViewModelProvider.Factory {


    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        val creator = creators[modelClass] ?: creators.entries.firstOrNull {
            modelClass.isAssignableFrom(it.key)
        }?.value ?: throw Exception("unknow model class $modelClass")
        try {
            return creator.get() as T
        } catch (t: Throwable) {
            throw RuntimeException(t)
        }
    }


}