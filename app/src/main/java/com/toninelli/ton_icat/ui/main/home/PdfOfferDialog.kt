package com.toninelli.ton_icat.ui.main.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.toninelli.fileutil.autoclearedValue
import com.toninelli.fileutil.logd
import com.toninelli.ton_icat.R
import com.toninelli.ton_icat.databinding.DialogPdfOfferBinding
import com.toninelli.ton_icat.di.Injectable
import com.toninelli.ton_icat.model.Offer
import com.toninelli.ton_icat.ui.common.ViewModelFactory
import javax.inject.Inject

class PdfOfferDialog(val itemOffer: Offer) : DialogFragment(), Injectable {

    @Inject
    lateinit var factory: ViewModelFactory

    private var binding by autoclearedValue<DialogPdfOfferBinding>()
    private var adapter by autoclearedValue<PdfOfferAdapterData>()
    private val modelOfferDialog: PdfOfferDialogViewModel by viewModels { factory }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = DataBindingUtil.inflate(inflater, R.layout.dialog_pdf_offer, container, false)
        adapter = PdfOfferAdapterData { logd(it) }
        binding.model = modelOfferDialog
        binding.offer = itemOffer
        binding.lifecycleOwner = this
        binding.newsPdfList.layoutManager =
            GridLayoutManager(activity, 3, RecyclerView.VERTICAL, false)
        binding.newsPdfList.adapter = adapter
        return binding.root
    }


    override fun onStart() {
        super.onStart()
        dialog?.window!!.setLayout(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.WRAP_CONTENT
        )
//        dialog?.window!!.setGravity(Gravity.TOP)

    }


}