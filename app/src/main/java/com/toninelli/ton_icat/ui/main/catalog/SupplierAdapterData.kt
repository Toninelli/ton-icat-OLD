package com.toninelli.ton_icat.ui.main.catalog

import androidx.recyclerview.widget.DiffUtil
import com.toninelli.ton_icat.R
import com.toninelli.ton_icat.binding.BindableListAdapterData
import com.toninelli.ton_icat.databinding.CatalogSuppItemBinding
import com.toninelli.ton_icat.model.Supplier
import com.toninelli.ton_icat.ui.common.BaseAdapter
import com.toninelli.ton_icat.vo.Resource
import com.toninelli.ton_icat.vo.case

class SupplierAdapterData(val callback: (Supplier) -> Unit) :
    BaseAdapter<Supplier, CatalogSuppItemBinding>(
        R.layout.catalog_supp_item,
        diffCallback = object : DiffUtil.ItemCallback<Supplier>() {
            override fun areItemsTheSame(oldItem: Supplier, newItem: Supplier): Boolean {
                return oldItem == newItem
            }

            override fun areContentsTheSame(oldItem: Supplier, newItem: Supplier): Boolean {
                return oldItem.supId == newItem.supId
            }
        }), BindableListAdapterData<List<Supplier>> {


    override fun onBindViewHolder(
        binding: CatalogSuppItemBinding,
        item: Supplier,
        position: Int
    ) {
        binding.supplier = item
        binding.root.setOnClickListener { binding.supplier?.let(callback) }
    }

    override fun setData(data: Resource<List<Supplier>>) {
        data.case(success = { submitList(it.data) })
    }
}