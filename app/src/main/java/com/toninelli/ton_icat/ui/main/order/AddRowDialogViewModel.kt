package com.toninelli.ton_icat.ui.main.order

import androidx.lifecycle.ViewModel
import androidx.lifecycle.map
import com.toninelli.ton_icat.domain.*
import com.toninelli.ton_icat.model.OrderRow
import javax.inject.Inject

class AddRowDialogViewModel @Inject constructor(
    private val getCatalogItemByCodListUseCase: GetCatalogItemByCodListUseCase,
    private val insertOrderRowUseCase: InsertOrderRowUseCase
): ViewModel(){

    val catalogResourceResult = getCatalogItemByCodListUseCase.resultLiveData

    val insertResult = insertOrderRowUseCase.resultLiveData


    fun loadCatalogItem(cod: Int, customer: String){
        exec(getCatalogItemByCodListUseCase, GetCatalogItemByCodListUseCase.Param(listOf(cod), customer))
    }

    fun insertRow(orderRow: OrderRow){
        exec(insertOrderRowUseCase, InsertOrderRowUseCase.Param(orderRow))
    }

}