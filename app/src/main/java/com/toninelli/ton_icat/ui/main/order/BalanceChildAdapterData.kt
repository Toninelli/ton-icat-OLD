package com.toninelli.ton_icat.ui.main.order

import android.view.Menu
import android.view.MenuItem
import android.view.MotionEvent
import androidx.appcompat.view.ActionMode
import androidx.recyclerview.selection.*
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.toninelli.fileutil.logd
import com.toninelli.ton_icat.R
import com.toninelli.ton_icat.databinding.BalanceChildItemBinding
import com.toninelli.ton_icat.model.Order
import com.toninelli.ton_icat.model.OrderActionType
import com.toninelli.ton_icat.ui.common.BaseAdapter

class BalanceChildAdapterData(
    private val callback: (OrderActionType, List<Order>) -> Unit
) :
    BaseAdapter<Order, BalanceChildItemBinding>(
        R.layout.balance_child_item, object : DiffUtil.ItemCallback<Order>() {
            override fun areItemsTheSame(oldItem: Order, newItem: Order): Boolean {
                return oldItem.orderId == newItem.orderId
            }

            override fun areContentsTheSame(oldItem: Order, newItem: Order): Boolean {
                return oldItem.creationDate == newItem.creationDate
            }

        }) {



    override fun onBindViewHolder(binding: BalanceChildItemBinding, item: Order, position: Int) {
        binding.order = item

        binding.rowOrderHeaderButton.setOnClickListener {
            binding.order?.let { callback(OrderActionType.HEADER, listOf(it)) }
        }

        binding.rowOrderDetailButton.setOnClickListener {
            binding.order?.let { callback(OrderActionType.DETAIL, listOf(it)) }
        }

        binding.rowOrderDeleteButton.setOnClickListener {
            binding.order?.let {
                callback(OrderActionType.DELETE, listOf(it))
            }
        }
    }

    fun setCustomerOrder(orders: List<Order>?) {
        orders?.let {
            submitList(it)
        }
    }

}