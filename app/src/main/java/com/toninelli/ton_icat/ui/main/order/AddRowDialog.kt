package com.toninelli.ton_icat.ui.main.order

import android.app.AlertDialog
import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.view.inputmethod.EditorInfo
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import com.toninelli.fileutil.autoclearedValue
import com.toninelli.fileutil.logd
import com.toninelli.ton_icat.R
import com.toninelli.ton_icat.binding.ResourceListener
import com.toninelli.ton_icat.databinding.DialogAddRowBinding
import com.toninelli.ton_icat.di.Injectable
import com.toninelli.ton_icat.extension.hideKeyboard
import com.toninelli.ton_icat.extension.observe
import com.toninelli.ton_icat.model.Order
import com.toninelli.ton_icat.model.OrderRow
import com.toninelli.ton_icat.ui.common.ViewModelFactory
import com.toninelli.ton_icat.ui.main.catalog.CatalogPackageItemAdapter
import com.toninelli.ton_icat.vo.BooleanResult
import com.toninelli.ton_icat.vo.Resource
import com.toninelli.ton_icat.vo.SingleEventWrapper
import com.toninelli.ton_icat.vo.case
import kotlinx.android.synthetic.main.catalog_item_offer.view.*
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import java.util.*
import javax.inject.Inject

class AddRowDialog(val order: Order) : DialogFragment(), Injectable, ResourceListener {

    @Inject
    lateinit var factory: ViewModelFactory

    var binding by autoclearedValue<DialogAddRowBinding>()
    val model: AddRowDialogViewModel by viewModels { factory }
    private var packageAdapter by autoclearedValue<AddRowPackageItemAdapter>()

    private var orderRow: OrderRow? = null
        set(value) {
            field = value
            binding.orderRow = field
            packageAdapter.submitList(field?.catalogItem?.article?.packaging)
        }


    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return activity?.let {
            val builder = AlertDialog.Builder(it)

            val inflater = requireActivity().layoutInflater

            binding = DialogAddRowBinding.inflate(inflater)

            binding.model = model

            binding.lifecycleOwner = this

            binding.listener = this

            builder.setView(binding.root)
            builder.setTitle(getString(R.string.dialog_add_order_row_title))
            builder.setMessage(getString(R.string.dialog_add_order_row_message, order.orderId))

            builder.create()

        } ?: throw IllegalStateException("Activity must not be null")
    }

    override fun onStart() {
        super.onStart()

        binding.addRowCodArtEditTex.setOnEditorActionListener { v, actionId, event ->
            return@setOnEditorActionListener when (actionId) {
                EditorInfo.IME_ACTION_DONE -> {

                    model.loadCatalogItem(v.text.toString().toInt(), order.customerId)

                    activity?.hideKeyboard(v)

                    true
                }
                else -> false
            }
        }

        setViewForOffer()
        setViewForQuantity()
        setViewForPackage()

        binding.addRowItemButton.catalogButtonAdd.setOnClickListener {
            orderRow?.let {
                it.orderId = order.orderId
                it.customerId = order.customerId
                it.addition = Date().time
                model.insertRow(it)

            }
        }

    }

    private fun setViewForPackage() {
        packageAdapter = AddRowPackageItemAdapter {
            orderRow?.selectedPackage = it
            orderRow?.multQta = it.qta
            binding.addRowItemSelection.catalogQuantity.multQta = it.qta
        }

        binding.addRowItemSelection.catalogPackageList.adapter = packageAdapter

    }

    private fun setViewForOffer() {
        binding.addRowItemOffer.catalogOfferRadioGroup.setOnCheckedChangeListener { group, checkedId ->

            orderRow?.let {

                when (checkedId) {
                    binding.addRowItemOffer.catalogOfferRadioGroup.catalog_offer_radio_group_yes.id -> {
                        it.catalogItem.acceptedOffer = true
                        binding.addRowItemSelection.catalogQuantity.price =
                            it.catalogItem.offerPrice
                    }

                    binding.addRowItemOffer.catalogOfferRadioGroup.catalog_offer_radio_group_no.id -> {
                        it.catalogItem.acceptedOffer = false
                        binding.addRowItemSelection.catalogQuantity.price =
                            it.catalogItem.discountedPrice?.let { it }
                                ?: it.catalogItem.article.price
                    }
                }
            }

        }

    }

    private fun setViewForQuantity() {
        binding.addRowItemSelection.catalogQuantity.setOnQuantityChangeListener { qta, multQta, _ ->
            orderRow?.orderQta = qta
            orderRow?.multQta = multQta
        }
    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        observe(model.catalogResourceResult) {
            it.case(success = {
                orderRow = OrderRow(catalogItem = it.data?.get(0)!!)
            })
        }
    }

    override fun <T> onResourceSuccess(resource: Resource<T>) {
        val booleanResult = resource.data as SingleEventWrapper<BooleanResult>
        if(booleanResult.getContent()?.value == true)
            dismiss()
    }

}
