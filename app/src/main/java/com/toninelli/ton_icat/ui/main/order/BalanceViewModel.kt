package com.toninelli.ton_icat.ui.main.order

import androidx.lifecycle.ViewModel
import androidx.lifecycle.map
import com.toninelli.fileutil.logd
import com.toninelli.ton_icat.domain.*
import javax.inject.Inject

class BalanceViewModel @Inject constructor(
   private val getAllCustomersDoingOrder: GetAllCustomerDoingOrder,
   private val getAllOrdersUseCase: GetAllOrdersUseCase,
   private val deleteOrderUseCase: DeleteOrderUseCase

): ViewModel() {

    init {
        exec(getAllOrdersUseCase, null)
        exec(getAllCustomersDoingOrder,null)
    }


    val allCustomerResult = getAllCustomersDoingOrder.resultLiveData

    val allOrdersResult = getAllOrdersUseCase.resultLiveData

    fun deleteOrder(orderId: Long){
        exec(deleteOrderUseCase,DeleteOrderUseCase.Param(orderId))
    }

}
