package com.toninelli.ton_icat.ui.main.catalog


import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.*
import androidx.core.view.GravityCompat
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.mancj.materialsearchbar.MaterialSearchBar
import com.toninelli.fileutil.autoclearedValue
import com.toninelli.fileutil.logd
import com.toninelli.ton_icat.NavGraphDirections
import com.toninelli.ton_icat.R
import com.toninelli.ton_icat.databinding.FragmentCatalogBinding

import com.toninelli.ton_icat.di.Injectable
import com.toninelli.ton_icat.extension.observe
import com.toninelli.ton_icat.icatcontext.RunTimeValue
import com.toninelli.ton_icat.ui.BaseFragment
import com.toninelli.ton_icat.ui.common.icatoption.IcatOptionDialog
import kotlinx.android.synthetic.main.fragment_catalog.*
import java.util.*


class CatalogFragment : BaseFragment(), Injectable, MaterialSearchBar.OnSearchActionListener {

    private var binding by autoclearedValue<FragmentCatalogBinding>()
    private val viewModel: CatalogViewModel by viewModels { factory }
    private var categAdapter by autoclearedValue<CategAdapterData>()
    private var suppAdapter by autoclearedValue<SupplierAdapterData>()
    private var selectedCategAdapter by autoclearedValue<SelectedCategAdapter>()
    private var catalogItemAdapter by autoclearedValue<CatalogItemAdapterData>()
    private var orderAdapter by autoclearedValue<CatalogOrderAdapterData>()
    lateinit var option: IcatOptionDialog
    private lateinit var orderBottomSheet: BottomSheetBehavior<View>
    private val codListArgs by navArgs<CatalogFragmentArgs>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentCatalogBinding.inflate(inflater, container, false)
        setUpView()
        setUpAdapters()
        setUpOrderBottomSheet()
        return binding.root
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        codListArgs.items?.toList()?.let {
            viewModel.loadItemsByCodList(it)
        }

    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        RunTimeValue.activeOrder.removeObservers(this)

        observe(RunTimeValue.activeOrder) {
            orderAdapter.activeOrderId = it?.orderId
            catalogItemAdapter.makeOrderEnabled = it != null
        }
    }

    private fun setUpView() {
        binding.lifecycleOwner = this
        binding.model = viewModel
        binding.catalogDrawer.setScrimColor(activity.getColor(android.R.color.transparent))
        binding.searchBar.setOnSearchActionListener(this)
        binding.searchBar.setCardViewElevation(50)
        binding.searchBar.addTextChangeListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {}
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if ( s.toString().length > 2) {
                    viewModel.query = s.toString()
                }
            }
        })

        option =
            IcatOptionDialog.Builder(this).showPriceFilter(true).showVisibilityOption(false).build()

        binding.layoutCatalogDrawer.optionBtn.setOnClickListener {
            catalog_drawer.closeDrawer(GravityCompat.END)
            option.show()
        }
    }

    private fun setUpAdapters() {
        selectedCategAdapter =
            SelectedCategAdapter {
                categAdapter.loadSubCateg(it.parent)
                viewModel.loadCatalogItemByCateg(it.parent)
            }

        categAdapter = CategAdapterData {
            selectedCategAdapter.addSelectedCateg(it)
            viewModel.loadCatalogItemByCateg(it.id)
        }

        suppAdapter = SupplierAdapterData {
            viewModel.loadCatalogItemBySupplier(it.supId)
        }

        catalogItemAdapter = CatalogItemAdapterData(
            {
                it.customerId = viewModel.selectedCustomerResult.value?.data?.customerId
                it.orderId = orderAdapter.activeOrderId
                it.addition = Date().time
                viewModel.insertOrderRow(it)

            },
            {
                val dialog = CatalogItemDetailDialog(
                    it.article.artCod,
                    viewModel.selectedCustomerResult.value?.data?.customerId
                )
                dialog.show(activity.supportFragmentManager, "detail")
            })

        orderAdapter = CatalogOrderAdapterData(
            {
                RunTimeValue.activeOrder.postValue(it)
            },
            {
                findNavController().navigate(NavGraphDirections.actionGlobalOrderDetailFragment(it.orderId))
            })

        binding.catalogIcatItemList.addOnScrollListener(object : RecyclerView.OnScrollListener() {

            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                val layoutManager = recyclerView.layoutManager as LinearLayoutManager
                val lastPosition = layoutManager.findLastVisibleItemPosition()
                if (lastPosition != -1 && lastPosition == catalogItemAdapter.itemCount - 1) {
                    viewModel.nextPageHandler.loadNextPage(catalogItemAdapter.itemCount)
                }
            }
        })

        binding.catalogIcatItemList.adapter = catalogItemAdapter
        binding.layoutCatalogDrawer.categList.adapter = categAdapter
        binding.layoutCatalogDrawer.supplierList.adapter = suppAdapter
        binding.layoutCatalogDrawer.selectedCategList.adapter = selectedCategAdapter
        binding.catalogOrderList.adapter = orderAdapter
    }


    private fun setUpOrderBottomSheet() {
        orderBottomSheet = BottomSheetBehavior.from(binding.catalogOrderBottomSheet)
        orderBottomSheet.isHideable = true
        orderBottomSheet.state = BottomSheetBehavior.STATE_HIDDEN

    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_catalog, menu)
        return super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.catalog_filter_icon -> {
                binding.searchBar.disableSearch()
                if (catalog_drawer.isDrawerOpen(GravityCompat.END))
                    catalog_drawer.closeDrawer(GravityCompat.END)
                else {
                    catalog_drawer.openDrawer(GravityCompat.END)
                }
                true
            }
            R.id.catalog_order_icon -> {
                orderBottomSheet.state = BottomSheetBehavior.STATE_COLLAPSED
                true
            }

            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onButtonClicked(buttonCode: Int) {
        /* nothing */
    }

    override fun onSearchStateChanged(enabled: Boolean) {
        /* nothing */
    }

    override fun onSearchConfirmed(text: CharSequence?) {
        viewModel.loadCatalogItemByQuery(text.toString())
        binding.searchBar.clearFocus()
    }


}
