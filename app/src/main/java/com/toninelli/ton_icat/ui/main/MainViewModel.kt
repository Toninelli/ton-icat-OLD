package com.toninelli.ton_icat.ui.main

import androidx.lifecycle.ViewModel
import androidx.lifecycle.map
import androidx.lifecycle.viewModelScope
import com.toninelli.ton_icat.domain.GetAgentUseCase
import com.toninelli.ton_icat.domain.GetCustomerByIdUseCase
import com.toninelli.ton_icat.domain.GetSelectedCustomerUseCase
import com.toninelli.ton_icat.domain.exec
import javax.inject.Inject

class MainViewModel @Inject constructor(
    private val getSelectedCustomerUseCase: GetSelectedCustomerUseCase,
    private val getAgentUseCase: GetAgentUseCase
): ViewModel() {

    init {
        exec(getSelectedCustomerUseCase,null)
        exec(getAgentUseCase)
    }

    val agentResult = getAgentUseCase.resultLiveData.map {
        it?.data
    }

    val selectedCustomerResult = getSelectedCustomerUseCase.resultLiveData


}