package com.toninelli.ton_icat.ui.main.customer

import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.DiffUtil
import com.toninelli.ton_icat.R
import com.toninelli.ton_icat.binding.BindableListAdapterData
import com.toninelli.ton_icat.databinding.CustomerHistoryItemBinding
import com.toninelli.ton_icat.model.CustomerHistory
import com.toninelli.ton_icat.ui.common.BaseAdapter
import com.toninelli.ton_icat.vo.Resource
import com.toninelli.ton_icat.vo.case

class CustomerHistoryAdapter(val activity: AppCompatActivity, val callback: (Int)-> Unit) : BaseAdapter<CustomerHistory, CustomerHistoryItemBinding>(R.layout.customer_history_item, object : DiffUtil.ItemCallback<CustomerHistory>(){
    override fun areItemsTheSame(oldItem: CustomerHistory, newItem: CustomerHistory): Boolean {
        return oldItem == newItem
    }

    override fun areContentsTheSame(oldItem: CustomerHistory, newItem: CustomerHistory): Boolean {
        return oldItem.date == newItem.date
    }
}), BindableListAdapterData<List<CustomerHistory>> {

    override fun onBindViewHolder(
        binding: CustomerHistoryItemBinding,
        item: CustomerHistory,
        position: Int
    ) {
        binding.history = item

        binding.root.setOnClickListener {
            binding.history?.let {
                CustomerHistoryRowDialog(it, callback).show(activity.supportFragmentManager, "History Row Dialog")
            }
        }
    }

    override fun setData(data: Resource<List<CustomerHistory>>) {
        data.case(success = {submitList(it.data)})
    }
}