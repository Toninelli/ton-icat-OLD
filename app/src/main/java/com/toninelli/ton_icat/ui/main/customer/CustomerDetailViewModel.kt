package com.toninelli.ton_icat.ui.main.customer

import androidx.lifecycle.ViewModel
import androidx.lifecycle.map
import com.toninelli.ton_icat.domain.*
import com.toninelli.ton_icat.model.Customer
import com.toninelli.ton_icat.model.Order
import javax.inject.Inject

class CustomerDetailViewModel @Inject constructor(
    private val selectCustomerUseCase: SelectCustomerUseCase,
    private val getSelectedCustomerUseCase: GetSelectedCustomerUseCase,
    private val getCustomerFlowByIdUseCase: GetCustomerFlowByIdUseCase,
    private val getOrderByCustomerUseCase: GetOrderByCustomerUseCase,
    private val insertOrderUseCase: InsertOrderUseCase,
    private val updatePreferredAddressUseCase: UpdatePreferredAddressUseCase
) : ViewModel() {

    init {
        exec(getSelectedCustomerUseCase,null)
    }

    val selectedCustomerResult = getSelectedCustomerUseCase.resultLiveData.map {
        it.data?.customerId
    }

    val customerResult = getCustomerFlowByIdUseCase.resultLiveData.map {
        it.data
    }

    val orderNotCompleted = getOrderByCustomerUseCase.resultLiveData.map {
        it?.data?.let { it.filter { it.state == Order.State.NOT_COMPLETED }.size } ?: 0
    }

    val orderTransmitted = getOrderByCustomerUseCase.resultLiveData.map {
        it?.data?.let { it.filter { it.state == Order.State.TRANSMITTED }.size } ?: 0
    }

    val insertResult = insertOrderUseCase.resultLiveData

    fun selectCustomer(select: Boolean) {
        customerResult.value?.let {
            if (selectedCustomerResult.value != it.customerId || !select)
                exec(selectCustomerUseCase,SelectCustomerUseCase.Param(it.customerId, select))
        }
    }

    fun getCustomer(id: String) {
        exec(getCustomerFlowByIdUseCase,GetCustomerByIdUseCase.Param(id))
        exec(getOrderByCustomerUseCase,GetOrderByCustomerUseCase.Param(id))
    }

    fun insertOrder(){
        exec(insertOrderUseCase,InsertOrderUseCase.Param(customerResult.value?.customerId!!))
    }

    fun updatePreferredAddress(address: Customer.Address){
        customerResult.value?.let {
            exec(updatePreferredAddressUseCase, UpdatePreferredAddressUseCase.Param(it.customerId, address))
        }
    }


}