package com.toninelli.ton_icat.ui.main.home

import android.os.Bundle

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView

import com.toninelli.fileutil.autoclearedValue
import com.toninelli.fileutil.logd
import com.toninelli.ton_icat.NavGraphDirections
import com.toninelli.ton_icat.R
import com.toninelli.ton_icat.databinding.FragmentHomeBinding
import com.toninelli.ton_icat.di.Injectable
import com.toninelli.ton_icat.ui.BaseFragment

class HomeFragment : BaseFragment(), Injectable {

    private var binding by autoclearedValue<FragmentHomeBinding>()
    private var offerAdapter by autoclearedValue<HomeOfferAdapterData>()

    private val homeViewModel: HomeViewModel by viewModels { factory }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_home, container, false)
        offerAdapter = HomeOfferAdapterData(
            {
                val list = it.articles.map { it.artId }.toIntArray()
                findNavController().navigate(NavGraphDirections.actionGlobalCatalogFragment(list))
            },
            {
                val dialog = PdfOfferDialog(it)
                dialog.show(activity.supportFragmentManager, "Pdf Dialog")
            }
        )



        binding.homeListOffer.layoutManager =
            GridLayoutManager(activity, 2, RecyclerView.VERTICAL, false)
        binding.homeListOffer.adapter = offerAdapter

        binding.lifecycleOwner = this
        binding.model = homeViewModel


        return binding.root
    }


}
