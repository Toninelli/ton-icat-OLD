package com.toninelli.ton_icat.ui.main.customer


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.android.material.snackbar.Snackbar
import com.toninelli.fileutil.autoclearedValue
import com.toninelli.fileutil.logd
import com.toninelli.ton_icat.R
import com.toninelli.ton_icat.databinding.FragmentCustomerDetailBinding
import com.toninelli.ton_icat.di.Injectable
import com.toninelli.ton_icat.extension.string
import com.toninelli.ton_icat.ui.BaseFragment


class CustomerDetailFragment : BaseFragment(), Injectable {

    var binding by autoclearedValue<FragmentCustomerDetailBinding>()
    val model: CustomerDetailViewModel by viewModels { factory }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentCustomerDetailBinding.inflate(inflater, container, false)

        binding.lifecycleOwner = this
        binding.customerDetailOrder.lifecycleOwner = this

        binding.customerDetailToggleGroup.addOnButtonCheckedListener { group, checkedId, isChecked ->

            model.selectCustomer(isChecked)
        }

        val customer = navArgs<CustomerDetailFragmentArgs>().value.customer

        model.getCustomer(customer)

        binding.customerDetailOrder.customerDetailAddOrderButton.setOnClickListener {
            model.insertOrder()
        }

        binding.customerDetailButton.customerDetailHistoryButton.setOnClickListener {
            findNavController().navigate(CustomerDetailFragmentDirections.actionCustomerDetailFragmentToCustomerHistoryFragment(model.customerResult.value?.customerId!!))
        }

        binding.customerDetailButton.customerDetailAddressButton.setOnClickListener {
            model.customerResult.value?.let {

                var checkedItem = -1
                val preferredAddress = it.preferredAddress
                val list = it.address
                if(list.contains(preferredAddress))
                    checkedItem = list.indexOf(preferredAddress)

                MaterialAlertDialogBuilder(activity)
                    .setTitle(R.string.customer_detail_address_dialog_title)
                    .setSingleChoiceItems(list.map { it.string() }.toTypedArray(), checkedItem){dialog, which ->
                        model.updatePreferredAddress(list[which])
                    }
                    .show()
            }
        }

        binding.model = model

        return binding.root
    }

}
