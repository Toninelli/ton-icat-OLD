package com.toninelli.ton_icat.ui.common

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView

abstract class BasePagedAdapter<Item, Binding>(
    private val layoutId: Int,
    private val diffCallback: DiffUtil.ItemCallback<Item>
) : PagedListAdapter<Item, BasePagedAdapter.PagedBaseViewHolder<Binding>>(diffCallback) where Binding : ViewDataBinding {

    protected lateinit var binding: Binding

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PagedBaseViewHolder<Binding> {
        binding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            layoutId,
            parent,
            false
        )

        onCreateViewHolder(binding, viewType)

        return PagedBaseViewHolder(
            binding
        )
    }

    override fun onBindViewHolder(holder: PagedBaseViewHolder<Binding>, position: Int) {
        onBindViewHolder(holder.binding,getItem(position),position)
    }

    open fun onCreateViewHolder(binding: Binding, viewType: Int) {}

    abstract fun onBindViewHolder(binding: Binding, item: Item?, position: Int)

    class PagedBaseViewHolder<out T : ViewDataBinding> constructor(val binding: T) :
        RecyclerView.ViewHolder(binding.root)

}