package com.toninelli.ton_icat.ui.common.icatoption

import androidx.lifecycle.ViewModel
import com.toninelli.ton_icat.domain.SetVisibilityOptionUseCase
import com.toninelli.ton_icat.domain.exec
import com.toninelli.ton_icat.model.VisibilityOption
import javax.inject.Inject

class IcatOptionDialogViewModel @Inject constructor(
    private val setVisibilityOptionUseCase: SetVisibilityOptionUseCase
    ) : ViewModel(){

    val optionResult = setVisibilityOptionUseCase.resultLiveData

    init {
        exec(setVisibilityOptionUseCase, null)
    }

    fun setIcatOption(option: VisibilityOption){
        val param = SetVisibilityOptionUseCase.Param(option)
        exec(setVisibilityOptionUseCase, param)
    }

}