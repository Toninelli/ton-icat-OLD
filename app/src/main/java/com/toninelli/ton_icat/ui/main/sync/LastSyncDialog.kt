package com.toninelli.ton_icat.ui.main.sync

import android.app.Dialog
import android.os.Bundle
import android.view.*
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.viewModels
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.toninelli.fileutil.logd

import com.toninelli.ton_icat.R
import com.toninelli.ton_icat.databinding.DialogLastSyncLayoutBinding
import com.toninelli.ton_icat.di.Injectable
import com.toninelli.ton_icat.extension.differenceDaysFromToday
import com.toninelli.ton_icat.extension.format
import com.toninelli.ton_icat.extension.observe
import com.toninelli.ton_icat.model.SyncLog
import com.toninelli.ton_icat.model.SyncStatus
import com.toninelli.ton_icat.ui.common.ViewModelFactory
import com.toninelli.ton_icat.vo.case
import javax.inject.Inject


class LastSyncDialog : DialogFragment(), Injectable {

    @Inject
    lateinit var factory: ViewModelFactory

    lateinit var binding: DialogLastSyncLayoutBinding

    private val lastSyncViewModel: LastSyncViewModel by viewModels { factory }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return activity?.let {
            val builder = MaterialAlertDialogBuilder(it)

            binding = DialogLastSyncLayoutBinding.inflate(requireActivity().layoutInflater)

            builder.setView(binding.root)

            builder.create()
        }?: throw IllegalStateException("Activity null")
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        observe(lastSyncViewModel.result) {

            it.case(success = {
                it.data?.let {
                    fillDialog(it)
                } ?: dismiss()
            },
                error = { dismiss()})
        }

    }

    private fun fillDialog(data: SyncLog) {
        when (data.status) {
            SyncStatus.NOT_COMPLETED -> {
                binding.syncIcon.setImageDrawable(resources.getDrawable(R.drawable.error))
                binding.syncTitle.text = getString(R.string.last_sync_dialog_title_error)
                binding.syncMsg.text = getString(R.string.last_sync_dialog_message_error)
                binding.syncAction.text = getString(R.string.last_sync_dialog_actions_error)
                binding.root.background = resources.getDrawable(R.drawable.background_dialog_error)
            }
            SyncStatus.COMPLETED -> {
                if (data.lastSync.differenceDaysFromToday() >= 1) {
                    binding.syncIcon.setImageDrawable(resources.getDrawable(R.drawable.warning))
                    binding.syncTitle.text = getString(R.string.last_sync_dialog_title_warning)
                    binding.syncMsg.text = getString(R.string.last_sync_dialog_message_warning)
                    binding.syncAction.text = getString(R.string.last_sync_dialog_actions_warning)
                    binding.root.background =
                        resources.getDrawable(R.drawable.background_dialog_warning)
                } else {
                    binding.syncIcon.setImageDrawable(resources.getDrawable(R.drawable.ok))
                    binding.syncTitle.text = getString(R.string.last_sync_dialog_title_ok)
                    binding.syncMsg.text =
                        getString(R.string.last_sync_dialog_message_ok, data.lastSync.format())
//                    binding.syncAction.text = getString(R.string.last_sync_dialog_actions_ok)
                    binding.root.background = resources.getDrawable(R.drawable.background_dialog_ok)
                }
            }
        }
    }

}