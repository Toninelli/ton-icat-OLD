package com.toninelli.ton_icat.ui.main.sync

import androidx.lifecycle.ViewModel
import androidx.lifecycle.map
import androidx.lifecycle.viewModelScope


import com.toninelli.ton_icat.domain.ExeSyncUseCase
import com.toninelli.ton_icat.domain.exec
import javax.inject.Inject

class SyncProgressViewModel @Inject constructor(private val syncUseCase: ExeSyncUseCase) :
    ViewModel() {

    val result = syncUseCase.resultLiveData

    val dumpStatus = result.map {
        it?.data?.status
    }

    val syncProgress = result.map { it?.data }

    fun syncCatalog() {
        exec(syncUseCase,null)
    }

}