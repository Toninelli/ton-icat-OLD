package com.toninelli.ton_icat.ui.main.catalog

import androidx.recyclerview.widget.DiffUtil
import com.toninelli.fileutil.logd
import com.toninelli.ton_icat.R
import com.toninelli.ton_icat.binding.BindableListAdapterData
import com.toninelli.ton_icat.binding.BindableListAdapterInformation
import com.toninelli.ton_icat.databinding.CatalogItemBinding
import com.toninelli.ton_icat.model.CatalogItem
import com.toninelli.ton_icat.model.OrderRow
import com.toninelli.ton_icat.ui.common.BaseAdapter
import com.toninelli.ton_icat.vo.Resource
import com.toninelli.ton_icat.vo.case
import kotlinx.android.synthetic.main.catalog_item_offer.view.*
import java.util.*

class CatalogItemAdapterData(
    val onInsertOrder: (OrderRow) -> Unit,
    val callback: (CatalogItem) -> Unit
) :
    BaseAdapter<CatalogItem, CatalogItemBinding>(
        R.layout.catalog_item,
        object : DiffUtil.ItemCallback<CatalogItem>() {

            override fun areItemsTheSame(oldItem: CatalogItem, newItem: CatalogItem): Boolean {
                return oldItem.article.artCod == newItem.article.artCod
            }

            override fun areContentsTheSame(oldItem: CatalogItem, newItem: CatalogItem): Boolean {
                return oldItem.discountedPrice == newItem.discountedPrice
            }
        }), BindableListAdapterData<List<CatalogItem>>, BindableListAdapterInformation<Array<Int>> {

    var makeOrderEnabled: Boolean? = null
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    private var rangeValue: Array<Int>? = null

    override fun onBindViewHolder(
        binding: CatalogItemBinding,
        item: CatalogItem,
        position: Int
    ) {
        val orderRow = OrderRow(catalogItem = item)
        binding.art = item
        binding.position = position.toString()
        binding.makeOrderEnabled = makeOrderEnabled

        binding.root.setOnClickListener {
            binding.art?.let{
                callback(it)
                logd(it.article.category)
            }
        }

        binding.catalogItemButton.catalogButtonAdd.setOnClickListener {
            binding.art?.let {
                onInsertOrder(orderRow)
            }
        }

        setViewForOffer(binding, item, orderRow)
        setViewForPackageSelection(binding, item, orderRow)
        setViewForQuantity(binding, item, orderRow)

    }

    private fun setViewForQuantity(
        binding: CatalogItemBinding,
        item: CatalogItem,
        orderRow: OrderRow
    ) {

        binding.catalogItemSelection.row = orderRow
        binding.catalogItemSelection.catalogQuantity.setOnQuantityChangeListener { qta, multQta, _ ->
            orderRow.orderQta = qta
            orderRow.multQta = multQta
        }
    }

    private fun setViewForPackageSelection(
        binding: CatalogItemBinding,
        item: CatalogItem,
        orderRow: OrderRow
    ) {
        val packAdapter = CatalogPackageItemAdapter {
            orderRow.selectedPackage = it
            orderRow.multQta = it.qta
            binding.catalogItemSelection.catalogQuantity.multQta = it.qta
        }
        binding.catalogItemSelection.catalogPackageList.adapter = packAdapter
        packAdapter.submitList(item.article.packaging)
        packAdapter.selectedPackage = orderRow.selectedPackage
    }

    private fun setViewForOffer(
        binding: CatalogItemBinding,
        item: CatalogItem,
        orderRow: OrderRow
    ) {
        item.offerName?.let {
            binding.catalogItemOffer.catalogOfferRadioGroup.setOnCheckedChangeListener { group, checkedId ->
                when (checkedId) {
                    binding.catalogItemOffer.catalogOfferRadioGroup.catalog_offer_radio_group_yes.id -> {
                        item.acceptedOffer = true
                        binding.catalogItemSelection.catalogQuantity.price = item.offerPrice
                    }

                    binding.catalogItemOffer.catalogOfferRadioGroup.catalog_offer_radio_group_no.id -> {
                        item.acceptedOffer = false
                        binding.catalogItemSelection.catalogQuantity.price =
                            item.discountedPrice?.let { it } ?: item.article.price
                    }
                }
            }

            when (item.acceptedOffer) {
                true -> {
                    binding.catalogItemOffer.catalogOfferRadioGroup.check(binding.catalogItemOffer.catalogOfferRadioGroup.catalog_offer_radio_group_yes.id)
                }
                false -> {
                    binding.catalogItemOffer.catalogOfferRadioGroup.check(binding.catalogItemOffer.catalogOfferRadioGroup.catalog_offer_radio_group_no.id)
                }
            }
        }
    }

    private var lastDataResult: List<CatalogItem>? = null

    override fun setData(data: Resource<List<CatalogItem>>) {
        data.case(success = {
            lastDataResult = data.data
            logd(lastDataResult?.size)
            rangeValue?.let { range ->
                filterList(range)
            } ?: submitList(lastDataResult)

        })
    }

    override fun setInformation(info: Array<Int>) {
        this.rangeValue = info
        filterList(info)
    }

    private fun filterList(range: Array<Int>) {
        val filterList =
            lastDataResult?.filter { it.article.price >= range[0] && it.article.price <= range[1] }
        submitList(filterList)
    }


}