package com.toninelli.ton_icat.ui.main.catalog

import androidx.recyclerview.widget.DiffUtil
import com.toninelli.ton_icat.R
import com.toninelli.ton_icat.binding.BindableListAdapterData
import com.toninelli.ton_icat.databinding.CatalogItemDetailItemItemBinding
import com.toninelli.ton_icat.model.Article
import com.toninelli.ton_icat.model.CatalogItem
import com.toninelli.ton_icat.ui.common.BaseAdapter
import com.toninelli.ton_icat.vo.Resource
import com.toninelli.ton_icat.vo.case


class CatalogItemDetailItemAdapter : BaseAdapter<Article.Item, CatalogItemDetailItemItemBinding>(
    R.layout.catalog_item_detail_item_item,
    object : DiffUtil.ItemCallback<Article.Item>() {
        override fun areItemsTheSame(oldItem: Article.Item, newItem: Article.Item): Boolean {
            return oldItem == newItem
        }

        override fun areContentsTheSame(oldItem: Article.Item, newItem: Article.Item): Boolean {
            return oldItem.name == newItem.name
        }

    }), BindableListAdapterData<List<CatalogItem>>{
    override fun onBindViewHolder(
        binding: CatalogItemDetailItemItemBinding,
        item: Article.Item,
        position: Int
    ) {
        binding.item = item
    }

    override fun setData(data: Resource<List<CatalogItem>>) {
        data.case(success = {
            it.data?.get(0)?.article?.item?.let {
                submitList(it)
            }
        })
    }

}