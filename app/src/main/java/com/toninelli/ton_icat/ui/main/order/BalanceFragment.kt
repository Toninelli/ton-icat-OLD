package com.toninelli.ton_icat.ui.main.order


import android.os.Bundle
import android.view.*
import androidx.appcompat.view.ActionMode
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.toninelli.fileutil.autoclearedValue
import com.toninelli.fileutil.logd
import com.toninelli.ton_icat.NavGraphDirections
import com.toninelli.ton_icat.R
import com.toninelli.ton_icat.databinding.FragmentBalanceBinding
import com.toninelli.ton_icat.di.Injectable
import com.toninelli.ton_icat.extension.waitForLayout
import com.toninelli.ton_icat.model.OrderActionType
import com.toninelli.ton_icat.ui.BaseFragment

class BalanceFragment : BaseFragment(), Injectable{

    var binding by autoclearedValue<FragmentBalanceBinding>()
    val model: BalanceViewModel by viewModels { factory }
    var adapter by autoclearedValue<BalanceAdapterData>()


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentBalanceBinding.inflate(inflater, container, false)

        binding.lifecycleOwner = this

        binding.model = model

        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        adapter = BalanceAdapterData(model, binding.balanceList) { orderActionType, orders ->
            when (orderActionType) {
                OrderActionType.DETAIL -> {
                    findNavController().navigate(NavGraphDirections.actionGlobalOrderDetailFragment(orders[0].orderId))
                }
                OrderActionType.HEADER -> {
                    findNavController().navigate(NavGraphDirections.actionGlobalOrderHeaderFragment(orders[0].orderId))
                }
                OrderActionType.DELETE -> {
                    if (orders.size > 1) {

                        orders.forEach { model.deleteOrder(it.orderId) }

                    } else {
                        MaterialAlertDialogBuilder(activity)
                            .setTitle(R.string.order_delete_action)
                            .setPositiveButton(R.string.button_yes) { _, _ ->
                                model.deleteOrder(
                                    orders[0].orderId
                                )
                            }
                            .setNegativeButton(R.string.button_no) { dialog, _ -> dialog.dismiss() }
                            .show()
                    }
                }

            }
        }

        binding.balanceList.adapter = adapter

        adapter.initSelectionAdapter(this)

    }




}
