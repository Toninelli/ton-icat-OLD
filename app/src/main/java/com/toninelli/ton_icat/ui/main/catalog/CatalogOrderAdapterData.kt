package com.toninelli.ton_icat.ui.main.catalog


import androidx.recyclerview.widget.DiffUtil
import com.toninelli.fileutil.logd
import com.toninelli.ton_icat.R
import com.toninelli.ton_icat.binding.BindableListAdapterData
import com.toninelli.ton_icat.binding.onResourceListener
import com.toninelli.ton_icat.databinding.CatalogOrderItemBinding
import com.toninelli.ton_icat.model.Order
import com.toninelli.ton_icat.ui.common.BaseAdapter
import com.toninelli.ton_icat.vo.Resource
import com.toninelli.ton_icat.vo.case

class CatalogOrderAdapterData(
    private val callback: (Order?) -> Unit,
    private val onLongClickCallback: (Order) -> Unit
) :
    BaseAdapter<Order, CatalogOrderItemBinding>(
        R.layout.catalog_order_item, object : DiffUtil.ItemCallback<Order>() {
            override fun areItemsTheSame(oldItem: Order, newItem: Order): Boolean {
                return oldItem == newItem
            }

            override fun areContentsTheSame(oldItem: Order, newItem: Order): Boolean {
                return oldItem.orderId == newItem.orderId
            }

        }), BindableListAdapterData<List<Order>> {

    private var selectedOrderPosition: Pair<CatalogOrderItemBinding?, Int> = null to -1

    var activeOrderId: Long? = null
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun onBindViewHolder(binding: CatalogOrderItemBinding, item: Order, position: Int) {
        binding.order = item
        binding.catalogOrderItemButton.setOnClickListener {

            if (selectedOrderPosition.second != position) {

                selectedOrderPosition.first?.let {
                    it.catalogOrderItemButton.isChecked = false
                }

                selectedOrderPosition = binding to position
                callback(item)
            } else {

                selectedOrderPosition = binding to -1
                callback(null)
            }

        }

        binding.catalogOrderItemButton.setOnLongClickListener {
            binding.order?.let(onLongClickCallback)
            true
        }

        activeOrderId?.let {
            binding.catalogOrderItemButton.isChecked = it == item.orderId
        } ?: kotlin.run { binding.catalogOrderItemButton.isChecked = false }

    }

    override fun setData(data: Resource<List<Order>>) {

        data.case(
            success = {
                submitList(it.data)
                if (it.data.isNullOrEmpty())
                    callback(null)
            },
            loading = { submitList(null) },
            error = {callback(null)}
        )
    }

}