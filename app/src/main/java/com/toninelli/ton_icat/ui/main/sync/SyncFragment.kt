package com.toninelli.ton_icat.ui.main.sync


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.viewModels
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.toninelli.fileutil.logd
import com.toninelli.ton_icat.R
import com.toninelli.ton_icat.data.database.DbRepository
import com.toninelli.ton_icat.databinding.FragmentSyncBinding
import com.toninelli.ton_icat.di.Injectable
import com.toninelli.ton_icat.extension.observe
import com.toninelli.ton_icat.model.SelectedCustomer
import com.toninelli.ton_icat.ui.BaseFragment
import com.toninelli.ton_icat.vo.Either
import com.toninelli.ton_icat.vo.Failure
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.onCompletion
import kotlinx.coroutines.flow.onStart
import kotlinx.coroutines.launch
import javax.inject.Inject

class SyncFragment : BaseFragment(), Injectable {


    @Inject
    lateinit var dbRepository: DbRepository

    lateinit var binding: FragmentSyncBinding

    private val syncViewModel: SyncViewModel by viewModels { factory }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_sync, container, false)

        binding.model = syncViewModel
        binding.lifecycleOwner = this

        binding.buttonNo.setOnClickListener {
            findNavController().popBackStack(R.id.homeFragment, false)
        }

        binding.buttonYes.setOnClickListener { showSyncDialog() }

        return binding.root
    }

    private fun showSyncDialog() {

        val newFragment = SyncProgressDialog {
            findNavController().popBackStack(R.id.homeFragment, false)
        }
        newFragment.show(activity.supportFragmentManager, "sync")
    }


}
