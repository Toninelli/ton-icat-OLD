package com.toninelli.ton_icat.ui.main.order

import androidx.recyclerview.widget.DiffUtil
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.toninelli.ton_icat.R
import com.toninelli.ton_icat.binding.BindableListAdapterData
import com.toninelli.ton_icat.binding.BindableListAdapterInformation
import com.toninelli.ton_icat.databinding.OrderDetailRowItemBinding
import com.toninelli.ton_icat.model.Order
import com.toninelli.ton_icat.model.OrderRow
import com.toninelli.ton_icat.model.OrderRowActionType
import com.toninelli.ton_icat.ui.common.BaseAdapter
import com.toninelli.ton_icat.vo.Resource
import com.toninelli.ton_icat.vo.case

class OrderRowAdapterData(private val callback: (OrderRowActionType, OrderRow) -> Unit) :
    BaseAdapter<OrderRow, OrderDetailRowItemBinding>(
        R.layout.order_detail_row_item, object : DiffUtil.ItemCallback<OrderRow>() {
            override fun areItemsTheSame(oldItem: OrderRow, newItem: OrderRow): Boolean {
                return oldItem == newItem
            }

            override fun areContentsTheSame(oldItem: OrderRow, newItem: OrderRow): Boolean {
                return oldItem.orderRowId == newItem.orderRowId
            }

        }), BindableListAdapterData<List<OrderRow>>, BindableListAdapterInformation<Order.State> {

    var state: Order.State? = null

    override fun onBindViewHolder(
        binding: OrderDetailRowItemBinding,
        item: OrderRow,
        position: Int
    ) {
        binding.item = item
        binding.state = state
        binding.orderDetailRowDeleteButton.setOnClickListener {
            MaterialAlertDialogBuilder(binding.root.context)
                .setTitle(R.string.order_row_delete_action)
                .setPositiveButton(R.string.button_yes) { _, _ ->
                    binding.item?.let { callback(OrderRowActionType.DELETE, it) }
                }
                .setNegativeButton(R.string.button_no){ dialog, _ ->
                    dialog.dismiss()
                }
                .show()

        }

        binding.orderDetailRowHeadButton.setOnClickListener {
            binding.item?.let { callback(OrderRowActionType.HEADER, it) }
        }

        binding.orderDetailRowDiscountButton.setOnClickListener {
            binding.item?.let { callback(OrderRowActionType.DISCOUNT, it) }
        }

    }

    override fun setData(data: Resource<List<OrderRow>>) {
        data.case(success = { submitList(it.data) })
    }

    override fun setInformation(info: Order.State) {
        this.state = info
    }


}