package com.toninelli.ton_icat.ui.main.home

import androidx.recyclerview.widget.DiffUtil
import com.toninelli.ton_icat.R
import com.toninelli.ton_icat.binding.BindableListAdapterData
import com.toninelli.ton_icat.databinding.PdfItemBinding
import com.toninelli.ton_icat.model.Pdf
import com.toninelli.ton_icat.ui.common.BaseAdapter
import com.toninelli.ton_icat.vo.Resource
import com.toninelli.ton_icat.vo.case

class PdfOfferAdapterData(val callback: (Pdf) -> Unit) : BaseAdapter<Pdf, PdfItemBinding>(
    R.layout.pdf_item,
    diffCallback = object : DiffUtil.ItemCallback<Pdf>() {
        override fun areItemsTheSame(oldItem: Pdf, newItem: Pdf): Boolean {
            return oldItem == newItem
        }

        override fun areContentsTheSame(oldItem: Pdf, newItem: Pdf): Boolean {
            return oldItem.name == newItem.name
        }
    }), BindableListAdapterData<List<Pdf>> {
    override fun onBindViewHolder(
        binding: PdfItemBinding,
        item: Pdf,
        position: Int
    ) {
        binding.pdf = item
        binding.root.setOnClickListener {
            binding.pdf?.let(callback)
        }
    }

    override fun setData(data: Resource<List<Pdf>>) {
        data.case(success = { submitList(it.data) })
    }
}