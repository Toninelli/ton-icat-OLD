package com.toninelli.ton_icat.ui.main.catalog

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import com.toninelli.fileutil.autoclearedValue
import com.toninelli.ton_icat.R
import com.toninelli.ton_icat.databinding.FragmentImageBinding
import com.toninelli.ton_icat.ui.BaseFragment


class ImageFragment(val name: String) : BaseFragment() {

    var binding by autoclearedValue<FragmentImageBinding>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentImageBinding.inflate(inflater, container, false)

        binding.name = name

        binding.image.setOnClickListener {
            val dialog = PhotoZoomableDialog(name)
            dialog.show(activity.supportFragmentManager, "zoom")
        }

        return binding.root
    }


}