package com.toninelli.ton_icat.ui.main.customer


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.reddit.indicatorfastscroll.FastScrollItemIndicator
import com.toninelli.fileutil.autoclearedValue
import com.toninelli.ton_icat.NavGraphDirections
import com.toninelli.ton_icat.R
import com.toninelli.ton_icat.databinding.FragmentCustomerBinding
import com.toninelli.ton_icat.di.Injectable
import com.toninelli.ton_icat.ui.BaseFragment
import java.util.*

class CustomerFragment : BaseFragment(), Injectable {

    private var binding by autoclearedValue<FragmentCustomerBinding>()
    private val viewModel: CustomerViewModel by viewModels { factory }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_customer, container, false)

        binding.lifecycleOwner = this

        binding.model = viewModel

        setUpList()

        return binding.root
    }

    private fun setUpList() {

        val adapter = CustomerAdapterData {
            findNavController().navigate(NavGraphDirections.actionGlobalCustomerDetailFragment(it.customerId))

        }

        binding.customerList.adapter = adapter

        binding.customerFastscroller.setupWithRecyclerView(binding.customerList, { position ->
            val item = adapter.getCustomer(position)
            FastScrollItemIndicator.Text(item.name.first().toString().toUpperCase(Locale.ROOT))
        })

        binding.customerFastscrollerThumb.setupWithFastScroller(binding.customerFastscroller)

    }

}
