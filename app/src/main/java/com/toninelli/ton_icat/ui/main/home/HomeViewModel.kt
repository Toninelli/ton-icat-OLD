package com.toninelli.ton_icat.ui.main.home

import androidx.lifecycle.ViewModel
import com.toninelli.ton_icat.domain.GetOffersUseCase
import com.toninelli.ton_icat.domain.exec
import javax.inject.Inject

class HomeViewModel @Inject constructor(
    private val offersUseCase: GetOffersUseCase
) : ViewModel() {

    val offersResult = offersUseCase.resultLiveData

    init {
        exec(offersUseCase,null)

    }

}