package com.toninelli.ton_icat.ui.main.catalog


import androidx.recyclerview.widget.DiffUtil
import com.toninelli.ton_icat.R
import com.toninelli.ton_icat.binding.BindableListAdapterData
import com.toninelli.ton_icat.data.database.CategoryTree
import com.toninelli.ton_icat.databinding.CatalogCategItemBinding
import com.toninelli.ton_icat.model.Category
import com.toninelli.ton_icat.ui.common.BaseAdapter
import com.toninelli.ton_icat.vo.Resource
import com.toninelli.ton_icat.vo.case

class CategAdapterData(val callback: (Category) -> Unit) :
    BaseAdapter<Category, CatalogCategItemBinding>(
        R.layout.catalog_categ_item,
        diffCallback = object : DiffUtil.ItemCallback<Category>() {
            override fun areItemsTheSame(oldItem: Category, newItem: Category): Boolean {
                return oldItem == newItem
            }

            override fun areContentsTheSame(oldItem: Category, newItem: Category): Boolean {
                return oldItem.id == newItem.id
            }
        }), BindableListAdapterData<CategoryTree> {

    private var categoryTree: CategoryTree? = null

    override fun onBindViewHolder(
        binding: CatalogCategItemBinding,
        item: Category,
        position: Int
    ) {
        binding.category = item
        binding.root.setOnClickListener {
            binding.category?.let {
                loadSubCateg(it.id)
                callback(it)
            }
        }

    }

    fun loadSubCateg(categId: Int) {
        submitList(categoryTree?.getSubCateg(categId))
    }

    override fun setData(data: Resource<CategoryTree>) {
        data.case(
            success = {
                this.categoryTree = it.data
                submitList(categoryTree?.getSubCateg(0))
            })
    }


}