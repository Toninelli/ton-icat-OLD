package com.toninelli.ton_icat.ui.main.order

import android.graphics.ColorFilter
import android.graphics.PorterDuff
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.MotionEvent
import androidx.appcompat.view.ActionMode
import androidx.recyclerview.selection.*
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.toninelli.fileutil.logd
import com.toninelli.ton_icat.R
import com.toninelli.ton_icat.binding.BindableListAdapterData
import com.toninelli.ton_icat.databinding.BalanceChildItemBinding
import com.toninelli.ton_icat.databinding.OrderItemBinding

import com.toninelli.ton_icat.model.Order
import com.toninelli.ton_icat.model.OrderActionType
import com.toninelli.ton_icat.ui.common.BaseAdapter
import com.toninelli.ton_icat.vo.Resource
import com.toninelli.ton_icat.vo.case

class OrderAdapterData(
    private val recyclerView: RecyclerView,
    private val callback: (OrderActionType, List<Order>) -> Unit
) : BaseAdapter<Order, OrderItemBinding>(
    R.layout.order_item,
    object : DiffUtil.ItemCallback<Order>() {
        override fun areItemsTheSame(oldItem: Order, newItem: Order): Boolean {
            return oldItem.orderId == newItem.orderId
        }

        override fun areContentsTheSame(oldItem: Order, newItem: Order): Boolean {
            return oldItem.creationDate == newItem.creationDate
        }

    }), BindableListAdapterData<List<Order>> {

    var selectionTracker: SelectionTracker<Long>? = null

    var actionMode: ActionMode? = null

    init {
        setHasStableIds(true)
    }

//    fun initSelectionAdapter(fragment: OrderFragment) {
//        selectionTracker = SelectionTracker.Builder<Long>(
//            "card selection",
//            recyclerView,
//            KeyProvider(recyclerView),
//            OrderDetailsLookUp(recyclerView),
//            StorageStrategy.createLongStorage()
//        ).withSelectionPredicate(SelectionPredicates.createSelectAnything()).build()
//
//        selectionTracker?.addObserver(object : SelectionTracker.SelectionObserver<Long>() {
//            override fun onSelectionChanged() {
//
//                if (selectionTracker!!.selection.size() > 0) {
//                    if (actionMode == null) {
//                        actionMode =
//                            fragment.activity.startSupportActionMode(object : ActionMode.Callback {
//                                override fun onActionItemClicked(
//                                    mode: ActionMode?,
//                                    item: MenuItem?
//                                ): Boolean {
//
//                                    selectionTracker?.let { tracker ->
//
//                                        val list = mutableListOf<Order>()
//
//                                        tracker.selection.forEach {
//                                            list.add(currentList[it.toInt()])
//                                        }
//
//                                        callback(OrderActionType.DELETE, list)
//
//                                        tracker.clearSelection()
//                                    }
//                                    return false
//                                }
//
//                                override fun onCreateActionMode(
//                                    mode: ActionMode?,
//                                    menu: Menu?
//                                ): Boolean {
//                                    val inflater = mode?.menuInflater
//                                    inflater?.inflate(R.menu.menu_selected_order, menu)
//                                    return true
//                                }
//
//                                override fun onPrepareActionMode(
//                                    mode: ActionMode?,
//                                    menu: Menu?
//                                ): Boolean {
//                                    return false
//                                }
//
//                                override fun onDestroyActionMode(mode: ActionMode?) {
//                                    selectionTracker?.clearSelection()
//                                    actionMode = null
//                                }
//
//
//                            })
//                    }
//                    actionMode?.title = selectionTracker!!.selection.size().toString()
//                } else if (actionMode != null) {
//                    actionMode?.finish()
//                }
//
//            }
//        })
//
//    }


    override fun onBindViewHolder(binding: OrderItemBinding, item: Order, position: Int) {
        binding.order = item

        binding.rowOrderHeaderButton.setOnClickListener {
            binding.order?.let { callback(OrderActionType.HEADER, listOf(it)) }
        }

        binding.rowOrderDetailButton.setOnClickListener {
            binding.order?.let { callback(OrderActionType.DETAIL, listOf(it)) }
        }

        binding.rowOrderDeleteButton.setOnClickListener {
            binding.order?.let { callback(OrderActionType.DELETE, listOf(it)) }
        }

//        selectionTracker?.let {
//            binding.orderItemCardView.isChecked = it.isSelected(position.toLong())
//        }

    }


    override fun onCreateViewHolder(
        binding: OrderItemBinding,
        viewType: Int
    ): BaseViewHolder<OrderItemBinding> {
        return OrderViewHolder(binding)
    }



//    class OrderDetailsLookUp(private val recyclerView: RecyclerView) :
//        ItemDetailsLookup<Long>() {
//        override fun getItemDetails(event: MotionEvent): ItemDetails<Long>? {
//            val view = recyclerView.findChildViewUnder(event.x, event.y)
//            if (view != null) {
//                return (recyclerView.getChildViewHolder(view) as OrderViewHolder)
//                    .getItemDetails()
//            }
//            return null
//        }
//
//    }
//
//    class KeyProvider(private val recyclerView: RecyclerView) : ItemKeyProvider<Long>(SCOPE_MAPPED){
//        override fun getKey(position: Int): Long? {
//            return recyclerView.adapter?.getItemId(position)
//        }
//
//        override fun getPosition(key: Long): Int {
//            val viewHolder = recyclerView.findViewHolderForItemId(key)
//            return viewHolder?.layoutPosition ?: RecyclerView.NO_POSITION
//        }
//
//    }
//
//
//    override fun getItemId(position: Int): Long {
//        return position.toLong()
//    }


    override fun setData(data: Resource<List<Order>>) {
        data.case(success = {
            submitList(it.data) })
    }

    class OrderViewHolder(binding: OrderItemBinding) :
        BaseViewHolder<OrderItemBinding>(binding) {


//        fun getItemDetails(): ItemDetailsLookup.ItemDetails<Long> =
//            object : ItemDetailsLookup.ItemDetails<Long>() {
//                override fun getPosition(): Int = itemId.toInt()
//                override fun getSelectionKey(): Long? = itemId
//            }

    }


}