package com.toninelli.ton_icat.ui.login

import androidx.lifecycle.ViewModel
import androidx.lifecycle.map
import com.toninelli.fileutil.logd
import com.toninelli.ton_icat.domain.*
import com.toninelli.ton_icat.model.Agent
import com.toninelli.ton_icat.model.Credential
import com.toninelli.ton_icat.vo.Resource
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.coroutineScope
import javax.inject.Inject
import kotlin.coroutines.coroutineContext

class LoginViewModel @Inject constructor(
    private val getCredentialUseCase: GetCredentialUseCase,
    private val loginUseCase: LoginUseCase,
    private val checkUniqueAgentUseCase: CheckUniqueAgentUseCase
): ViewModel() {

    val credential = getCredentialUseCase.resultLiveData.map {
        it?.data
    }

    val loginResult = loginUseCase.resultLiveData

    val checkUniqueAgentResult = checkUniqueAgentUseCase.resultLiveData.map {
        it?.data
    }


    init {
        exec(getCredentialUseCase)

    }

    fun login(){
        credential.value?.let {

            val checkedCredential = it.takeIf { it.username != null && it.password != null }

            checkedCredential?.let { exec(loginUseCase, LoginUseCase.Param(it)) } ?: loginResult.postValue(Resource.Error(Exception("Campi mancanti")))

        }
    }

    fun checkAgent(loginAgent: Agent){
        exec(checkUniqueAgentUseCase, CheckUniqueAgentUseCase.Param(loginAgent))
    }

}