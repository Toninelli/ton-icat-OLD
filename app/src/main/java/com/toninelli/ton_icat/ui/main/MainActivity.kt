package com.toninelli.ton_icat.ui.main

import android.app.ActivityManager
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.Gravity
import android.view.Menu
import android.view.MenuItem
import androidx.activity.viewModels
import androidx.navigation.findNavController
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.lifecycleScope
import androidx.navigation.ui.*
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.toninelli.fileutil.logd
import com.toninelli.ton_icat.NavGraphDirections
import com.toninelli.ton_icat.R
import com.toninelli.ton_icat.databinding.ActivityMainBinding
import com.toninelli.ton_icat.databinding.NavHeaderMainBinding
import com.toninelli.ton_icat.extension.observe
import com.toninelli.ton_icat.extension.showBottomNav
import com.toninelli.ton_icat.ui.common.ViewModelFactory
import com.toninelli.ton_icat.ui.login.LoginActivity
import com.toninelli.ton_icat.ui.main.sync.LastSyncDialog
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasAndroidInjector
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_main.view.*
import kotlinx.android.synthetic.main.app_bar_main.*
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import javax.inject.Inject
import kotlin.math.log

class MainActivity : AppCompatActivity(), HasAndroidInjector {

    @Inject
    lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Any>

    @Inject
    lateinit var factory: ViewModelFactory

    private val model: MainViewModel by viewModels { factory }

    lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)

        setUpView()
    }

    private fun setUpView() {

        val topLevelDestination = setOf(
            R.id.homeFragment,
            R.id.syncFragment,
            R.id.catalogFragment,
            R.id.orderFragment,
            R.id.customerFragment,
            R.id.balanceFragment
        )

        val appBarConfiguration = AppBarConfiguration(topLevelDestination, drawer_layout)
        val navController = findNavController(R.id.nav_host_fragment)

        toolbar.also {
            setSupportActionBar(it)
            it.setupWithNavController(navController, appBarConfiguration)
            supportActionBar?.title = getString(R.string.home)
        }


        nav_view.setupWithNavController(navController)

        navController.addOnDestinationChangedListener { _, destination, _ ->

            when (destination.id) {
                R.id.orderFragment,R.id.balanceFragment -> showBottomNav(true, R.menu.menu_bottom_order)
                else -> showBottomNav(false)
            }
        }

        val navHeaderBinding = NavHeaderMainBinding.bind(binding.navView.getHeaderView(0))

        navHeaderBinding.model = model

        navHeaderBinding.lifecycleOwner = this

        binding.navViewButtonLogout.setOnClickListener {
            binding.drawerLayout.closeDrawer(Gravity.LEFT)
            MaterialAlertDialogBuilder(this)
                .setTitle(R.string.dialog_logout)
                .setPositiveButton(R.string.button_yes){dialog, which ->
                    dialog.dismiss()
                    startActivity(Intent(this, LoginActivity::class.java))
                    finish()
                }
                .setNegativeButton(R.string.button_no){dialog, which ->
                    dialog.dismiss()
                }
                .show()
        }

        LastSyncDialog().show(supportFragmentManager, "last sync dialog")


    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        logd("create")
        menuInflater.inflate(R.menu.menu_main, menu)
        setUpCustomerOptions(menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.menu_selected_customer -> {

                when (item.isChecked) {
                    true -> {
                        findNavController(R.id.nav_host_fragment).navigate(
                            NavGraphDirections.actionGlobalCustomerDetailFragment(
                                model.selectedCustomerResult.value?.data?.customerId!!
                            )
                        )
                    }
                    false -> {
                        MaterialAlertDialogBuilder(this)
                            .setTitle(R.string.dialog_menu_selected_customer_false_title)
                            .setNeutralButton(R.string.dialog_menu_selected_customer_false_button) { _, _ ->
                                findNavController(R.id.nav_host_fragment).navigate(
                                    NavGraphDirections.actionGlobalCustomerFragment()
                                )
                            }.show()
                    }
                }

                true
            }
            else -> return super.onOptionsItemSelected(item)
        }
    }

    private fun setUpCustomerOptions(menu: Menu?) {
        val item = menu?.findItem(R.id.menu_selected_customer)
        logd(model.selectedCustomerResult.hasObservers())
        if (!model.selectedCustomerResult.hasActiveObservers()) {
            observe(model.selectedCustomerResult) {
                it?.data?.let {
                    item?.setIcon(R.drawable.baseline_how_to_reg_24)
                    item?.isChecked = true
                } ?: kotlin.run {
                    item?.setIcon(R.drawable.baseline_person_24)
                    item?.isChecked = false
                }
            }
        }else{
            model.selectedCustomerResult.removeObservers(this)
            setUpCustomerOptions(menu)
        }
    }

    override fun androidInjector(): AndroidInjector<Any> = dispatchingAndroidInjector


}
