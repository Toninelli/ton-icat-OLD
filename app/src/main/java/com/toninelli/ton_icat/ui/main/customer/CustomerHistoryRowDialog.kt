package com.toninelli.ton_icat.ui.main.customer

import android.app.Dialog
import android.os.Bundle
import androidx.fragment.app.DialogFragment
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.toninelli.fileutil.autoclearedValue
import com.toninelli.ton_icat.R
import com.toninelli.ton_icat.databinding.DialogCustomerHistoryRowBinding
import com.toninelli.ton_icat.model.CustomerHistory

class CustomerHistoryRowDialog(val customerHistory: CustomerHistory, val callback: (Int)-> Unit): DialogFragment() {

    var binding by autoclearedValue<DialogCustomerHistoryRowBinding>()

    var adapter by autoclearedValue<CustomerHistoryRowAdapter>()

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return activity?.let {
            val builder = MaterialAlertDialogBuilder(it)

            val inflater = requireActivity().layoutInflater

            binding = DialogCustomerHistoryRowBinding.inflate(inflater)

            binding.history = customerHistory

            adapter = CustomerHistoryRowAdapter(customerHistory.rowCustomers){
                callback(it)
                dismiss()
            }

            binding.customerHistoryRowList.adapter = adapter

            builder.setTitle(R.string.customer_history_row_dialog_title)

            builder.setView(binding.root)

            builder.create()
        }?: throw IllegalStateException("Activity null")
    }


    override fun onStart() {
        super.onStart()
        dialog?.window!!.setLayout(1500, 1800)
    }


}