package com.toninelli.ton_icat.ui.main.sync

import androidx.lifecycle.ViewModel
import com.toninelli.ton_icat.domain.GetLastSyncUseCase
import com.toninelli.ton_icat.domain.exec
import javax.inject.Inject

class SyncViewModel @Inject constructor(private val getLastSyncUseCase: GetLastSyncUseCase) :
    ViewModel() {

    init {
        exec(getLastSyncUseCase,null)
    }

    val result = getLastSyncUseCase.resultLiveData

}