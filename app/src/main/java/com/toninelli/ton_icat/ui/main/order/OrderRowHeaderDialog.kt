package com.toninelli.ton_icat.ui.main.order

import android.app.Dialog
import android.os.Bundle
import androidx.fragment.app.DialogFragment
import androidx.navigation.fragment.findNavController
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.toninelli.fileutil.autoclearedValue
import com.toninelli.ton_icat.NavGraphDirections
import com.toninelli.ton_icat.R
import com.toninelli.ton_icat.databinding.DialogOrderRowHeaderBinding
import com.toninelli.ton_icat.model.OrderRow

class OrderRowHeaderDialog(val row: OrderRow): DialogFragment() {

    private var binding by autoclearedValue<DialogOrderRowHeaderBinding>()

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return activity?.let {
            val builder = MaterialAlertDialogBuilder(it)

            binding = DialogOrderRowHeaderBinding.inflate(requireActivity().layoutInflater)

            binding.row = row

            builder.setView(binding.root)

            builder.setTitle(R.string.dialog_order_row_header_title)

            builder.setPositiveButton(R.string.dialog_order_row_header_catalog_button){ dialog, _ ->
                findNavController().navigate(NavGraphDirections.actionGlobalCatalogFragment( intArrayOf(row.catalogItem.article.artCod)))
                dialog.dismiss()
            }

            builder.create()

        } ?: throw IllegalStateException("Activity null")
    }


}