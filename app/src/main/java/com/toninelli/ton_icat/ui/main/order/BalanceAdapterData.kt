package com.toninelli.ton_icat.ui.main.order

import android.view.Menu
import android.view.MenuItem
import android.view.MotionEvent
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.view.ActionMode
import androidx.recyclerview.selection.*
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.toninelli.fileutil.logd
import com.toninelli.fileutil.nullIfEmpty
import com.toninelli.ton_icat.R
import com.toninelli.ton_icat.binding.BindableListAdapterData
import com.toninelli.ton_icat.binding.BindableListAdapterInformation
import com.toninelli.ton_icat.databinding.BalanceParentItemBinding
import com.toninelli.ton_icat.databinding.OrderItemBinding
import com.toninelli.ton_icat.model.Customer
import com.toninelli.ton_icat.model.Order
import com.toninelli.ton_icat.model.OrderActionType
import com.toninelli.ton_icat.ui.common.BaseAdapter
import com.toninelli.ton_icat.vo.Resource
import com.toninelli.ton_icat.vo.case
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import okhttp3.internal.toImmutableList


class BalanceAdapterData(
    val model: BalanceViewModel,
    private val recyclerView: RecyclerView,
    private val callback: (OrderActionType, List<Order>) -> Unit
) : BaseAdapter<Customer, BalanceParentItemBinding>(
    R.layout.balance_parent_item, object : DiffUtil.ItemCallback<Customer>() {
        override fun areItemsTheSame(oldItem: Customer, newItem: Customer): Boolean {
            return false
        }

        override fun areContentsTheSame(oldItem: Customer, newItem: Customer): Boolean {
            return false
        }

    }), BindableListAdapterData<List<Customer>>, BindableListAdapterInformation<List<Order>?> {

    private var expandedSection = Pair<BalanceParentItemBinding?, Int>(null, -1)

    private val orderMap = mutableMapOf<String, MutableList<Order>>()

    private val customerMap = mutableMapOf<String, BalanceChildAdapterData>()

    private val colors = arrayOf(R.color.color_secondary, R.color.color_primary_variant)

    var selectionTracker: SelectionTracker<Long>? = null

    var actionMode: ActionMode? = null

    init {
        setHasStableIds(true)
    }

    fun initSelectionAdapter(fragment: BalanceFragment) {
        selectionTracker = SelectionTracker.Builder<Long>(
            "card selection",
            recyclerView,
            KeyProvider(recyclerView),
            BalanceLookUp(recyclerView),
            StorageStrategy.createLongStorage()
        ).withSelectionPredicate(SelectionPredicates.createSelectAnything()).build()

        selectionTracker?.addObserver(object : SelectionTracker.SelectionObserver<Long>() {
            override fun onSelectionChanged() {

                if (selectionTracker!!.selection.size() > 0) {
                    if (actionMode == null) {
                        actionMode =
                            fragment.activity.startSupportActionMode(object : ActionMode.Callback {
                                override fun onActionItemClicked(
                                    mode: ActionMode?,
                                    item: MenuItem?
                                ): Boolean {

                                    selectionTracker?.let { tracker ->

                                        val list = mutableListOf<Customer>()

                                        tracker.selection.forEach {
                                            list.add(currentList[it.toInt()])
                                        }

                                        val orderList: MutableList<Order> = mutableListOf()

                                        list.forEach { customer ->
                                            orderMap[customer.customerId]?.toList()
                                                ?.let { orderList.addAll(it) }
                                        }

                                        callback(OrderActionType.DELETE, orderList)

                                        tracker.clearSelection()
                                    }
                                    return false
                                }

                                override fun onCreateActionMode(
                                    mode: ActionMode?,
                                    menu: Menu?
                                ): Boolean {
                                    val inflater = mode?.menuInflater
                                    inflater?.inflate(R.menu.menu_selected_order, menu)
                                    return true
                                }

                                override fun onPrepareActionMode(
                                    mode: ActionMode?,
                                    menu: Menu?
                                ): Boolean {
                                    return false
                                }

                                override fun onDestroyActionMode(mode: ActionMode?) {
                                    selectionTracker?.clearSelection()
                                    actionMode = null
                                }


                            })
                    }
                    actionMode?.title = selectionTracker!!.selection.size().toString()
                } else if (actionMode != null) {
                    actionMode?.finish()
                }

            }
        })

    }

    override fun onCreateViewHolder(
        binding: BalanceParentItemBinding,
        viewType: Int
    ): BaseViewHolder<BalanceParentItemBinding> {
        return BalanceViewHolder(binding)
    }

    override fun onBindViewHolder(
        binding: BalanceParentItemBinding,
        item: Customer,
        position: Int
    ) {
        binding.customer = item

        binding.subItemVisibility = expandedSection.second == position

        binding.balanceParentLine.setBackgroundColor(binding.root.context.getColor(colors[position % 2]))

        changeArrow(binding)

        binding.root.setOnClickListener {

            if (selectionTracker?.selection?.isEmpty != false) {
                binding.subItemVisibility?.let {

                    if (expandedSection.second != position) {
                        expandedSection.first?.let { binding ->
                            binding.subItemVisibility = false
                            changeArrow(binding)
                        }
                        expandedSection = Pair(binding, position)
                    } else {
                        expandedSection = Pair(null, -1)
                    }


                    binding.subItemVisibility = !it
                    changeArrow(binding)
                }
            }
        }


        val adapter = if (customerMap[item.customerId] == null) {

            val childAdapter = customerMap.getOrPut(item.customerId,
                { BalanceChildAdapterData(callback) })

            val orders = orderMap[item.customerId]

            childAdapter.setCustomerOrder(orders)

            childAdapter

        } else {

            customerMap[item.customerId]
        }

        binding.balanceParentItemList.adapter = adapter

        selectionTracker?.let {
            binding.balanceParentItemLayout.isChecked = it.isSelected(position.toLong())
        }


    }


    private fun changeArrow(binding: BalanceParentItemBinding) {
        when (binding.subItemVisibility) {
            true -> binding.balanceParentArrow.setImageDrawable(binding.root.context.getDrawable(R.drawable.baseline_keyboard_arrow_up_24))
            false -> binding.balanceParentArrow.setImageDrawable(binding.root.context.getDrawable(R.drawable.baseline_keyboard_arrow_down_24))
        }

    }

    override fun setData(data: Resource<List<Customer>>) {
        data.case(success = {
            submitList(it.data)
        })


    }

    override fun setInformation(info: List<Order>?) {
        info?.let {
            val newMap = mutableMapOf<String, MutableList<Order>>()
            it.forEach { order ->
                val list = newMap.getOrPut(order.customerId) { mutableListOf() }
                list.add(order)
                newMap[order.customerId] = list
            }

            val oldKeys = orderMap.keys.toList()
            val newKeys = newMap.keys.toList()

            val removeKeys = oldKeys.subtract(newKeys)

            removeKeys.toList().nullIfEmpty()?.let {
                val items = currentList.toMutableList()
                it.forEach { removedCustomer ->
                    items.removeIf { it.customerId == removedCustomer }
                    customerMap.remove(removedCustomer)
                    expandedSection = Pair(null, -1)
                }
                submitList(items)
            }

            orderMap.clear()
            orderMap.putAll(newMap)


            if (customerMap.isNotEmpty()) {

                orderMap.forEach { key, value ->
                    customerMap.get(key)?.setCustomerOrder(value)
                }
            }
        }
    }

    class BalanceLookUp(private val recyclerView: RecyclerView) :
        ItemDetailsLookup<Long>() {
        override fun getItemDetails(event: MotionEvent): ItemDetails<Long>? {
            val view = recyclerView.findChildViewUnder(event.x, event.y)
            if (view != null) {
                return (recyclerView.getChildViewHolder(view) as BalanceViewHolder)
                    .getItemDetails()
            }
            return null
        }

    }

    class KeyProvider(private val recyclerView: RecyclerView) :
        ItemKeyProvider<Long>(SCOPE_MAPPED) {
        override fun getKey(position: Int): Long? {
            return recyclerView.adapter?.getItemId(position)
        }

        override fun getPosition(key: Long): Int {
            val viewHolder = recyclerView.findViewHolderForItemId(key)
            return viewHolder?.layoutPosition ?: RecyclerView.NO_POSITION
        }

    }


    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    class BalanceViewHolder(binding: BalanceParentItemBinding) :
        BaseViewHolder<BalanceParentItemBinding>(binding) {


        fun getItemDetails(): ItemDetailsLookup.ItemDetails<Long> =
            object : ItemDetailsLookup.ItemDetails<Long>() {
                override fun getPosition(): Int = itemId.toInt()
                override fun getSelectionKey(): Long? = itemId
            }

    }

}