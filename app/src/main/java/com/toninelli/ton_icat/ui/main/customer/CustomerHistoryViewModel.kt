package com.toninelli.ton_icat.ui.main.customer

import androidx.lifecycle.ViewModel
import com.toninelli.ton_icat.domain.GetCustomerByIdUseCase
import com.toninelli.ton_icat.domain.GetCustomerHistoryByIdUseCase
import com.toninelli.ton_icat.domain.exec
import javax.inject.Inject

class CustomerHistoryViewModel @Inject constructor(
    private val getCustomerHistoryByIdUseCase: GetCustomerHistoryByIdUseCase
): ViewModel() {

    val historyResult = getCustomerHistoryByIdUseCase.resultLiveData

    fun loadCustomerHistory(customerId: String){
        if(historyResult.value == null)
            exec(getCustomerHistoryByIdUseCase, GetCustomerHistoryByIdUseCase.Param(customerId))
    }

}