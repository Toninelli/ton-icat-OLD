package com.toninelli.ton_icat.ui.main.home

import androidx.recyclerview.widget.DiffUtil

import com.toninelli.ton_icat.R
import com.toninelli.ton_icat.binding.BindableListAdapterData

import com.toninelli.ton_icat.databinding.OffersItemBinding
import com.toninelli.ton_icat.model.Offer
import com.toninelli.ton_icat.ui.common.BaseAdapter
import com.toninelli.ton_icat.vo.Resource
import com.toninelli.ton_icat.vo.case

class HomeOfferAdapterData(val callback: (Offer) -> Unit, val onPdfClick: (Offer) -> Unit) :
    BaseAdapter<Offer, OffersItemBinding>(
        R.layout.offers_item,
        object : DiffUtil.ItemCallback<Offer>() {
            override fun areItemsTheSame(oldItem: Offer, newItem: Offer): Boolean {
                return oldItem == newItem
            }

            override fun areContentsTheSame(oldItem: Offer, newItem: Offer): Boolean {
                return oldItem.name == newItem.name
            }

        }), BindableListAdapterData<List<Offer>> {


    override fun onBindViewHolder(
        binding: OffersItemBinding,
        item: Offer,
        position: Int
    ) {
        binding.offer = item
        binding.root.setOnClickListener {
            binding.offer?.let { callback.invoke(it) }
        }

    }

    override fun setData(data: Resource<List<Offer>>) {
        data.case(success = { submitList(it.data) })
    }


}