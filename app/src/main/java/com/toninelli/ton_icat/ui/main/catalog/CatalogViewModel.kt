package com.toninelli.ton_icat.ui.main.catalog

import androidx.lifecycle.*
import com.toninelli.fileutil.logd
import com.toninelli.fileutil.loge
import com.toninelli.ton_icat.domain.*
import com.toninelli.ton_icat.extension.switchRes
import com.toninelli.ton_icat.icatcontext.RunTimeValue

import com.toninelli.ton_icat.model.CatalogItem
import com.toninelli.ton_icat.model.OrderRow
import com.toninelli.ton_icat.model.SelectedCustomer
import com.toninelli.ton_icat.util.NextPageHandler
import com.toninelli.ton_icat.vo.Resource
import com.toninelli.ton_icat.vo.case
import kotlinx.coroutines.cancel
import javax.inject.Inject

class CatalogViewModel @Inject constructor(
    private val getCategUseCase: GetCategUseCase,
    private val getSupplierUseCase: GetSupplierUseCase,
    private val getCatalogItemByCategUseCase: GetCatalogItemByCategUseCase,
    private val getCatalogItemByQueryUseCase: GetCatalogItemByQueryUseCase,
    private val getCatalogItemBySuppUseCase: GetCatalogItemBySuppUseCase,
    private val getCatalogItemByCodListUseCase: GetCatalogItemByCodListUseCase,
    private val getSelectedCustomerUseCase: GetSelectedCustomerUseCase,
    private val getCustomerOrdersNotCompletedUseCase: GetCustomerOrdersNotCompletedUseCase,
    private val insertOrderRowUseCase: InsertOrderRowUseCase
) : ViewModel() {

    var query: String = ""
        set(value) {
            if (value != field) {
                field = value
                loadCatalogItemByQuery(field)
            }
        }

    private var lastSelectedCustomerResult: Resource<SelectedCustomer>? = null

    val useCasesResult = MediatorLiveData<Resource<List<CatalogItem>>>()

    val nextPageHandler = NextPageHandler(useCasesResult) {
        loadMoreData(it)
    }

    val categOrSupplier = MutableLiveData(true)

    val resultCateg = getCategUseCase.resultLiveData

    val resultSupplier = getSupplierUseCase.resultLiveData

    val result = nextPageHandler.totalResult

    val selectedCustomerResult = getSelectedCustomerUseCase.resultLiveData.map {
        it.also {
            it.case(success = { res ->
                if (lastSelectedCustomerResult != it) {
                    loadCustomerOrder(it.data?.customerId)
                    lastSelectedCustomerResult = it
                    refreshData()
                }
            })
        }
    }

    val orderResult = getCustomerOrdersNotCompletedUseCase.resultLiveData

    val insertResult = insertOrderRowUseCase.resultLiveData

    private val paramList = Array<Any>(4) { -1 }

    private var lastMethodSelected = -1

    init {
        exec(getCategUseCase)
        exec(getSupplierUseCase)
        exec(getSelectedCustomerUseCase)

        useCasesResult.addSource(getCatalogItemByCategUseCase.resultLiveData) {
            useCasesResult.value = it
        }

        useCasesResult.addSource(getCatalogItemByQueryUseCase.resultLiveData) {
            useCasesResult.value = it
        }

        useCasesResult.addSource(getCatalogItemBySuppUseCase.resultLiveData) {
            useCasesResult.value = it
        }

        useCasesResult.addSource(getCatalogItemByCodListUseCase.resultLiveData) {
            useCasesResult.value = it
        }
    }


    fun loadCatalogItemByCateg(id: Int, limit: Int = 50, offset: Int = 0) {
        if (paramList[0] != id) {
            nextPageHandler.reset()
        }
        paramList[0] = id
        lastMethodSelected = 0
        if (id == 0) return
        exec(
            getCatalogItemByCategUseCase,
            GetCatalogItemByCategUseCase.Param(
                id,
                limit,
                offset,
                lastSelectedCustomerResult?.data?.customerId
            )
        )
    }

    fun loadCatalogItemBySupplier(supplierId: String, limit: Int = 50, offset: Int = 0) {
        if (paramList[1] != supplierId) {
            nextPageHandler.reset()
        }
        paramList[1] = supplierId
        lastMethodSelected = 1
        exec(
            getCatalogItemBySuppUseCase,
            GetCatalogItemBySuppUseCase.Param(
                supplierId,
                limit,
                offset,
                lastSelectedCustomerResult?.data?.customerId
            )
        )
    }

    fun loadCatalogItemByQuery(query: String, limit: Int = 600, offset: Int = 0) {
        if (paramList[2] != query) {
            nextPageHandler.reset()
        }
        paramList[2] = query
        lastMethodSelected = 2
        exec(
            getCatalogItemByQueryUseCase,
            GetCatalogItemByQueryUseCase.Param(
                query,
                limit,
                offset,
                lastSelectedCustomerResult?.data?.customerId
            )
        )
    }

    fun loadItemsByCodList(cods: List<Int>) {
        lastMethodSelected = 3
        paramList[3] = cods
        exec(
            getCatalogItemByCodListUseCase,
            GetCatalogItemByCodListUseCase.Param(cods, lastSelectedCustomerResult?.data?.customerId)
        )
    }


    fun insertOrderRow(orderRow: OrderRow) {
        exec(insertOrderRowUseCase, InsertOrderRowUseCase.Param(orderRow))
    }


    private fun loadCustomerOrder(customerId: String?) {
            exec(
                getCustomerOrdersNotCompletedUseCase,
                GetCustomerOrdersNotCompletedUseCase.Param(customerId)
            )

    }

    private fun loadMoreData(it: Int) {
        loge("reload $it")
        reload(offset = it)
    }

    private fun refreshData() {
        loge("refresh")
        result.value?.data?.let {
            nextPageHandler.clearData()
            reload(limit = it.size)
        }
    }

    private fun reload(limit: Int = 50, offset: Int = 0) {
        when (lastMethodSelected) {
            0 -> {
                loadCatalogItemByCateg(paramList[0] as Int, limit, offset)
            }
            1 -> {
                loadCatalogItemBySupplier(paramList[1] as String, limit, offset)
            }
            2 -> {
                loadCatalogItemByQuery(paramList[2] as String, offset = offset)

            }
            3 -> {
                if (offset == 0)
                    loadItemsByCodList(paramList[3] as List<Int>)
            }

        }
    }

}