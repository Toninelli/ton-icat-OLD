package com.toninelli.ton_icat.ui.main.customer


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.navigation.NavArgsLazy
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.RecyclerView
import com.toninelli.fileutil.autoclearedValue
import com.toninelli.fileutil.logd
import com.toninelli.ton_icat.NavGraphDirections
import com.toninelli.ton_icat.R
import com.toninelli.ton_icat.databinding.FragmentCustomerHistoryBinding
import com.toninelli.ton_icat.di.Injectable
import com.toninelli.ton_icat.extension.observe
import com.toninelli.ton_icat.ui.BaseFragment
import com.toninelli.ton_icat.ui.common.ViewModelFactory
import javax.inject.Inject


class CustomerHistoryFragment : BaseFragment(), Injectable {

    private var binding by autoclearedValue<FragmentCustomerHistoryBinding>()

    private var adapter by autoclearedValue<CustomerHistoryAdapter>()

    private val model: CustomerHistoryViewModel by viewModels { factory }

    private val customerIdArgs by navArgs<CustomerHistoryFragmentArgs>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentCustomerHistoryBinding.inflate(inflater, container, false)

        binding.lifecycleOwner = this

        binding.model = model

        adapter = CustomerHistoryAdapter(activity){
            findNavController().navigate(NavGraphDirections.actionGlobalCatalogFragment(intArrayOf(it)))
        }

        binding.historyList.adapter = adapter

        binding.historyList.addItemDecoration(DividerItemDecoration(activity, RecyclerView.VERTICAL))

        return binding.root
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        model.loadCustomerHistory(customerIdArgs.customerId)
    }


}
