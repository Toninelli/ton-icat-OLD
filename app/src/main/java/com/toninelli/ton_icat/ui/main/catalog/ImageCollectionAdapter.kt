package com.toninelli.ton_icat.ui.main.catalog

import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter


class ImageCollectionAdapter(fragment: Fragment, val urls: List<String>) : FragmentStateAdapter(fragment){
    override fun getItemCount() = urls.size

    override fun createFragment(position: Int): Fragment {

        return ImageFragment(urls[position])
    }


}