package com.toninelli.ton_icat.ui.main.sync

import android.app.Dialog
import android.content.DialogInterface
import android.os.Bundle
import android.view.*
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.DialogFragment
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import com.toninelli.fileutil.autoclearedValue
import com.toninelli.ton_icat.R
import com.toninelli.ton_icat.binding.ResourceListener
import com.toninelli.ton_icat.databinding.DialogSyncProgressLayoutBinding
import com.toninelli.ton_icat.di.Injectable
import com.toninelli.ton_icat.model.SyncProgress
import com.toninelli.ton_icat.ui.common.ViewModelFactory
import com.toninelli.ton_icat.vo.Resource
import com.toninelli.ton_icat.vo.Status
import javax.inject.Inject


class SyncProgressDialog(val callback: () -> Unit) : DialogFragment(), Injectable,
    ResourceListener {

    @Inject
    lateinit var factory: ViewModelFactory

    var binding by autoclearedValue<DialogSyncProgressLayoutBinding>()

    private val progressViewModel: SyncProgressViewModel by viewModels { factory }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DialogSyncProgressLayoutBinding.inflate(inflater)

        binding.listener = this

        progressViewModel.syncCatalog()

        binding.model = progressViewModel

        binding.lifecycleOwner = this

        return binding.root
    }

    override fun onStart() {
        super.onStart()
        dialog?.window!!.setLayout(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.WRAP_CONTENT
        )
        dialog?.window!!.setGravity(Gravity.TOP)

    }

    override fun onDismiss(dialog: DialogInterface) {
        super.onDismiss(dialog)
        callback()
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = super.onCreateDialog(savedInstanceState)
        isCancelable = false
        return dialog
    }

    override fun <T> onResourceSuccess(resource: Resource<T>) {
        super.onResourceSuccess(resource)
        val progress = resource.data as SyncProgress
        if (progress.status == Status.SUCCESS) {
            isCancelable = true
        }

    }

    override fun onResourceError(exception: Exception) {
        super.onResourceError(exception)
        isCancelable = true
    }

}