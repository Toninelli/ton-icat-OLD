package com.toninelli.ton_icat.ui.main.order

import androidx.lifecycle.ViewModel
import androidx.lifecycle.map
import com.toninelli.ton_icat.domain.*
import com.toninelli.ton_icat.extension.switchRes
import com.toninelli.ton_icat.model.Customer
import com.toninelli.ton_icat.model.Order
import javax.inject.Inject

class OrderHeaderViewModel @Inject constructor(
    private val getOrderByIdUseCase: GetOrderByIdUseCase,
    private val getCustomerByIdUseCase: GetCustomerByIdUseCase,
    private val updateOrderTypeUseCase: UpdateOrderTypeUseCase,
    private val updateOrderDestinationUseCase: UpdateOrderDestinationUseCase,
    private val updateOrderDeliveryUseCase: UpdateOrderDeliveryUseCase,
    private val updateOrderNoteUseCase: UpdateOrderNoteUseCase,
    private val sendOrderUseCase: SendOrderUseCase
) : ViewModel() {

    var orderResult = getOrderByIdUseCase.resultLiveData.map {
        it?.data
    }

    val customerResult = getOrderByIdUseCase.resultLiveData.switchRes {
        loadCustomer(it.data?.customerId ?: "null")
        getCustomerByIdUseCase.resultLiveData
    }

    val typeList = Order.Type.values().map { it.typeName }

    val deliveryList = Order.Delivery.getStringValues()

    val sendOrderResult = sendOrderUseCase.resultLiveData

    fun loadOrder(orderId: Long) {
        exec(getOrderByIdUseCase, GetOrderByIdUseCase.Param(orderId))
    }

    private fun loadCustomer(customerId: String) {
        exec(getCustomerByIdUseCase, GetCustomerByIdUseCase.Param(customerId))
    }

    fun updateOrderType(type: Order.Type) {
        orderResult.value?.orderId?.let {
            exec(
                updateOrderTypeUseCase,
                UpdateOrderTypeUseCase.Param(it, type)
            )
        }
    }

    fun updateOrderDestination(address: Customer.Address) {
        orderResult.value?.orderId?.let {
            exec(updateOrderDestinationUseCase, UpdateOrderDestinationUseCase.Param(it, address))
        }
    }

    fun updateOrderNote(note: String){
        orderResult.value?.orderId?.let {
            exec(updateOrderNoteUseCase, UpdateOrderNoteUseCase.Param(it, note))
        }
    }

    fun updateOrderDelivery(delivery: Order.Delivery) {
        orderResult.value?.orderId?.let {
            exec(
                updateOrderDeliveryUseCase,
                UpdateOrderDeliveryUseCase.Param(it, delivery)
            )
        }
    }

    fun sendOrder(){
        orderResult.value?.orderId?.let {
            exec(sendOrderUseCase, SendOrderUseCase.Param(it))
        }
    }

}