package com.toninelli.ton_icat.ui.main.catalog

import androidx.recyclerview.widget.DiffUtil
import com.toninelli.ton_icat.R
import com.toninelli.ton_icat.databinding.CatalogSelectedCategItemBinding
import com.toninelli.ton_icat.model.Category
import com.toninelli.ton_icat.ui.common.BaseAdapter
import kotlinx.coroutines.*


class SelectedCategAdapter(val callback: (Category) -> Unit) :
    BaseAdapter<Category, CatalogSelectedCategItemBinding>(
        R.layout.catalog_selected_categ_item,
        diffCallback = object : DiffUtil.ItemCallback<Category>() {
            override fun areItemsTheSame(oldItem: Category, newItem: Category): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(oldItem: Category, newItem: Category): Boolean {
                return oldItem.id == newItem.id
            }
        }) {


    private var block: Boolean = false

    override fun onBindViewHolder(
        binding: CatalogSelectedCategItemBinding,
        item: Category,
        position: Int
    ) {
        binding.category = item

        binding.root.setOnClickListener {
            if (!block) {
                binding.category?.let {
                    removeSelectedCateg(it)
                    callback(it)
                    startTimer()
                }
            }
        }
    }

    private fun startTimer() {
        CoroutineScope(Dispatchers.IO).launch {
            block = true
            delay(3000)
            block = false
        }
    }


    fun addSelectedCateg(category: Category) {
        val showedList = currentList.toMutableList()
        submitList(null)

        if (showedList.isNullOrEmpty())
            submitList(listOf(category))
        else {
            if (!showedList.contains(category))
                showedList.add(category)
            submitList(showedList)
        }
    }

    private fun removeSelectedCateg(category: Category) {
        val items = currentList.toMutableList()
        submitList(null)
        val index = items.indexOf(category)
        items.removeAll {
            items.indexOf(it) > index
        }
        items.remove(category)
        submitList(items)
    }

    fun clear() {
        submitList(null)
    }

}