package com.toninelli.ton_icat.ui.login


import android.app.ActivityManager
import android.content.Context
import android.content.Context.ACTIVITY_SERVICE
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.lifecycle.*
import androidx.navigation.fragment.findNavController
import androidx.viewbinding.ViewBinding
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.toninelli.fileutil.autoclearedValue
import com.toninelli.fileutil.logd
import com.toninelli.ton_icat.R
import com.toninelli.ton_icat.databinding.FragmentLoginBinding
import com.toninelli.ton_icat.di.Injectable
import com.toninelli.ton_icat.extension.observe
import com.toninelli.ton_icat.ui.BaseFragment
import kotlin.properties.ReadOnlyProperty
import kotlin.properties.ReadWriteProperty
import kotlin.reflect.KProperty


class LoginFragment : BaseFragment(), Injectable {

    private var binding by viewBinding<FragmentLoginBinding>()

    private val model: LoginViewModel by viewModels { factory }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = FragmentLoginBinding.inflate(inflater, container, false)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.lifecycleOwner = viewLifecycleOwner


        binding.model = model

        binding.loginButton.setOnClickListener {
            model.login()
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        observe(model.loginResult) {
            it?.data?.let {
                model.checkAgent(it)
            }
        }


        observe(model.checkUniqueAgentResult) {
            it?.value?.let { res ->
                if (res) {
                    findNavController().navigate(LoginFragmentDirections.actionLoginFragmentToMainActivity())
                    activity.finish()
                } else {
                    MaterialAlertDialogBuilder(activity)
                        .setTitle(R.string.login_different_agent_dialog_title)
                        .setMessage(R.string.login_different_agent_dialog_message)
                        .setPositiveButton(R.string.button_yes) { _, _ ->
                            (activity.getSystemService(ACTIVITY_SERVICE) as ActivityManager).clearApplicationUserData()
                        }
                        .setNegativeButton(R.string.button_no) { dialog, _ ->
                            dialog.dismiss()
                        }
                        .show()

                }
            }
        }
    }
}


class FragmentViewBindingDelegate<T : ViewBinding>(
    val fragment: Fragment
) : ReadWriteProperty<Fragment, T> {
    private var binding: T? = null

    init {
        fragment.lifecycle.addObserver(object : DefaultLifecycleObserver {
            override fun onCreate(owner: LifecycleOwner) {
                fragment.viewLifecycleOwnerLiveData.observe(fragment) { viewLifecycleOwner ->
                    viewLifecycleOwner.lifecycle.addObserver(object : DefaultLifecycleObserver {
                        override fun onDestroy(owner: LifecycleOwner) {
                            binding = null
                        }
                    })
                }
            }
        })
    }

    override fun getValue(thisRef: Fragment, property: KProperty<*>): T {
        return binding ?: throw IllegalStateException(
            "should never call auto-cleared-value get when it might not be available"
        )
    }

    override fun setValue(thisRef: Fragment, property: KProperty<*>, value: T) {
        binding = value
    }
}

fun <T : ViewBinding> Fragment.viewBinding() =
    FragmentViewBindingDelegate<T>(this)


