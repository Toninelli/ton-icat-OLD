package com.toninelli.ton_icat.ui.common

import android.content.Context
import android.graphics.Canvas
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.LinearLayout
import androidx.databinding.DataBindingUtil
import com.toninelli.ton_icat.R
import com.toninelli.ton_icat.databinding.QuantityWidgetViewBinding

class QuantityWidget(context: Context, attrs: AttributeSet?) : LinearLayout(context, attrs) {

    var binding: QuantityWidgetViewBinding
    private var totalQta: Int = 0
    private var totalPrice: Float = 0.0F
    var callback: ((qta: Int, multQta: Int,  totalPrice: Float) -> Unit)? = null

    var qta: Int = 0
    set(value) {
        field = value
        binding.qta = value
        calculateTotal()
        invalidate()
    }


    //attributes
    var maxQta: Int = 9999999
        set(value) {
            field = value
            invalidate()
        }
    var multQta: Int = 1
        set(value) {
            field = value
            calculateTotal()
            invalidate()
        }

    var price: Float = 0.0F
        set(value) {
            field = value
            binding.price = value
            invalidate()
        }


    init {

        val inflater = LayoutInflater.from(context)
        binding = QuantityWidgetViewBinding.inflate(inflater, this, true)

        attrs?.let {

            val attributes = context.obtainStyledAttributes(it, R.styleable.QuantityWidget)

            val multQta = attributes.getInt(R.styleable.QuantityWidget_multQta, multQta)

            val price = attributes.getFloat(R.styleable.QuantityWidget_price, price)

            val maxQta = attributes.getInt(R.styleable.QuantityWidget_maxQta, maxQta)

            val initialQta = attributes.getInt(R.styleable.QuantityWidget_qta, qta)

            initListener()

            this.qta = initialQta
            this.multQta = multQta
            this.price = price
            this.maxQta = maxQta

            attributes.recycle()
        }

    }

    private fun initListener() {
        binding.quantityWidgetMinus.setOnClickListener {
            if (qta > 0) {
                qta--
            }

            callback?.let { it(qta, multQta, totalPrice) }
        }

        binding.quantityWidgetPlus.setOnClickListener {
            val tempQta = qta + 1
            val tempValue = tempQta * multQta
            if (tempValue <= maxQta) {
               qta++
            }

            callback?.let { it(qta, multQta, totalPrice) }

        }
    }

    private fun calculateTotal() {
        totalQta = qta * multQta
        totalPrice = totalQta * price
        binding.totalQta = totalQta

    }

    fun setOnQuantityChangeListener(callback: (qta: Int, multQta: Int, totalPrice: Float) -> Unit) {
        this.callback = callback
    }

}