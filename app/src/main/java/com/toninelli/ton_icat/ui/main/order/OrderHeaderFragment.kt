package com.toninelli.ton_icat.ui.main.order


import android.os.Bundle
import android.text.InputType
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import androidx.core.widget.addTextChangedListener
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.navArgs
import com.google.android.material.datepicker.MaterialDatePicker
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.toninelli.fileutil.autoclearedValue
import com.toninelli.fileutil.logd
import com.toninelli.ton_icat.R
import com.toninelli.ton_icat.databinding.FragmentOrderHeaderBinding
import com.toninelli.ton_icat.di.Injectable
import com.toninelli.ton_icat.extension.formatWithoutHour
import com.toninelli.ton_icat.extension.hideKeyboard
import com.toninelli.ton_icat.extension.observe
import com.toninelli.ton_icat.model.Order
import com.toninelli.ton_icat.ui.BaseFragment
import java.util.*

class OrderHeaderFragment : BaseFragment(), Injectable {

    var binding by autoclearedValue<FragmentOrderHeaderBinding>()
    val model: OrderHeaderViewModel by viewModels { factory }
    var adapter by autoclearedValue<OrderHeaderDestinationAdapterData>()

    private val pickerBuilder = MaterialDatePicker.Builder.datePicker()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentOrderHeaderBinding.inflate(inflater, container, false)

        val id = navArgs<OrderHeaderFragmentArgs>().value.order

        model.loadOrder(id)

        binding.lifecycleOwner = this

        binding.model = model

        binding.editEnabled = true

        binding.orderHeaderSendButton.setOnClickListener {
            MaterialAlertDialogBuilder(activity)
                .setTitle(R.string.order_header_send_button_dialog_title)
                .setMessage(R.string.order_header_send_button_dialog_message)
                .setPositiveButton(R.string.button_yes){ _, _ ->
                    model.sendOrder()
                    binding.editEnabled = false
                }
                .setNegativeButton(R.string.button_no){ dialog, _ -> dialog.dismiss() }
                .show()
        }

        binding.orderHeaderCalendarButton.setOnClickListener {
            val picker = pickerBuilder.build()
            picker.show(activity.supportFragmentManager, "picker")
            picker.addOnPositiveButtonClickListener {
                model.updateOrderDelivery(Order.Delivery(model.orderResult.value?.delivery?.delName, Date(it)))
                binding.orderHeaderSelectedDate.text = Date(it).formatWithoutHour()
            }
        }

        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        adapter = OrderHeaderDestinationAdapterData {
            model.updateOrderDestination(it)
        }

        binding.orderHeaderDestinationList.adapter = adapter

        binding.orderHeaderTypeMenuList.addTextChangedListener {

            val newType = if (it.toString() == Order.Type.ORDER_PROPOSAL.typeName)
                Order.Type.ORDER_PROPOSAL
            else
                Order.Type.ORDER

            if (newType != model.orderResult.value?.type) {
                model.updateOrderType(newType)
            }
        }

        observe(model.orderResult) {
            adapter.selectedDestination = it?.destination
            pickerBuilder.setSelection(it?.delivery?.delDate?.time)
        }

        binding.orderHeaderDeliveryMenuList.addTextChangedListener { value ->
            if (value.toString().isNotBlank()) {
                val newDelivery = Order.Delivery(value.toString(), model.orderResult.value?.delivery?.delDate)
                model.updateOrderDelivery(newDelivery)
            }

        }

        binding.orderHeaderNotesEditText.imeOptions = EditorInfo.IME_ACTION_DONE

        binding.orderHeaderNotesEditText.setRawInputType(InputType.TYPE_CLASS_TEXT)

        binding.orderHeaderNotesEditText.setOnEditorActionListener { v, actionId, event ->
            return@setOnEditorActionListener when (actionId) {
                EditorInfo.IME_ACTION_DONE -> {

                    activity.hideKeyboard(v)

                    v.clearFocus()

                    model.updateOrderNote(v.text.toString())

                    true
                }
                else -> false
            }
        }

    }


}
